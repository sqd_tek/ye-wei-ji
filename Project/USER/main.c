/************************ST Header File**************************/

/************************HUAWEI LiteOS Header File***************/
#include "stdlib.h"
#include "string.h"
#include "los_base.h"
#include "los_config.h"
#include "los_typedef.h"
#include "los_hwi.h"
#include "los_task.ph"
#include "los_sem.h"
#include "los_event.h"
#include "los_memory.h"
#include "los_queue.ph"
#include "los_sem.h"
#include "los_event.h"
#include "stdio.h"
#include "hal_relay.h"
#include "hal_senser.h"
#include "hal_lcd.h"

/**********************SQD Header File**************************/
#include "beep.h"
#include "led.h"
#include "hal_lcd.h"
#include "uart.h"
#include "delay.h"
#include "Timer.h"
#include "hal_key.h"
#include "hal_gprs.h"
#include "ADS8344.h"
#include "RX485.h"
#include "battery.h"
#include "rtc.h"
#include "gpio.h"
#include "usmart.h"
#include "common.h"
#include "download.h"
#include "FlashCrc.h"
#include "iwdg.h"
#include "hal_eeprom.h"
#include "stm32f10x.h"
#include "config.h"
#include "battery.h"
//#define //d_printf printf

DebugInfo debugInfo[TANK_MAX_LEN];
UINT32 g_TestTskHandle;
Board_Info g_board_info[TANK_MAX_LEN];
unsigned int send_flag = 0;
unsigned int sqd_send_flag = 1;
uint32_t gprs_count = 0;
uint32_t sqd_gprs_count = 0;
extern GprsStage gprs_stage;
extern uint16_t timer_intervel[9];
// 调试打印语句控制
int g_print_flag = 1;

int8_t BatteryStatus = -1;
uint16_t BatteryValue = 0;
/*
 * 心跳任务
 * 主要用来计时，和一些标志位的改变
 */
void heartbeat_task(void) {
	uint32_t secondCount = 0;
	eeprom_task();
	Battery_Init(ADC1, ADC_Channel_13);
	for (;;) {
		IWDG_ReloadCounter();
		gprs_count++;
		sqd_gprs_count++;
		secondCount++;
		// 电池未插入
		if (!Battery_InsertStatus()) {
			BatteryValue = Battery_GetVoltage(100);
			
			if (!Battery_chargeStatus()) { // 电池正常充电
				BatteryStatus = BATTERY_CHARGE;
			} else { 
				if (!Battery_FillStatus()) { // 电池充满
					BatteryStatus = BATTERY_FILL;
				} else {
					BatteryStatus = BATTERY_NOT_CHARGE;
				}
			}
		} else {
			BatteryStatus = BATTERY_NOT_INSERT;
			printf("Please insert the Battery!\r\n");
		}
		
		if (gprs_count % 10 == 0) {
			D_PRINTF("flag:%x, gprs:%d,  %d %d\r\n", send_flag, gprs_count, g_board_info[0].sendTimeIndex, timer_intervel[g_board_info[0].sendTimeIndex]);
			D_PRINTF("second count:%d\r\n", secondCount);
		}
//		if (timer_intervel[g_board_info[0].sendTimeIndex] > (30*60)) { // 发送时间需要大于30分钟
//			if (gprs_count < (timer_intervel[g_board_info[0].sendTimeIndex]) - 5 * 60) { // 提前5分钟唤醒
//				send_flag |= SEND_FLAG_LOW_POWER;	// 关闭电源，进入低功耗
//			} else {
//				send_flag &= ~SEND_FLAG_LOW_POWER;	// 打开电源，退出低功耗
//			}
//		}
		if (gprs_count >= (timer_intervel[g_board_info[0].sendTimeIndex])) { 
			send_flag &= ~SEND_FLAG_TIME;	// 进入发送模式
		} else {
			send_flag |= SEND_FLAG_TIME;	
		}
	}
}



UINT32 Create_GPRS_Task(void) {
    UINT32 uwRet = LOS_OK;
    
    TSK_INIT_PARAM_S task_init_param;
    task_init_param.usTaskPrio = 1;
    task_init_param.pcName = "GPRS_Task";
    task_init_param.pfnTaskEntry = (TSK_ENTRY_FUNC)GPRS_Task;
    task_init_param.uwStackSize = SIZE(0x1100) ;
    task_init_param.uwResved = 0;
    uwRet = LOS_TaskCreate(&g_TestTskHandle, &task_init_param);
    if (uwRet != LOS_OK) {
        return uwRet;
    }
    return uwRet;
}

UINT32 Create_Heartbeat_Task(void) {
    UINT32 uwRet = LOS_OK;
    
    TSK_INIT_PARAM_S task_init_param;
    task_init_param.usTaskPrio = 6;
    task_init_param.pcName = "Heartbeat_Task";
    task_init_param.pfnTaskEntry = (TSK_ENTRY_FUNC)heartbeat_task;
    task_init_param.uwStackSize = SIZE(0x200) ;
    task_init_param.uwResved = 0;
    uwRet = LOS_TaskCreate(&g_TestTskHandle, &task_init_param);
    if (uwRet != LOS_OK) {
        return uwRet;
    }
    return uwRet;
}

UINT32 Create_GetSensorData(void) {
	UINT32 uwRet = LOS_OK;
    
    TSK_INIT_PARAM_S task_init_param;
    task_init_param.usTaskPrio = 3;
    task_init_param.pcName = "Heartbeat_Task";
    task_init_param.pfnTaskEntry = (TSK_ENTRY_FUNC)get_sensor_data_task;
    task_init_param.uwStackSize = SIZE(0x500) ;
    task_init_param.uwResved = 0;
    uwRet = LOS_TaskCreate(&g_TestTskHandle, &task_init_param);
    if (uwRet != LOS_OK) {
        return uwRet;
    }
    return uwRet;
}
UINT32 Create_Usmart_Task(void) {
	UINT32 uwRet = LOS_OK;
    
    TSK_INIT_PARAM_S task_init_param;
    task_init_param.usTaskPrio = 2;
    task_init_param.pcName = "Usmart_Task";
    task_init_param.pfnTaskEntry = (TSK_ENTRY_FUNC)funUsmart;
    task_init_param.uwStackSize = SIZE(0x500) ;
    task_init_param.uwResved = 0;
    uwRet = LOS_TaskCreate(&g_TestTskHandle, &task_init_param);
    if (uwRet != LOS_OK) {
        return uwRet;
    }
    return uwRet;
}


UINT32 Create_LCD_Task(void) {
	UINT32 uwRet = LOS_OK;
    
    TSK_INIT_PARAM_S task_init_param;
    task_init_param.usTaskPrio = 5;
    task_init_param.pcName = "Lcd_Task";
    task_init_param.pfnTaskEntry = (TSK_ENTRY_FUNC)lcd_task;
    task_init_param.uwStackSize = SIZE(0x300) ;
    task_init_param.uwResved = 0;
    uwRet = LOS_TaskCreate(&g_TestTskHandle, &task_init_param);
    if (uwRet != LOS_OK) {
        return uwRet;
    }
    return uwRet;
}

/*
 * SWD/Debug串口选择
 * 开机 3 秒之后，进入串口调试
 * 上电 3 秒之内，可以进行 SWD 下载程序
 */
void SwdUartSelect(void) {
	GPIOX_Init(GPIOA, GPIO_Pin_8, GPIO_Mode_Out_PP, true);
	GPIOX_Init(GPIOB, GPIO_Pin_9, GPIO_Mode_Out_PP, true);
}

void GPIOSelect(void) {
	GPIOX_Init(GPIOA, GPIO_Pin_All, GPIO_Mode_IPD, false);
	GPIOX_Init(GPIOB, GPIO_Pin_All, GPIO_Mode_IPD, false);
	GPIOX_Init(GPIOC, GPIO_Pin_All, GPIO_Mode_IPD, false);
	GPIOX_Init(GPIOD, GPIO_Pin_All, GPIO_Mode_IPD, false);
	GPIOX_Init(GPIOE, GPIO_Pin_All, GPIO_Mode_IPD, false);
}

/*
 * 使用 boot 和远程升级功能之后，需要在 main 中调用本函数
 * 进行 A 分区的配置表更新，开启看门狗 (16S)
 */ 
void update_Init(void) {
	boot_done();
    // Initialize global partition info, if this is the first time
    // the board run, it will be reset to default values.
    init_factory_info();
    // The running version application is always in partition A
    if (!load_partition_info(ParA, &g_info)) {
        printf("Error when loading boot partition info.\r\n");
        // TODO: ?
    } else {
		g_version = g_info.version;
    }
	IWDG_Init();			//init watch dog 16s
}

/*
 * 系统正式运行之前的硬件初始化
 * 包括调试串口初始化
 * SWD/debug 选择初始化
 * OLED 显示公司LOGO
 * 任务创建
 */ 
void Hardware_Init(void) {
	NVIC_PriorityGroupConfig(NVIC_PriorityGroup_4);
	
#ifdef USARTX_1
	Usart1_Init(115200);  
#elif defined USARTX_2
	Usart2_Init(115200);  
#elif defined USARTX_3
	Usart3_Init(115200);  
#elif defined USARTX_4
	Uart4_Init(115200);  
#elif defined USARTX_5
	Uart5_Init(115200);  
#endif
	// display logo
	update_Init();
	SwdUartSelect();
	
    Create_Heartbeat_Task();
	Create_LCD_Task();
	Create_GetSensorData();
    Create_GPRS_Task();
}
