#ifndef __CONFIG_H__
#define __CONFIG_H__
#include "stm32f10x.h"
#include "stm32f10x_usart.h"
#include "uart.h"

/* 
 * 支持计算罐体个数
 * 工业：	1
 * LNG：	1
 * 加气站：	2
 */
#define TANK_MAX_LEN 1 			


/*
 *	调试串口
 * USARTX_1
 * USARTX_2
 * USARTX_3
 * USARTX_4
 * USARTX_5
 */
#if !((defined USARTX_1) || (defined USARTX_2) || (defined USARTX_3) || (defined USARTX_4) || (defined USARTX_5)) // 没有定义调试串口
	#define USARTX_1
#endif

/*
 * 调试串口输出，可以打印错误位置
 */
#define DEBUGOUT(x)     printf("[%s] %d: %s\r\n", (uint8_t *)__FILE__, __LINE__, (x));


// 需要一个控制变量
#define D_PRINTF(...)	if (g_print_flag){ printf(__VA_ARGS__);}

#define AD_AVG_NUM		5

#define PI                      3.14 

typedef struct {
	uint32_t flag;
	uint32_t id;
	char sim[13];							// "1064820873354"
	
	uint32_t diffpressure_VoltageLow;   	// mV
	uint32_t diffpressure_VoltageHigh;		// mV
	
	uint32_t diffpressure_ValueLow;			// Pa
	uint32_t diffpressure_ValueHigh;		// Pa
	
	uint32_t pressure_VoltageLow;			// mV
	uint32_t pressure_VoltageHigh;			// mV
	
	uint32_t pressure_ValueLow;				// Kpa
	uint32_t pressure_ValueHigh;			// Kpa
	
	uint32_t temp_CurrentLow;			// uA
	uint32_t temp_CurrentHigh;			// uA
	
	int32_t temp_ValueLow;				// ℃
	int32_t temp_ValueHigh;				// ℃
	
	float diffpressure_a;
	float diffpressure_b;
	float inputpressure_a;
	float inputpressure_b;

	// 卧式罐
	uint32_t diameter;						// mm
	uint32_t length;						// mm
	
	uint32_t level;							// nn
    uint32_t weight;						// kg
    uint32_t per;							// 百分比  * 1000
	uint32_t volume;						// L
	uint32_t maxVolume;						// L
	
	uint32_t InputPressure;					// 压力 KPa
	
	uint32_t alarm_LevelHighHigh;			// LNG液柱高高报警 mm
	uint32_t alarm_LevelHigh;				// LNG液柱高报警   mm
	uint32_t alarm_LevelLowLow;				// LNG液柱低低报警 mm
	uint32_t alarm_LevelLow;				// LNG液柱低报警   mm
	uint32_t alarm_InputPressureHigh;		// LNG压力高报警   kpa

	uint32_t alarm_TempLow;
	
	uint32_t alarm_Bits;
	
	uint32_t tank_type;
	uint32_t gas_index;
	uint32_t sendTimeIndex;
	
	uint32_t gprs_send_alarm_mask;			// 新奥报警     掩码
	uint32_t gprs_send_data_mask;			// 新奥发送数据 掩码
} Board_Info;

extern Board_Info g_board_info[TANK_MAX_LEN];

typedef struct {
	uint32_t dp_ad;
	uint32_t dp_vol;
	uint32_t dp_val;
	uint32_t p_ad;
	uint32_t p_vol;
	uint32_t p_val;
	uint32_t t_ad;
	uint32_t t_vol;
	uint32_t t_val;
	
	float dp_k;
	float p_k;
	float t_k;
	
	uint32_t level;
	uint32_t h20_mm;
	uint32_t weight;
} DebugInfo;
extern DebugInfo debugInfo[TANK_MAX_LEN];


enum sensor_num{
    DIFFPRESSURE_SENSOR = 0,
    INPUTPRESSURE_SENSOR,
	MAX_SENSOR,
    DIFFPRESSURE_SENSOR_1,
	TEMP_SENSOR,
};




#endif

