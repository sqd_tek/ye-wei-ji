#include <string.h>
#include "stm32f10x_flash.h"
#include "Download.h"
#include "stdio.h"
#include "FlashCrc.h"
#include "common.h"
#include "iwdg.h"
#include "delay.h"
#include "config.h"

#define d_printf D_PRINTF
extern int g_print_flag;

// Place for boot mode verification (better at the later of valid RAM)
#define BOOT_SIGN_ADDR 0x2000B000

// Default config data, const with bootloader
const PartitionInfo *def_info;
// Run time config data, effective only in each run-time application
PartitionInfo g_info;

static FunVoidType JumpToApplication;

extern u16 g_version_bak;

u16 calc_checksum(u16 init, u16 *buf, u32 len) {
    return init;
}

bool runable_address(u32 addr) {
    return (((*(vu32*)addr) & 0x2FFE0000 ) == 0x20000000) ? TRUE : FALSE;
}

bool valid_paritition_info(u32 addr) {
    if (*(u16*)addr != (BOOT_MAGIC & 0xFFFF))
        return FALSE;
    if (*(u16*)(addr + 2) != ((BOOT_MAGIC >> 16) & 0xFFFF))
        return FALSE;
    return TRUE;
}

// boot_enter and boot_done are to check failure times using RAM area.
// when warm reseting, those RAM data will be kept, so we can use that
// to count boot failure without hurting the flash life.

// Plant boot signature in RAM to indentify the boot method:
// copyfrom records from where partition A is copied from, so we know
// which partition should be deleted when the failure exceeds the max value.
static const char ParStr[3] = {'A', 'B', 'C'};
bool boot_enter(PartitionNum copyfrom) {
    PartitionInfo *para_info;

    if (!valid_paritition_info(BOOT_SIGN_ADDR)) {
        init_partition_info((PartitionInfo *)BOOT_SIGN_ADDR);
    }

    para_info = (PartitionInfo *)BOOT_SIGN_ADDR;
    if (para_info->failure == 0) {
        para_info->copyfrom = copyfrom;
    }

    para_info->failure ++;
    d_printf("*** partition %c failure count is %d\r\n",
           ParStr[para_info->copyfrom], para_info->failure);
    if (para_info->failure >= MAX_FAILURE) {
        para_info->failure = 0;
        d_printf("Delete partition %c\r\n", ParStr[para_info->copyfrom]);
        delete_partition(ParA);
        delete_partition(*(PartitionNum *)&para_info->copyfrom);
        return FALSE;
    }

    return TRUE;
}

// This method should be called when app boot successfully, or that boot
// will be regarded as a failure boot when its' reset,
// and the boot partition may be deleted!
void boot_done(void) {
    PartitionInfo *para_info;

    // If boot from cold reset, there is no boot signature
    if (!valid_paritition_info(BOOT_SIGN_ADDR)) {
        d_printf("Cold boot is done.\r\n");
        return;
    }

    para_info = (PartitionInfo *)BOOT_SIGN_ADDR;
    para_info->failure = 0;

    d_printf("Boot from partition %c successfully.\r\n", ParStr[para_info->copyfrom]);
}

// Vector Table Relocation address, this method should be called by application
// each time after booting.
// Noted that, before VTOR is set, d_printf should not be called.
void update_vtor(void) {

    SCB->VTOR = 0x800A000;//para_info.partitions[ParA].base;
}

// If it succeeds, it will not return.
void run_application(u32 addr, PartitionNum copyfrom) {
    if (runable_address(addr)) {
        // Use watchdog to monitor the booting failure.
        // Noted that, user app should reset or disable IWDG A.S.A.P.
        IWDG_Init();

        // check failure times, if max count exceeds, the partition will be deleted.
        if (!boot_enter(copyfrom)) {
            d_printf("Exceed max failure when booting partition %c\r\n", ParStr[copyfrom]);
            reset_by_iwdg();
        }

        d_printf("\r\nEntering user application\r\n");
        while (!IS_TIMEOUT_1MS(eTimUpdata, 50))
            ; // wait UART task is finished
//        BspClose();

        JumpToApplication = (FunVoidType)(*(vu32*) (addr + 4));
        /* Initialize user application's Stack Pointer */
        __set_MSP(*(vu32*)addr);
        JumpToApplication();
    }
}

void reset_by_iwdg(void) {
    IWDG_Init_3S();
    while(1)
        ;
}

// Whether the application partition is valid by CRC checking
bool valid_application(PartitionNum par) {
    u32 addr;
    u32 checksum;
    u32 len;
    PartitionInfo *info;
    u16 buffer[STM_SECTOR_SIZE / 2];

    if (par >= ParInvalid || def_info == NULL)
        return FALSE;

    // Specific partition info
    info = (PartitionInfo *)(def_info->partitions[par].info);

    len = info->partitions[par].len;
    if (info->magic != BOOT_MAGIC)
        return FALSE;
    if (len == 0 || len == 0xFFFFFFFF)
        return FALSE;

    // CRC checking
    checksum = 0;
    addr = info->partitions[par].base;
    while (len >= STM_SECTOR_SIZE) {
        STMFLASH_Read(addr, buffer, STM_SECTOR_SIZE / 2);
        checksum = GenerateCrc32(checksum, (u8 *)buffer, STM_SECTOR_SIZE);
        addr += STM_SECTOR_SIZE;
        len -= STM_SECTOR_SIZE;
    }

    // The last page may be not full
    if (len > 0) {
        STMFLASH_Read(addr, buffer, len / 2);
        addr += len;
        // In case the length is odd
        if ((len & 1) != 0) {
            STMFLASH_Read(addr, &buffer[len / 2], 1);
            buffer[len / 2] &= 0x00FF;
            len++;
        }
        checksum = GenerateCrc32(checksum, (u8 *)buffer, len);
    }
	
    return (checksum == info->checksum) ? TRUE : FALSE;
}

void init_partition_info(PartitionInfo *info) {
    memset((void *)info, 0, sizeof(PartitionInfo));

    info->magic = BOOT_MAGIC;
    info->version = g_version;  //  TODO: application version

    info->partitions[ParA].base = PartitionA;
    info->partitions[ParA].info= PartitionAInfo;
    info->partitions[ParA].len = PartitionAInfo - PartitionA;
    info->partitions[ParB].base = PartitionB;
    info->partitions[ParB].info= PartitionBInfo;
    info->partitions[ParB].len = PartitionBInfo - PartitionB;
    info->partitions[ParC].base = PartitionC;
    info->partitions[ParC].info= PartitionCInfo;
    info->partitions[ParC].len = PartitionCInfo - PartitionC;
}

bool save_partition_info(PartitionNum par, PartitionInfo *info) {
	d_printf("++save_partition_info++\r\n");
    if (par >= ParInvalid || info == NULL)
        return FALSE;

    FLASH_Unlock();
    FLASH_ErasePage(def_info->partitions[par].info);
    STMFLASH_Write_NoCheck(def_info->partitions[par].info, (u16*)info,
                           sizeof(PartitionInfo) / 2);
    FLASH_Lock();
	d_printf("--save_partition_info--\r\n");
    return TRUE;
}

bool load_partition_info(PartitionNum par, PartitionInfo *info) {
    u16 buffer[STM_SECTOR_SIZE / 2];
    if (par >= ParInvalid || info == NULL)
        return FALSE;
    memset((void *)buffer, 0, STM_SECTOR_SIZE);
    STMFLASH_Read(def_info->partitions[par].info, buffer,
                  STM_SECTOR_SIZE / 2);
    if (buffer[0] == (BOOT_MAGIC & 0xFFFF)) {
        memcpy((void *)info, buffer, sizeof(PartitionInfo));
        return TRUE;
    } else {
        return FALSE;
    }
}


bool delete_partition(PartitionNum par) {
    u32 addr = def_info->partitions[par].base;
    u32 len = def_info->partitions[par].len;

    if (par >= ParInvalid)
        return FALSE;

    FLASH_Unlock();
    while (len >= STM_SECTOR_SIZE) {
        FLASH_ErasePage(addr);
        addr += STM_SECTOR_SIZE;
        len -= STM_SECTOR_SIZE;
    }
    // the last page
    if (len > 0) {
        FLASH_ErasePage(addr);
    }
    FLASH_Lock();

    return TRUE;
}

static PartitionNum gprs_par = ParB;
PartitionNum gprs_update_init(void) {
    if (!valid_application(ParC))
        gprs_par = ParC;
    else
        gprs_par = ParB;
    load_partition_info(gprs_par, &g_info);
    if (g_info.magic != BOOT_MAGIC) {
		d_printf("g_info.magic != BOOT_MAGIC\r\n");
		init_partition_info(&g_info);
    }
    g_info.failure = 0;
    g_info.writep = g_info.partitions[gprs_par].base;
    g_info.app_len = 0;
    iap_write_appbin(g_info.partitions[gprs_par].base, NULL, 0, D_START);
	return gprs_par;
}

int gprs_get_data_callback(char* buf, int len) {
    g_info.checksum = GenerateCrc32(g_info.checksum, (u8*)buf, len);
    g_info.writep += len;
    g_info.app_len += len;
    iap_write_appbin(g_info.partitions[gprs_par].base, (u8*)buf, len, D_DATA);
    return len;
}

void gprs_update_done(void) {
	d_printf("++gprs_update_done++\r\n");
    iap_write_appbin(g_info.partitions[gprs_par].base, NULL, 0, D_STOP);
    g_info.timestamp = 0; //  TODO: current time
    g_info.partitions[gprs_par].len = g_info.app_len;
    g_info.version = g_version_bak;
    save_partition_info(gprs_par, &g_info);
    d_printf("gprs_par=%d. len=%u\r\n", gprs_par, g_info.app_len);
    d_printf("writep=%u, base=%u\r\n", g_info.writep, g_info.partitions[gprs_par].base);
	d_printf("--gprs_update_done--\r\n");
}

void init_factory_info(void) {
    if (!valid_paritition_info(SysConfigAddr)) {
        d_printf("Prepare default config at the first running\r\n");
        init_partition_info(&g_info);

        FLASH_Unlock();
        FLASH_ErasePage(SysConfigAddr);
        STMFLASH_Write_NoCheck(SysConfigAddr, (u16*)&g_info,
                               sizeof(g_info) / 2);
        FLASH_Lock();
    }
    def_info = (PartitionInfo *)(SysConfigAddr);
}


