#include "stm32f10x_flash.h"

#include "Download.h"
#include "common.h"
#include "stdio.h"


static u32 write_addr;  // next address to be written
static u16 write_len;   // length of valid data in iapbuf
static u16 iapbuf[1028];

void STMFLASH_Write_NoCheck(u32 WriteAddr,u16 *pBuffer,u16 NumToWrite) {
    u16 i;
    for (i = 0; i < NumToWrite; i++) {
        FLASH_ProgramHalfWord(WriteAddr, pBuffer[i]);
        WriteAddr += 2;
    }
}
void STMFLASH_Write(u32 WriteAddr, u16 *pBuffer, u16 NumToWrite) {
    u32 secpos;    //扇区地址
    u16 secoff;    //扇区内偏移地址(16位字计算)
    u16 secremain; //扇区内剩余地址(16位字计算)
    u16 i;
    u32 offaddr;   //去掉0X08000000后的地址
    u16 STMFLASH_BUF[STM_SECTOR_SIZE / 2];  // 最多是2K字节

    if (WriteAddr < STM32_FLASH_BASE || (WriteAddr >= (STM32_FLASH_BASE + 1024 * STM32_FLASH_SIZE)))
        return;  // 非法地址

    FLASH_Unlock();             //解锁
    offaddr = WriteAddr - STM32_FLASH_BASE;     //实际偏移地址.
    secpos = offaddr / STM_SECTOR_SIZE;         //扇区地址  0~127 for STM32F103RBT6
    secoff = (offaddr % STM_SECTOR_SIZE) / 2;   //在扇区内的偏移(2个字节为基本单位.)
    secremain = STM_SECTOR_SIZE / 2 - secoff;   //扇区剩余空间大小
    if (NumToWrite <= secremain)
        secremain = NumToWrite;                 // 不大于该扇区范围

    while (TRUE) {
        STMFLASH_Read(secpos * STM_SECTOR_SIZE + STM32_FLASH_BASE,
                      STMFLASH_BUF, STM_SECTOR_SIZE / 2);  // 读出整个扇区的内容
        for (i = 0; i < secremain; i++) {                   // 校验数据
            if(STMFLASH_BUF[secoff + i] != 0XFFFF)
                break;  // 需要擦除
        }
        if (i < secremain)  {  //需要擦除
            FLASH_ErasePage(secpos * STM_SECTOR_SIZE + STM32_FLASH_BASE);  // 擦除这个扇区
            for (i = 0; i < secremain; i++) {  // 复制
                STMFLASH_BUF[i + secoff] = pBuffer[i];
            }
            STMFLASH_Write_NoCheck(secpos * STM_SECTOR_SIZE + STM32_FLASH_BASE,
                                   STMFLASH_BUF, STM_SECTOR_SIZE / 2);  // 写入整个扇区
        } else {
            STMFLASH_Write_NoCheck(WriteAddr, pBuffer, secremain);  // 写已经擦除了的,直接写入扇区剩余区间.
        }

        if (NumToWrite == secremain) {
            break;  // 写入结束了
        } else  {
            //写入未结束
            secpos ++;                  // 扇区地址增1
            secoff = 0;                 // 偏移位置为0
            pBuffer += secremain;       // 指针偏移
            WriteAddr += secremain * 2; // 写地址偏移
            NumToWrite -= secremain;    // 字节(16位)数递减
            if(NumToWrite > (STM_SECTOR_SIZE / 2))
                secremain = STM_SECTOR_SIZE / 2;  // 下一个扇区还是写不完
            else
                secremain = NumToWrite;           // 下一个扇区可以写完了
        }
    }
    FLASH_Lock();  // 上锁
}

void iap_write_appbin(u32 addr, u8 *buf, u32 len, AppWriteStage stage) {
    u32 t;
    u16 val;
    u8 *dfu = buf;

    if (stage == D_START) {
        write_addr = addr;
        write_len = 0;
    }

    FLASH_Unlock();
    for (t = 0; t < len; t += 2) {
        val  = (u16)dfu[1] << 8;
        val += (u16)dfu[0];
        dfu += 2;
        iapbuf[write_len++] = val;
        if (write_len == STM_SECTOR_SIZE / 2) {
            FLASH_ErasePage(write_addr);
            STMFLASH_Write_NoCheck(write_addr, iapbuf, STM_SECTOR_SIZE / 2);
            write_len = 0;
            write_addr += STM_SECTOR_SIZE;
        }
    }

    if (stage == D_STOP) {
        if (write_len > 0) {
            FLASH_ErasePage(write_addr);
            STMFLASH_Write_NoCheck(write_addr, iapbuf, write_len);
            write_addr += write_len * 2;
            write_len = 0;
        }
    }
    FLASH_Lock();
}

void FLASH_ProgramStart(void) {
}

void FLASH_ProgramDone(void) {
}

void STMFLASH_Read(u32 ReadAddr,u16 *pBuffer,u16 NumToRead) {
    u16 i;
    for (i = 0; i < NumToRead; i++) {
        pBuffer[i] = *(vu16*)ReadAddr;  // 读取2个字节.
        ReadAddr += 2;                  // 偏移2个字节.
    }
}

