#include "usmart.h"
#include "usmart_str.h"
//////////////////////////////////////////////////////////////////////////////////	 
//本程序只供学习使用，未经作者许可，不得用于其它任何用途
//ALIENTEK STM32开发板	   
//正点原子@ALIENTEK
//技术论坛:www.openedv.com
//创建日期:2011/6/13
//版本：V2.8
//版权所有，盗版必究。
//Copyright(C) 正点原子 2011-2021
//All rights reserved
//********************************************************************************
//升级说明
//V1.4
//增加了对参数为string类型的函数的支持.适用范围大大提高.
//优化了内存占用,静态内存占用为79个字节@10个参数.动态适应数字及字符串长度
//V2.0 
//1,修改了list指令,打印函数的完整表达式.
//2,增加了id指令,打印每个函数的入口地址.
//3,修改了参数匹配,支持函数参数的调用(输入入口地址).
//4,增加了函数名长度宏定义.	
//V2.1 20110707		 
//1,增加dec,hex两个指令,用于设置参数显示进制,及执行进制转换.
//注:当dec,hex不带参数的时候,即设定显示参数进制.当后跟参数的时候,即执行进制转换.
//如:"dec 0XFF" 则会将0XFF转为255,由串口返回.
//如:"hex 100" 	则会将100转为0X64,由串口返回
//2,新增usmart_get_cmdname函数,用于获取指令名字.
//V2.2 20110726	
//1,修正了void类型参数的参数统计错误.
//2,修改数据显示格式默认为16进制.
//V2.3 20110815
//1,去掉了函数名后必须跟"("的限制.
//2,修正了字符串参数中不能有"("的bug.
//3,修改了函数默认显示参数格式的修改方式. 
//V2.4 20110905
//1,修改了usmart_get_cmdname函数,增加最大参数长度限制.避免了输入错误参数时的死机现象.
//2,增加USMART_ENTIM2_SCAN宏定义,用于配置是否使用TIM2定时执行scan函数.
//V2.5 20110930
//1,修改usmart_init函数为void usmart_init(u8 sysclk),可以根据系统频率自动设定扫描时间.(固定100ms)
//2,去掉了usmart_init函数中的uart_init函数,串口初始化必须在外部初始化,方便用户自行管理.
//V2.6 20111009
//1,增加了read_addr和write_addr两个函数.可以利用这两个函数读写内部任意地址(必须是有效地址).更加方便调试.
//2,read_addr和write_addr两个函数可以通过设置USMART_USE_WRFUNS为来使能和关闭.
//3,修改了usmart_strcmp,使其规范化.			  
//V2.7 20111024
//1,修正了返回值16进制显示时不换行的bug.
//2,增加了函数是否有返回值的判断,如果没有返回值,则不会显示.有返回值时才显示其返回值.
//V2.8 20111116
//1,修正了list等不带参数的指令发送后可能导致死机的bug.
/////////////////////////////////////////////////////////////////////////////////////
////////////////////////////用户配置区///////////////////////////////////////////////
//这下面要包含所用到的函数所申明的头文件(用户自己添加) 
#include "delay.h"	
#include "hal_senser.h"
#include "led.h"
#include "hal_eeprom.h"
#include "config.h"

#define d_printf D_PRINTF
extern int g_print_flag;

void print_hello(void);
void exit_hello(void);
int test(int8_t signNum, int8_t f) {
	return (int)(signNum+f);
}
//extern void PWM_DAC_Set(u16 vol);												
//函数名列表初始化(用户自己添加)
//用户直接在这里输入要执行的函数名及其查找串
struct _m_usmart_nametab usmart_nametab[]=
{
#if USMART_USE_WRFUNS == 1 	//如果使能了读写操作
	(void *)read_addr,  			"u32 read_addr(u32 addr)",
	(void *)write_addr, 			"void write_addr(u32 addr,u32 val)",	 
#endif		   
	(void *)print_hello, 			"void print_hello(void)",
	(void *)exit_hello,				"void exit_hello(void)",
	(void *)AD_SetCfg, 				"void AD_SetCfg(int num, int channel_num, int mode, int pos)",
	(void *)AD_GetCfg,	 			"int AD_GetCfg(int num, int n)",
	(void *)AD_ChannelVoltage,		"int AD_ChannelVoltage(int channel_num, int mode, int pos)",
	(void *)calc_diffpressureK,		"void calc_diffpressureK(int dp1, int dp2, int cur1, int cur2)",
//	(void *)calc_pressureK,			"void calc_pressureK(int p1, int p2, int cur1, int cur2)",
//	(void *)calc_tempK, 			"void calc_tempK(int t1, int t2, int cur1, int cur2)",
//	(void *)AD_SetValue,			"void AD_SetValue(enum sensor_num sensor, int pos, int value)",
	(void *)eeprom_SetBoardDefault, "void eeprom_SetBoardDefault(void)",
	(void *)eeprom_SaveBoardInfo, 	"void eeprom_SaveBoardInfo(void)",
	(void *)eeprom_GetBoardInfo, 	"void eeprom_GetBoardInfo(void)",
	(void *)eeprom_Read32Bits, 		"uint32_t eeprom_Read32Bits(uint32_t ReadAddr)",
	(void *)eeprom_Write32Bits, 	"void eeprom_Write32Bits(uint32_t WriteAddr, uint32_t data)",
//	(void *)AD_SetVoltageValue, 	"void AD_SetVoltageValue(enum sensor_num sensor, int pos, int value)",
	(void *)AD_SetLength,			"void AD_SetLength(int len)",
	(void *)AD_GetLength,			"int AD_GetLength(void)",
	(void *)AD_SetDiameter,			"void AD_SetDiameter(int Diameter)",
	(void *)AD_GetDiameter,			"int AD_GetDiameter(void)",
	(void *)AD_SetAB,				"void AD_SetAB(int serson_num, int a, int b)",
	(void *)AD_getAB,				"void AD_getAB(void)",
	(void *)AD_SetRes,				"void AD_SetRes(int num, int res)",
	(void *)AD_GetRes,				"int AD_GetRes(int num)",
	(void *)readID,					"void readID(void)",
	(void *)writeID,				"void writeID(uint32_t id)",
//	(void *)getTankNumID,			"int getTankNumID(void)",
//	(void *)setTankNumID,			"void setTankNumID(int id)",
	(void *)getMask, 				"uint32_t getMask(int id, int mask_mode)",
	(void *)setMask,				"void setMask(int id, int mask_mode, uint32_t mask_value)",
	(void *)getMaxVolume,			"int getMaxVolume(void)",
	(void *)setMaxVolume,			"void setMaxVolume(int maxVolume)",
	(void *)setThreshold,			"void setThreshold(int levelHH, int levelH, int levelL, int levelLL, int pressureH)",
	(void *)getThreshold,			"void getThreshold(void)",
	(void *)AD_SetTankType,			"void AD_SetTankType(int tanktype)",
	(void *)AD_GetTankType,			"int AD_GetTankType(void)",
	(void *)test,					"int test(int8_t signNum, int8_t f)",
};						  
///////////////////////////////////END///////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////
//函数控制管理器初始化
//得到各个受控函数的名字
//得到函数总数量
struct _m_usmart_dev usmart_dev=
{
	usmart_nametab,
	usmart_init,
	usmart_cmd_rec,
	usmart_exe,
	usmart_scan,
	sizeof(usmart_nametab)/sizeof(struct _m_usmart_nametab),//函数数量
	0,	  	//参数数量
	0,	 	//函数ID
	0,		//参数显示类型,0,10进制;1,16进制
	0,		//参数类型.bitx:,0,数字;1,字符串	    
	0,	  	//每个参数的长度暂存表,需要MAX_PARM个0初始化
	0,		//函数的参数,需要PARM_LEN个0初始化
};   



void print_hello(void) {
	g_print_flag = 1;
	
	d_printf("display hello people\r\n");
}



void exit_hello(void) {
	d_printf("not display\r\n");
	g_print_flag = 0;
	d_printf("not display faild\r\n g_print_flag:%d\r\n", g_print_flag);
}












