#include "oled.h"
#include "stdlib.h"
#include "oledfont.h"
#include "uart.h"
#include "GT20.h"
#include "Los_Task.h"
//////////////////////////////////////////////////////////////////////////////////	 
//本程序只供学习使用，未经作者许可，不得用于其它任何用途
//ALIENTEK战舰STM32开发板
//ALIENTEK OLED模块驱动代码	   
//正点原子@ALIENTEK
//技术论坛:www.openedv.com
//修改日期:2012/9/5
//版本：V1.0
//版权所有，盗版必究。
//Copyright(C) 广州市星翼电子科技有限公司 2009-2019
//All rights reserved									  
//////////////////////////////////////////////////////////////////////////////////	

//OLED的显存
//存放格式如下.
//[0]0 1 2 3 ... 127	
//[1]0 1 2 3 ... 127	
//[2]0 1 2 3 ... 127	
//[3]0 1 2 3 ... 127	
//[4]0 1 2 3 ... 127	
//[5]0 1 2 3 ... 127	
//[6]0 1 2 3 ... 127	
//[7]0 1 2 3 ... 127 			   
u8 OLED_GRAM[128][8];	 

void OLED_delay_ms(uint16_t ms) {
	delay_ms(ms); // 由于在
}

//更新显存到LCD		 
void OLED_Refresh_Gram(void)
{
	u8 i,n;		    
	for(i=0;i<8;i++)  
	{  
		OLED_WR_Byte (0xb0+i,OLED_CMD);    //设置页地址（0~7）
		OLED_WR_Byte (0x00,OLED_CMD);      //设置显示位置—列低地址
		OLED_WR_Byte (0x10,OLED_CMD);      //设置显示位置—列高地址   
		for(n=0;n<128;n++)OLED_WR_Byte(OLED_GRAM[n][i],OLED_DATA); 
	}       
}

void LCD_displayInt4824(u8 x, u8 y, uint32_t displayInt) {
	int t1, i, j = 0;
	u8 y0;
	u8 size = 48;
	u8 temp;
	int t2 = 0;
	
	t2 = x << 4;
	x = y << 4;
	y = t2;
	y0=y;
	
	while (j < 5) {
		t2 = displayInt / my_pow(10, 4-j) % 10; 
		if (t2 != 0 || j >= 4) { // 保证最后一位肯定有数据(0)
			break;
		}
		j++;
		x += 24;
	}
	for (;j < 5;j++) {
		t2 = displayInt / my_pow(10, 4-j) % 10;
		for (i = 0;i < 48*24/8;i++) {
			temp = oled_asc2_4824[t2][i];
			for(t1=0;t1<8;t1++)
			{
				if(temp&0x80)OLED_DrawPoint(x,y,1);
				else OLED_DrawPoint(x,y,0);
				temp<<=1;
				y++;
				if((y-y0)==size)
				{
					y=y0;
					x++;
					break;
				}
			}
		}
	}
}



//向SSD1306写入一个字节。
//dat:要写入的数据/命令
//cmd:数据/命令标志 0,表示命令;1,表示数据;
void OLED_WR_Byte(u8 dat,u8 cmd)
{	
	u8 i;			  
	if(cmd)
	  OLED_RS_Set();
	else 
	  OLED_RS_Clr();		  
	OLED_CS_Clr();
	for(i=0;i<8;i++)
	{			  
		OLED_SCLK_Clr();
		if(dat&0x80)
		   OLED_SDIN_Set();
		else 
		   OLED_SDIN_Clr();
		OLED_SCLK_Set();
		dat<<=1;   
	}				 		  
	OLED_CS_Set();
	OLED_RS_Set();   	  
} 
	  	  
//开启OLED显示    
void OLED_Display_On(void)
{
	OLED_WR_Byte(0X8D,OLED_CMD);  //SET DCDC命令
	OLED_WR_Byte(0X14,OLED_CMD);  //DCDC ON
	OLED_WR_Byte(DISPLAY_ON,OLED_CMD);  //DISPLAY ON
}
//关闭OLED显示     
void OLED_Display_Off(void)
{
	OLED_WR_Byte(0X8D,OLED_CMD);  //SET DCDC命令
	OLED_WR_Byte(0X10,OLED_CMD);  //DCDC OFF
	OLED_WR_Byte(DISPLAY_OFF,OLED_CMD);  //DISPLAY OFF
}		   			 
//清屏函数,清完屏,整个屏幕是黑色的!和没点亮一样!!!	  
void OLED_Clear(void)  
{  
	u8 i,n;  
	for(i=0;i<8;i++)for(n=0;n<128;n++)OLED_GRAM[n][i]=0X00;  
	OLED_Refresh_Gram();//更新显示
}
//画点 
//x:0~127
//y:0~63
//t:1 填充 0,清空				   
void OLED_DrawPoint(u8 x,u8 y,u8 t)
{
	u8 pos,bx,temp=0;
	if(x>127||y>63)return;//超出范围了.
	pos=7-y/8;
	bx=y%8;
	temp=1<<(7-bx);
	if(t)OLED_GRAM[x][pos]|=temp;
	else OLED_GRAM[x][pos]&=~temp;	    
}
//x1,y1,x2,y2 填充区域的对角坐标
//确保x1<=x2;y1<=y2 0<=x1<=127 0<=y1<=63	 	 
//dot:0,清空;1,填充	  
void OLED_Fill(u8 x1,u8 y1,u8 x2,u8 y2,u8 dot)  
{  
	u8 x,y;  
	for(x=x1;x<=x2;x++)
	{
		for(y=y1;y<=y2;y++)OLED_DrawPoint(x,y,dot);
	}													    
	OLED_Refresh_Gram();//更新显示
}
//在指定位置显示一个字符,包括部分字符
//x:0~127
//y:0~63
//mode:0,反白显示;1,正常显示				 
//size:选择字体 16/12 
void OLED_ShowChar(u8 x,u8 y,u8 chr,u8 size,u8 mode)
{
	u8 temp,t,t1;
	u8 y0=y;
	chr=chr-' ';//得到偏移后的值				   
    for(t=0;t<size;t++)
    {   
		if(size==12)temp=oled_asc2_1206[chr][t];  //调用1206字体
		else temp=oled_asc2_1608[chr][t];		 //调用1608字体 	                          
        for(t1=0;t1<8;t1++)
		{
			if(temp&0x80)OLED_DrawPoint(x,y,mode);
			else OLED_DrawPoint(x,y,!mode);
			temp<<=1;
			y++;
			if((y-y0)==size)
			{
				y=y0;
				x++;
				break;
			}
		}  	 
    }          
}
//m^n函数
u32 oled_pow(u8 m,u8 n)
{
	u32 result=1;	 
	while(n--)result*=m;    
	return result;
}				  
//显示2个数字
//x,y :起点坐标	 
//len :数字的位数
//size:字体大小
//mode:模式	0,填充模式;1,叠加模式
//num:数值(0~4294967295);	 		  
void OLED_ShowNum(u8 x,u8 y,u32 num,u8 len,u8 size)
{         	
	u8 t,temp;
	u8 enshow=0;						   
	for(t=0;t<len;t++)
	{
		temp=(num/oled_pow(10,len-t-1))%10;
		if(enshow==0&&t<(len-1))
		{
			if(temp==0)
			{
				OLED_ShowChar(x+(size/2)*t,y,' ',size,1);//判断数字前0并显示为空格
				continue;
			}else enshow=1; 
		 	 
		}
	 	OLED_ShowChar(x+(size/2)*t,y,temp+'0',size,1); 
	}
} 
//显示字符串
//x,y:起点坐标  
//*p:字符串起始地址
//用16字体
void OLED_ShowString(u8 x,u8 y,const u8 *p)
{
#define MAX_CHAR_POSX 122
#define MAX_CHAR_POSY 58          
    while(*p!='\0')
    {       
        if(x>MAX_CHAR_POSX){x=0;y+=16;}
        if(y>MAX_CHAR_POSY){y=x=0;OLED_Clear();}
        OLED_ShowChar(x,y,*p,16,1);	 
        x+=8;
        p++;
    }  
}	   



/***************************************************
 * start		: 循环开始行
 * stop 		: 循环结束行
 * LeftOrRight 	: 循环模式  1：>>>>>>>>  从左至右 
 * 				:			0: <<<<<<<<  从右至左
 * 开始滚动之后，不可以进行屏幕读写，否则会导致屏幕停止滚动。
 * 在进行屏幕读写之前，停止滚动，放置对RAM进行破坏
***************************************************/
void OLED_Srcoll(u8 start, u8 stop, u8 LeftOrRight, u8 DownFreq) {
	int downFlag =  0;
	
	if ((start > 7) || (stop > 7)) {
		printf("oled srcoll column out of range\r\n");
		return ;
	}
	
	if (DownFreq > SRCOLL_ROW_MAX) {
		printf("oled Line to refresh the screen out of range\r\n");
		return ;	
	}
	
	OLED_WR_Byte(DEACTIVATE_SCROLL, OLED_CMD);				// 关闭滚动
	if (DownFreq > 0) {
		downFlag = 3;
	}
	if (LeftOrRight) {
		OLED_WR_Byte(HOARIZONTAL_SCROLL_RIGHT + downFlag, OLED_CMD);	// 向右滚动
	} else {
		OLED_WR_Byte(HOARIZONTAL_SCROLL_LEFT + downFlag, OLED_CMD);	// 向左滚动
	}
	
	OLED_WR_Byte(0X00, OLED_CMD);							// 空字节
	OLED_WR_Byte(7 - stop, OLED_CMD);						// 开始滚动行
	OLED_WR_Byte(SRCOLL_TIME_5_FRAMES, OLED_CMD);			// 滚屏的时间间隔
	OLED_WR_Byte(7 - start, OLED_CMD);						// 结束滚动行  必须大于开始滚动行
	OLED_WR_Byte(DownFreq, OLED_CMD);							// 垂直滚动开关
	OLED_WR_Byte(0xFF, OLED_CMD);							// 垂直滚动开关
	OLED_WR_Byte(ACTIVATE_SCROLL, OLED_CMD);				// 开始滚动
}
void OLED_RightSrcoll(u8 start, u8 stop) {
	OLED_Srcoll(start, stop, SRCOLL_RIGHT, SRCOLL_ROW_OFF);
}
void OLED_LeftSrcoll(u8 start, u8 stop) {
	OLED_Srcoll(start, stop, SRCOLL_LEFT, SRCOLL_ROW_OFF);
}

void OLED_DisplayHanzi(u8 x, u8 y, u8 msb, u8 msl, u8 size, u8 mode) {
	char str16[32];
	int t1, i;
	u8 y0=y;
	u8 temp;
	
	S1Y_gt16_GetData(msb,msl, str16);
	
	for (i = 0;i < size*size/8/2;i++) {
		temp = str16[i];
		for(t1=0;t1<8;t1++)
		{
			if(temp&0x01)OLED_DrawPoint(x,y,mode);
			else OLED_DrawPoint(x,y,!mode);
			temp>>=1;
			y++;
			if((y-y0)==size)
			{
				y=y0;
				x++;
				break;
			}
		}
		temp = str16[i+size*size/8/2];
		for(t1=0;t1<8;t1++)
		{
			if(temp&0x01)OLED_DrawPoint(x,y,mode);
			else OLED_DrawPoint(x,y,!mode);
			temp>>=1;
			y++;
			if((y-y0)==size)
			{
				y=y0;
				x++;
				break;
			}
		}
	}
	//printf("\r\n");
}
void OLED_DisplayHanziStr(u8 x, u8 y, u8 size, u8 mode, char * disStr) {
	u8 i;
	u8 len = strlen(disStr);
	u8 msb, msl;
	
	for (i = 0;i < len;i+=2) {
		msb = disStr[i];
		msl = disStr[i + 1];
		OLED_DisplayHanzi(x, i, msb, msl, size, mode);
	}
}
// Angle Image Size:16*16
void OLED_DisplayAngle(u8 x, u8 y, u8 mode, u8 angle) {
	u8 temp,t,t1;
	u8 y0 = y;
	u8 size = 16;
	for(t=0;t<16*16/8;t++)
    {   
		temp=oled_asc2_angle[angle][t];		 //调用1608字体 	                          
        for(t1=0;t1<8;t1++)
		{
			if(temp&0x80)OLED_DrawPoint(x,y,mode);
			else OLED_DrawPoint(x,y,!mode);
			temp<<=1;
			y++;
			if((y-y0)==size)
			{
				y=y0;
				x++;
				break;
			}
		}  	 
    }       
}
void OLED_DisplayBatteryStatus(u8 x, u8 y, u8 mode, u8 batteryStatus) {
	u8 temp,t,t1;
	u8 y0 = y;
	u8 size = 16;
	
    for(t=0;t<64;t++)
    {
		temp=oled_asc2_battery[batteryStatus][t];		 //调用1608字体 	                          
        for(t1=0;t1<8;t1++)
		{
			if(temp&0x80)OLED_DrawPoint(x,y,mode);
			else OLED_DrawPoint(x,y,!mode);
			temp<<=1;
			y++;
			if((y-y0)==size)
			{
				y=y0;
				x++;
				break;
			}
		}  	 
    }   
	
}
void OLED_DisplayLOGOImage(void) {
	u16 temp,t,t1;
	u16 y0 = 0;
	u16 x = 0, y = 0;
	u16 mode = 1;
	u8 size = 64;
	printf("asdfgh\r\n");
    for(t=0;t<sizeof(oled_image_logo_128_64);t++)
    {
		temp=oled_image_logo_128_64[t];		 //调用1608字体 	                          
        for(t1=0;t1<8;t1++)
		{
			if(temp&0x80)OLED_DrawPoint(x,y,mode);
			else OLED_DrawPoint(x,y,!mode);
			temp<<=1;
			y++;
			if((y-y0)==size)
			{
				y=y0;
				x++;
				break;
			}
		}
    }
}
void __test(void) {
	char str16[32];
	int t1, i, j = 0;
	u8 x = 0, y = 0;
	u8 y0=y;
	u8 size = 48;
	u8 temp;
	u8 msl, msb;
	
	x = 0, y = 16;
	y0=y;
	for (j = 0;j < 5;j++)
	for (i = 0;i < 48*24/8;i++) {
		temp = oled_asc2_4824[j][i];
		printf("%02x ", temp);
		for(t1=0;t1<8;t1++)
		{
			if(temp&0x80)OLED_DrawPoint(x,y,1);
			else OLED_DrawPoint(x,y,0);
			temp<<=1;
			y++;
			if((y-y0)==size)
			{
				y=y0;
				x++;
				break;
			}
		}
	}
	printf("\r\n");
}
//初始化SSD1306					    
void OLED_Init(void)
{ 	
 	GPIO_InitTypeDef  GPIO_InitStructure;
 	
 	RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOA|RCC_APB2Periph_GPIOB|RCC_APB2Periph_GPIOD|RCC_APB2Periph_AFIO, ENABLE);	 //使能PC,D,G端口时钟
 //??PA15?
    GPIO_PinRemapConfig(GPIO_Remap_SWJ_JTAGDisable,ENABLE);

        
 #if OLED_MODE==1
 
 	GPIO_InitStructure.GPIO_Pin =0xFF; //PC0~7 OUT推挽输出
 	GPIO_Init(GPIOC, &GPIO_InitStructure);
 	GPIO_SetBits(GPIOC,0xFF); //PC0~7输出高

 	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_13|GPIO_Pin_14|GPIO_Pin_15;				 //PG13,14,15 OUT推挽输出
 	GPIO_Init(GPIOG, &GPIO_InitStructure);
 	GPIO_SetBits(GPIOG,GPIO_Pin_13|GPIO_Pin_14|GPIO_Pin_15);						 //PG13,14,15 OUT  输出高

 #else
	
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_2;	 //PD3,PD6推挽输出  
 	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_Out_PP; 		 //推挽输出
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;//速度50MHz
 	GPIO_Init(GPIOD, &GPIO_InitStructure);	  //初始化GPIOD3,6
 	GPIO_SetBits(GPIOD,GPIO_Pin_2);	//PD3,PD6 输出高

	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_3|GPIO_Pin_5|GPIO_Pin_9;				 //PG15 OUT推挽输出	  RST
 	GPIO_Init(GPIOB, &GPIO_InitStructure);
 	//GPIO_SetBits(GPIOB,GPIO_Pin_3|GPIO_Pin_5|GPIO_Pin_9);						 //PG15 OUT  输出高

	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_15|GPIO_Pin_11;				 //PG15 OUT推挽输出	  RST
 	GPIO_Init(GPIOA, &GPIO_InitStructure);
 	GPIO_SetBits(GPIOA,GPIO_Pin_15|GPIO_Pin_11);						 //PG15 OUT  输出高


 #endif
  							  
	OLED_RST_Clr();
	OLED_delay_ms(100);
	OLED_RST_Set(); 
	OLED_WR_Byte(DISPLAY_ON,OLED_CMD); //开启显示	 
					  
	OLED_WR_Byte(DISPLAY_OFF,OLED_CMD); //关闭显示
	OLED_WR_Byte(0xD5,OLED_CMD); //设置时钟分频因子,震荡频率
	OLED_WR_Byte(80,OLED_CMD);   //[3:0],分频因子;[7:4],震荡频率
	OLED_WR_Byte(0xA8,OLED_CMD); //设置驱动路数
	OLED_WR_Byte(0X3F,OLED_CMD); //默认0X3F(1/64) 
	OLED_WR_Byte(0xD3,OLED_CMD); //设置显示偏移
	OLED_WR_Byte(0X00,OLED_CMD); //默认为0

	OLED_WR_Byte(0x40,OLED_CMD); //设置显示开始行 [5:0],行数.
													    
	OLED_WR_Byte(0x8D,OLED_CMD); //电荷泵设置
	OLED_WR_Byte(0x14,OLED_CMD); //bit2，开启/关闭
	OLED_WR_Byte(0x20,OLED_CMD); //设置内存地址模式
	OLED_WR_Byte(0x02,OLED_CMD); //[1:0],00，列地址模式;01，行地址模式;10,页地址模式;默认10;
	OLED_WR_Byte(0xA1,OLED_CMD); //段重定义设置,bit0:0,0->0;1,0->127;
	OLED_WR_Byte(0xC0,OLED_CMD); //设置COM扫描方向;bit3:0,普通模式;1,重定义模式 COM[N-1]->COM0;N:驱动路数
	OLED_WR_Byte(0xDA,OLED_CMD); //设置COM硬件引脚配置
	OLED_WR_Byte(0x12,OLED_CMD); //[5:4]配置
		 
	OLED_WR_Byte(CONTRAST,OLED_CMD); //对比度设置
	OLED_WR_Byte(0xD9,OLED_CMD); //设置预充电周期
	OLED_WR_Byte(0xf1,OLED_CMD); //[3:0],PHASE 1;[7:4],PHASE 2;
	OLED_WR_Byte(0xDB,OLED_CMD); //设置VCOMH 电压倍率
	OLED_WR_Byte(0x30,OLED_CMD); //[6:4] 000,0.65*vcc;001,0.77*vcc;011,0.83*vcc;

	OLED_WR_Byte(TOTAL_DISPLAY_OFF, OLED_CMD); //全局显示开启;bit0:1,开启;0,关闭;(白屏/黑屏)
	OLED_WR_Byte(NORMAL_DISPLAY, OLED_CMD); //设置显示方式;bit0:1,反相显示;0,正常显示	    						   
	OLED_WR_Byte(DISPLAY_ON, OLED_CMD); //开启显示	 
	OLED_Clear();
	
	
	// gt20 字库芯片初始化
	GT20_Init(SPI1);
}  