#ifndef __OLED_H
#define __OLED_H			  	 
#include "stdlib.h"	
#include "stm32f10x.h"

//SCLK--PC.0
//SDA--PC.1
//RST--PC.2
//D/C-PC.3


//OLED模式设置
//0:4线串行模式
//1:并行8080模式
#define OLED_MODE 0
		    		

// cmd 命令列表
#define DISPLAY_ON						0xAF  // 显示开关关
#define DISPLAY_OFF						0xAE  // 显示开关关
#define TOTAL_DISPLAY_ON				0xA5  // 全局显示开
#define TOTAL_DISPLAY_OFF				0xA4  // 全局显示关
#define NORMAL_DISPLAY					0xA6  // 正向显示
#define INVERT_DISPLAY					0xA7  // 反向显示
#define HOARIZONTAL_SCROLL_LEFT 		0x27  // 向左水平滚动
#define HOARIZONTAL_SCROLL_RIGHT 		0x26  // 向右水平滚动
#define HOARIZONTAL_SCROLL_LEFT_DOWN 	0x29  // 向左下水平滚动
#define HOARIZONTAL_SCROLL_RIGHT_DOWN	0x2A  // 向右下水平滚动
#define ACTIVATE_SCROLL 				0x2F  // 滚动开
#define DEACTIVATE_SCROLL 				0x2E  // 滚动关


#define CONTRAST						0x81  // 0-0xff 屏幕对比度


// 屏幕滚动时间间隔
#define SRCOLL_TIME_5_FRAMES			0x00  // 5 	 frames
#define SRCOLL_TIME_64_FRAMES			0x01  // 64  frames
#define SRCOLL_TIME_128_FRAMES			0x02  // 128 frames
#define SRCOLL_TIME_256_FRAMES			0x03  // 256 frames
#define SRCOLL_TIME_3_FRAMES			0x04  // 3   frames
#define SRCOLL_TIME_4_FRAMES			0x05  // 4   frames
#define SRCOLL_TIME_25_FRAMES			0x06  // 25  frames
#define SRCOLL_TIME_2_FRAMES			0x07  // 2   frames

#define SRCOLL_ROW_OFF					0x00  // 0 ROWS
#define SRCOLL_ROW_MAX					0x3F  // 0 ROWS

#define SRCOLL_RIGHT					1	  // 滚动模式 从左至右
#define SRCOLL_LEFT						0 	  // 滚动模式 从右至左




//-----------------OLED端口定义----------------  					   
    						  
//-----------------OLED端口定义----------------  					   

#define OLED_CS_Clr()  GPIO_ResetBits(GPIOA,GPIO_Pin_15)
#define OLED_CS_Set()  GPIO_SetBits(GPIOA,GPIO_Pin_15)

#define OLED_RST_Clr() GPIO_ResetBits(GPIOA,GPIO_Pin_11)
#define OLED_RST_Set() GPIO_SetBits(GPIOA,GPIO_Pin_11)

#define OLED_RS_Clr() GPIO_ResetBits(GPIOD,GPIO_Pin_2)
#define OLED_RS_Set() GPIO_SetBits(GPIOD,GPIO_Pin_2)

//#define OLED_WR_Clr() GPIO_ResetBits(GPIOG,GPIO_Pin_14)
//#define OLED_WR_Set() GPIO_SetBits(GPIOG,GPIO_Pin_14)

//#define OLED_RD_Clr() GPIO_ResetBits(GPIOG,GPIO_Pin_13)
//#define OLED_RD_Set() GPIO_SetBits(GPIOG,GPIO_Pin_13)



//PC0~7,作为数据线
#define DATAOUT(x) GPIO_Write(GPIOC,x);//输出  
//使用4线串行接口时使用 

#define OLED_SCLK_Clr() GPIO_ResetBits(GPIOB,GPIO_Pin_3)
#define OLED_SCLK_Set() GPIO_SetBits(GPIOB,GPIO_Pin_3)

#define OLED_SDIN_Clr() GPIO_ResetBits(GPIOB,GPIO_Pin_5)
#define OLED_SDIN_Set() GPIO_SetBits(GPIOB,GPIO_Pin_5)


 		     
#define OLED_CMD  0	//写命令
#define OLED_DATA 1	//写数据
//OLED控制用函数
void OLED_WR_Byte(u8 dat,u8 cmd);	    
void OLED_Display_On(void);
void OLED_Display_Off(void);
void OLED_Refresh_Gram(void);		   
							   		    
void OLED_Init(void);
void OLED_Clear(void);
void OLED_DrawPoint(u8 x,u8 y,u8 t);
void OLED_Fill(u8 x1,u8 y1,u8 x2,u8 y2,u8 dot);
void OLED_ShowChar(u8 x,u8 y,u8 chr,u8 size,u8 mode);
void OLED_ShowNum(u8 x,u8 y,u32 num,u8 len,u8 size);
void OLED_ShowString(u8 x,u8 y,const u8 *p);	
void OLED_RightSrcoll(u8 start, u8 stop);
void OLED_LeftSrcoll(u8 start, u8 stop);
void OLED_DisplayHanzi(u8 x, u8 y, u8 msb, u8 msl, u8 size, u8 mode);
void LCD_displayInt4824(u8 x, u8 y, uint32_t displayInt);
void OLED_DisplayAngle(u8 x, u8 y, u8 mode, u8 angle);
void OLED_DisplayBatteryStatus(u8 x, u8 y, u8 mode, u8 batteryStatus);
void OLED_DisplayLOGOImage(void);
#endif  
	 



