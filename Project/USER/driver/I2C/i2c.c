#include "i2c.h"
//////////////////////////////////////////////////////////////////////////////////	 
//本程序只供学习使用，未经作者许可，不得用于其它任何用途
//ALIENTEK战舰STM32开发板
//IIC驱动 代码	   
//正点原子@ALIENTEK
//技术论坛:www.openedv.com
//修改日期:2012/9/9
//版本：V1.0
//版权所有，盗版必究。
//Copyright(C) 广州市星翼电子科技有限公司 2009-2019
//All rights reserved									  
//////////////////////////////////////////////////////////////////////////////////
void iic_delay_us(u16 t2)
{
	u16 i,j;
	for(i=0;i<12;i++)
		for(j=0;j<t2;j++);
}
//初始化IIC
void IIC_Init(void)
{					     
	GPIO_InitTypeDef GPIO_InitStructure;
	RCC_APB2PeriphClockCmd(	RCC_APB2Periph_GPIOB | RCC_APB2Periph_GPIOE, ENABLE );	
	   
	GPIO_InitStructure.GPIO_Pin = IIC_WP_PIN;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_Out_PP ;   //推挽输出
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
	GPIO_Init(IIC_WP_PORT, &GPIO_InitStructure);
	
	GPIO_InitStructure.GPIO_Pin = IIC_CLK_PIN; 
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_Out_OD ;   //推挽输出
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
	GPIO_Init(IIC_CLK_PORT, &GPIO_InitStructure);
	
	GPIO_InitStructure.GPIO_Pin = IIC_DATA_PIN;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_Out_OD ;   //推挽输出
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
	GPIO_Init(IIC_DATA_PORT, &GPIO_InitStructure);
	
	IIC_WP_RESET();
}
//产生IIC起始信号
void IIC_Start(void)
{
	SDA_OUT();     //sda线输出
	IIC_DATA_SET();	  	  
	IIC_CLK_SET();
	iic_delay_us(4);
 	IIC_DATA_RESET();//START:when CLK is high,DATA change form high to low 
	iic_delay_us(4);
	IIC_CLK_RESET();//钳住I2C总线，准备发送或接收数据 
}
//产生IIC停止信号
void IIC_Stop(void)
{
	SDA_OUT();//sda线输出
	IIC_CLK_RESET();
	IIC_DATA_RESET();//STOP:when CLK is high DATA change form low to high
 	iic_delay_us(4);
	IIC_CLK_SET(); 
	IIC_DATA_SET();//发送I2C总线结束信号
	iic_delay_us(4);							
}
//等待应答信号到来
//返回值：1，接收应答失败
//        0，接收应答成功
u8 IIC_Wait_Ack(void)
{
	u8 ucErrTime=0;
	SDA_IN();      //SDA设置为输入  
	IIC_DATA_SET();
	iic_delay_us(1);	   
	IIC_CLK_SET();
	iic_delay_us(1);	 
	while(IIC_READ_DATA())
	{
		ucErrTime++;
		if(ucErrTime>250)
		{
			IIC_Stop();
			return 1;
		}
	}
	IIC_CLK_RESET();//时钟输出0 	   
	return 0;  
} 
//产生ACK应答
void IIC_Ack(void)
{
	IIC_CLK_RESET();
	SDA_OUT();
	IIC_DATA_RESET();
	iic_delay_us(2);
	IIC_CLK_SET();
	iic_delay_us(2);
	IIC_CLK_RESET();
}
//不产生ACK应答		    
void IIC_NAck(void)
{
	IIC_CLK_RESET();
	SDA_OUT();
	IIC_DATA_SET();
	iic_delay_us(2);
	IIC_CLK_SET();
	iic_delay_us(2);
	IIC_CLK_RESET();
}					 				     
//IIC发送一个字节
//返回从机有无应答
//1，有应答
//0，无应答			  
void IIC_Send_Byte(u8 txd)
{                        
    u8 t;   
	SDA_OUT(); 	    
    IIC_CLK_RESET();//拉低时钟开始数据传输
    for(t=0;t<8;t++)
    {              
        //IIC_SDA=(txd&0x80)>>7;
		if((txd&0x80)>>7)
			IIC_DATA_SET();
		else
			IIC_DATA_RESET();
		txd<<=1; 	  
		iic_delay_us(2);   //对TEA5767这三个延时都是必须的
		IIC_CLK_SET();
		iic_delay_us(2); 
		IIC_CLK_RESET();	
		iic_delay_us(2);
    }	 
} 	    
//读1个字节，ack=1时，发送ACK，ack=0，发送nACK   
u8 IIC_Read_Byte(unsigned char ack)
{
	unsigned char i,receive=0;
	SDA_IN();//SDA设置为输入
    for(i=0;i<8;i++ )
	{
        IIC_CLK_RESET(); 
        iic_delay_us(2);
		IIC_CLK_SET();
        receive<<=1;
        if(IIC_READ_DATA())receive++;   
		iic_delay_us(1); 
    }					 
    if (!ack)
        IIC_NAck();//发送nACK
    else
        IIC_Ack(); //发送ACK   
    return receive;
}

void iic_delay_test(void) {
	int i = 0;
	for (i = 0;i < 100;i++){
		IIC_CLK_RESET();
        iic_delay_us(10);
		IIC_CLK_SET();
        iic_delay_us(10);
	}
}
