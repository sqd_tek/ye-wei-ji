#ifndef __I2C_H__
#define __I2C_H__

#include "los_typedef.h"
#include "stm32f10x.h"


//IO方向设置
#define SDA_IN()  {GPIOB->CRL&=0X0FFFFFFF;GPIOB->CRL|=8<<28;}
#define SDA_OUT() {GPIOB->CRL&=0X0FFFFFFF;GPIOB->CRL|=3<<28;}

#define IIC_WP_PORT			GPIOD
#define IIC_WP_PIN			GPIO_Pin_2
#define IIC_CLK_PORT		GPIOB
#define IIC_CLK_PIN			GPIO_Pin_6
#define IIC_DATA_PORT		GPIOB
#define IIC_DATA_PIN		GPIO_Pin_7

//IO操作函数	 
#define IIC_WP_SET()		//GPIO_SetBits(IIC_WP_PORT, IIC_WP_PIN)
#define IIC_WP_RESET()		//GPIO_ResetBits(IIC_WP_PORT, IIC_WP_PIN)
#define IIC_CLK_SET()		GPIO_SetBits(IIC_CLK_PORT, IIC_CLK_PIN)
#define IIC_CLK_RESET()		GPIO_ResetBits(IIC_CLK_PORT, IIC_CLK_PIN)
#define IIC_DATA_SET()		GPIO_SetBits(IIC_DATA_PORT, IIC_DATA_PIN)
#define IIC_DATA_RESET()	GPIO_ResetBits(IIC_DATA_PORT, IIC_DATA_PIN)
#define IIC_READ_DATA()		GPIO_ReadInputDataBit(IIC_DATA_PORT, IIC_DATA_PIN)

//IIC所有操作函数
void IIC_Init(void);                //初始化IIC的IO口				 
void IIC_Start(void);				//发送IIC开始信号
void IIC_Stop(void);	  			//发送IIC停止信号
void IIC_Send_Byte(u8 txd);			//IIC发送一个字节
u8 IIC_Read_Byte(unsigned char ack);//IIC读取一个字节
u8 IIC_Wait_Ack(void); 				//IIC等待ACK信号
void IIC_Ack(void);					//IIC发送ACK信号
void IIC_NAck(void);				//IIC不发送ACK信号

void IIC_Write_One_Byte(u8 daddr,u8 addr,u8 data);
u8 IIC_Read_One_Byte(u8 daddr,u8 addr);	 
#endif
