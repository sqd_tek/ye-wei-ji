#ifndef __MY_LIB_H
#define __MY_LIB_H

#include "stm32f10x.h"



typedef struct t_xtime {
  int year; int month;  int day;  
  int hour; int minute;  int second;
} _xtime ;
 
#define xMINUTE   (60             ) //1????
#define xHOUR      (60*xMINUTE) //1?????
#define xDAY        (24*xHOUR   ) //1????
#define xYEAR       (365*xDAY   ) //1????



char dchar(char* ptr);
void dump_hex(char* buf, u16 len);

void my_hex2str(u32 hex, char* buf, u16 len);
u32 my_str2hex(char* pstr);
float my_atof(char* pstr);
void my_ftoa(float f, char* buf, u16 len);
int my_atoi(char* pstr);
void my_itoa(u32 Int, char* buf, u16 len);
int my_pow(int x, int y);
bool my_isDigital(char *string);



void xSeconds2Date(unsigned long seconds,_xtime *time);
#endif
