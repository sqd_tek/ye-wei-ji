#include "my_lib.h"
#include "config.h"
#include "uart.h"
#include "math.h"
#include "string.h"
extern int g_print_flag;
static const char hexstr[16] = {'0', '1', '2', '3', '4', '5', '6', '7',
                                '8', '9', 'A', 'B', 'C', 'D', 'E', 'F'};

int my_pow(int x, int y) {
	int ret = 1;
	
	if (y < 0) {
		return 1;
	}
	
	if ((x == 0) || (x == 1) || (y == 1)) {
		return x;
	}
	
	if (y == 0) {
		return 1;
	}
	
	for (;y;y--) {
		ret *= x;
	}
	
	return ret;
	
	
}
char dchar(char* ptr) {
    return (*ptr >= ' ' && *ptr <= '~') ? (*ptr) : '.';
}


void dump_hex(char* buf, u16 len) {
    s32 i;
    s32 j;

    D_PRINTF("------------------------------------------------------------------\r\n");
    i = 0;
    j = len;
    while (j >= 16) {
//        d_printf("%04x: %02x%02x %02x%02x %02x%02x %02x%02x %02x%02x %02x%02x %02x%02x %02x%02x",
//                    i,*(buf+1),*(buf+0),*(buf+3),*(buf+2),
//                      *(buf+5),*(buf+4),*(buf+7),*(buf+6),
//                      *(buf+9),*(buf+8),*(buf+11),*(buf+10),
//                      *(buf+13),*(buf+12),*(buf+15),*(buf+14));
//        d_printf(" | %c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c |\r\n",
//                    dchar(buf+0),dchar(buf+1),dchar(buf+2),dchar(buf+3),
//                    dchar(buf+4),dchar(buf+5),dchar(buf+6),dchar(buf+7),
//                    dchar(buf+8),dchar(buf+9),dchar(buf+10),dchar(buf+11),
//                    dchar(buf+12),dchar(buf+13),dchar(buf+14),dchar(buf+15));
		D_PRINTF("%04x: %02x%02x %02x%02x %02x%02x %02x%02x %02x%02x %02x%02x %02x%02x %02x%02x",
                    i,*(buf+0),*(buf+1),*(buf+2),*(buf+3),
                      *(buf+4),*(buf+5),*(buf+6),*(buf+7),
                      *(buf+8),*(buf+9),*(buf+10),*(buf+11),
                      *(buf+12),*(buf+13),*(buf+14),*(buf+15));
        D_PRINTF(" | %c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c |\r\n",
                    dchar(buf+0),dchar(buf+1),dchar(buf+2),dchar(buf+3),
                    dchar(buf+4),dchar(buf+5),dchar(buf+6),dchar(buf+7),
                    dchar(buf+8),dchar(buf+9),dchar(buf+10),dchar(buf+11),
                    dchar(buf+12),dchar(buf+13),dchar(buf+14),dchar(buf+15));
        j -= 16;
        i += 16;
        buf += 16;
    }

    if (j > 0) {
//        D_PRINTF("%04x: %02x%02x %02x%02x %02x%02x %02x%02x %02x%02x %02x%02x %02x%02x %02x%02x",
//                    i,*(buf+1),*(buf+0),*(buf+3),*(buf+2),
//                      *(buf+5),*(buf+4),*(buf+7),*(buf+6),
//                      *(buf+9),*(buf+8),*(buf+11),*(buf+10),
//                      *(buf+13),*(buf+12),*(buf+15),*(buf+14));
//        D_PRINTF(" | %c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c |\r\n",
//                    dchar(buf+3),dchar(buf+2),dchar(buf+1),dchar(buf+0),
//                    dchar(buf+7),dchar(buf+6),dchar(buf+5),dchar(buf+4),
//                    dchar(buf+11),dchar(buf+10),dchar(buf+9),dchar(buf+8),
//                    dchar(buf+15),dchar(buf+14),dchar(buf+13),dchar(buf+12));
//        D_PRINTF("**** The last line may contain invalid data out of range.\r\n");
		D_PRINTF("%04x: %02x%02x %02x%02x %02x%02x %02x%02x %02x%02x %02x%02x %02x%02x %02x%02x",
                    i,*(buf+0),*(buf+1),*(buf+2),*(buf+3),
                      *(buf+4),*(buf+5),*(buf+6),*(buf+7),
                      *(buf+8),*(buf+9),*(buf+10),*(buf+11),
                      *(buf+12),*(buf+13),*(buf+14),*(buf+15));
        D_PRINTF(" | %c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c |\r\n",
                    dchar(buf+0),dchar(buf+1),dchar(buf+2),dchar(buf+3),
                    dchar(buf+4),dchar(buf+5),dchar(buf+6),dchar(buf+7),
                    dchar(buf+8),dchar(buf+9),dchar(buf+10),dchar(buf+11),
                    dchar(buf+12),dchar(buf+13),dchar(buf+14),dchar(buf+15));
        D_PRINTF("**** The last line may contain invalid data out of range.\r\n");
    }

    D_PRINTF("------------------------------------------------------------------\r\n");
}


// FIXME: atoi in STM32 library does not work
int my_atoi(char* pstr) {
    int Ret_Integer = 0;
    int Integer_sign = 1;

    if (pstr == NULL) {
        printf("Pointer is NULL\r\n");
        return 0;
    }
    while (*pstr == ' ') {
        pstr++;
    }

    if (*pstr == '-') {
        Integer_sign = -1;
    }
    if (*pstr == '-' || *pstr == '+') {
        pstr++;
    }

    while (*pstr >= '0' && *pstr <= '9') {
        Ret_Integer = Ret_Integer * 10 + *pstr - '0';
        pstr++;
    }

    Ret_Integer = Integer_sign * Ret_Integer;
    return Ret_Integer;
}
void my_itoa(u32 Int, char* buf, u16 len) {
	int i = 0;
	int code = 0;
	
	// UINT32_MAX  = 42 9496 7295
	if (len > 10) {
        printf("Warning:Beyond conversion range\r\n");
		len = 10;
	}
	
    i = 0;
    while (i < len) {
        code = (Int / (int)pow(10, len - i - 1)) % 10;
        buf[i] = hexstr[code];
        i++;
    }
}


// FIXME: atof in STM32 library does not work
float my_atof(char* pstr) {
    float Ret_Integer = 0;
	float Ret_Fraction = 0;
	int i = 1;
    int Integer_sign = 1;

    if (pstr == NULL) {
        printf("Pointer is NULL\r\n");
        return 0;
    }
    while (*pstr == ' ') {
        pstr++;
    }

    if (*pstr == '-') {
        Integer_sign = -1;
    }
    if (*pstr == '-' || *pstr == '+') {
        pstr++;
    }

    while (*pstr >= '0' && *pstr <= '9') {
        Ret_Integer = Ret_Integer * 10 + *pstr - '0';
        pstr++;
    }
	if (*pstr == '.') {
		pstr++;
		while (*pstr >= '0' && *pstr <= '9') {
			Ret_Fraction = Ret_Fraction * 10 + (*pstr - '0');
			pstr++;
			i *= 10;
		}
		Ret_Fraction /= i; 
	}
    
    return  Integer_sign * (Ret_Integer + Ret_Fraction);
}

// 默认保留两位小数
void my_ftoa(float f, char* buf, u16 len) {
	int i = 0;
	int code = 0;
	int Int_len = len - 3;
	int Int = (int)f;
	float Fra = f - Int;
	
	// UINT32_MAX  = 42 9496 7295
	if (len > 10) {
        printf("Warning:Beyond conversion range\r\n");
		len = 10;
	}
	
    i = 0;
    while (i < Int_len) {
        code = (Int / (int)pow(10, Int_len - i - 1)) % 10;
        buf[i] = hexstr[code];
        i++;
    }
	
	buf[i++] = '.';
	Fra *= 10;
	buf[i++] = hexstr[(int)Fra];
	Fra -= (int)Fra;
	
	
	Fra *= 10;
	buf[i++] = hexstr[(int)Fra];
	Fra -= (int)Fra;
}
u32 my_str2hex(char* pstr) {
    u32 ret = 0;
    int i = 0;

    if (pstr == NULL) {
        printf("Pointer is NULL\r\n");
        return ret;
    }
    while (*pstr == ' ') {
        pstr++;
    }

    while (*pstr) {
        ret = ret << 4;

        if (*pstr >= 'A' && *pstr <= 'F') {
            ret |= (*pstr - 'A' + 10);
        } else if (*pstr >= 'a' && *pstr <= 'f') {
            ret |= (*pstr - 'a' + 10);
        } else if (*pstr >= '0' && *pstr <= '9') {
            ret |= (*pstr - '0');
        } else {
            ret = ret >> 4;
            break;
        }

        pstr++;
        i++;
        if (i >= 8) {
            printf("Warning: only first 8 digits are converted\r\n");
            break;
        }
    }

    return ret;
}
void my_hex2str(u32 hex, char* buf, u16 len) {
    int i;
    int code = 0;

    if (len > 8) {
        printf("Warning: only first 8 digits are converted\r\n");
        len = 8;
    }

    i = 0;
    while (i < len) {
        code = (hex >> (len - i - 1) * 4) & 0xF;
        buf[i] = hexstr[code];
        i++;
    }
}


bool my_isDigital(char *string) {
	int i;
	
	if (string == NULL) {
		printf("Warning:string empty\r\n");
		return false;
	}
	
	
	for (i = 0;i < strlen(string);i++) {
		if ((string[i] < '0') || (string[i] > '9') || (string[i] == '.')) {
			return false;
		}
	}
	
	return true;
}

bool my_memIsLongInt(char *addr) {
	if (addr == NULL) {
		printf("Warning:addr is null\r\n");
		return false;
	}
	
	if (((addr[0]) | (addr[1] << 8) | (addr[2] << 16) | (addr[3] << 24)) == (*(uint32_t *)(addr))) {
		
	}
	
	
	return true;
}

void xSeconds2Date(unsigned long seconds,_xtime *time )
{
    static unsigned int month[12]={
        /*01?*/31, 
        /*02?*/28, 
        /*03?*/31, 
        /*04?*/30, 
        /*05?*/31, 
        /*06?*/30, 
        /*07?*/31, 
        /*08?*/31, 
        /*09?*/30, 
        /*10?*/31, 
        /*11?*/30, 
        /*12?*/31 
    };
    unsigned int days; 
    unsigned short leap_y_count; 
    time->second      = seconds % 60;//??? 
    seconds          /= 60; 
    time->minute      =  seconds % 60;//??? 
    seconds          += 8 * 60 ;        //???? ??UTC+8 bylzs
    seconds          /= 60; 
    time->hour        = seconds % 24;//??? 
    days              = seconds / 24;//????? 
    leap_y_count = (days + 365) / 1461;//????????(4???) 
    if( ((days + 366) % 1461) == 0) 
    {//?????1? 
        time->year = 1970 + (days / 366);//??? 
        time->month = 12;              //??? 
        time->day = 31; 
        return; 
    } 
    days -= leap_y_count; 
    time->year = 1970 + (days / 365);     //??? 
    days %= 365;                       //?????? 
    days = 01 + days;                  //1??? 
    if( (time->year % 4) == 0 ) 
    { 
        if(days > 60)--days;            //???? 
        else 
        { 
            if(days == 60) 
            { 
                time->month = 2; 
                time->day = 29; 
                return; 
            } 
        } 
    } 
    for(time->month = 0;month[time->month] < days;time->month++) 
    { 
        days -= month[time->month]; 
    } 
    ++time->month;               //??? 
    time->day = days;           //??? 
}
