#ifndef __GPRSDEV_H__
#define __GPRSDEV_H__

#include "stm32f10x_gpio.h"
#include "stm32f10x_usart.h"
#include "stm32f10x_dma.h"
#include "stm32f10x_rcc.h"
#include "stm32f10x.h"

#define POWER_DET_PORT GPIOC
#define POWER_DET_PIN  GPIO_Pin_14
#define POWER_KEY_PORT GPIOC
#define POWER_KEY_PIN  GPIO_Pin_2
#define GPRS_STATUS_PORT GPIOC
#define GPRS_STATUS_PIN  GPIO_Pin_0

#define Power_GPRS_On()     GPIO_ResetBits(GPIOB, GPIO_Pin_8) 
#define Power_GPRS_Off()    GPIO_SetBits(GPIOB, GPIO_Pin_8) 

#define GPRS_PWERKEY_Low()  GPIO_SetBits(POWER_KEY_PORT, POWER_KEY_PIN)
#define GPRS_PWERKEY_High() GPIO_ResetBits(POWER_KEY_PORT, POWER_KEY_PIN)
#define GPRS_STATUS_OK()    GPIO_ReadInputDataBit(GPRS_STATUS_PORT, GPRS_STATUS_PIN) 

#define GPRS_SIM_DEC()		GPIO_ReadInputDataBit(POWER_DET_PORT, POWER_DET_PIN)

#define GPRS_RXBUFFER_LEN   1200  //  = 1024 + 64 + some extra   AT CMD DATA
extern char GPRS_RxBuffer[GPRS_RXBUFFER_LEN];
extern int GPRS_index;
// exported methods
void GPRS_Init_Hardware(void);
unsigned int send_string(char *str);
unsigned int send_binary(char *str, unsigned int len);
void memset_rx_buf(void);

#endif
