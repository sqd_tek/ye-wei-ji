#include "stdio.h"
#include "stdlib.h"
#include "string.h"
#include "time.h"
#include "gprsdev.h"
#include "uart.h"
#include "gpio.h"

char GPRS_RxBuffer[GPRS_RXBUFFER_LEN];
int GPRS_index = 0;
void memset_rx_buf(void) {
    GPRS_index = 0;
    memset(GPRS_RxBuffer, 0, sizeof(GPRS_RxBuffer));
}
void GPIO_Configuration(void) {
    GPIOX_Init(POWER_KEY_PORT, POWER_KEY_PIN, GPIO_Mode_Out_PP, true);
    GPIOX_Init(GPIOB, GPIO_Pin_8, GPIO_Mode_Out_PP, false);
    GPIOX_Init(GPRS_STATUS_PORT, GPRS_STATUS_PIN, GPIO_Mode_IPD, false);
    GPIOX_Init(POWER_DET_PORT, POWER_DET_PIN, GPIO_Mode_IPU, false);
}
void GPRS_func(void) {
    GPRS_RxBuffer[GPRS_index++] = USART_ReceiveData(USART2);  // need fix use gprs uart num.
    if (GPRS_index > GPRS_RXBUFFER_LEN) {
        GPRS_index = 0;
    }
}
// Exported methods
void GPRS_Init_Hardware(void) {
    /* Configure Uart. */
    Usart2_Init(115200);
    Usart2SetIRQCallBack(GPRS_func);
    /* Configure the GPIO ports */
    GPIO_Configuration();
}

unsigned int send_string(char *str) {
    Usart2_Put_Str(str);
    return strlen(str);
}


unsigned int send_binary(char *str, unsigned int len) {
    int i = 0;
    for (i = 0; i < len; i++) {
        Usart2_Put_Char(str[i]);
    }
    return strlen(str);
}
