#ifndef __ADC_H__
#define __ADC_H__
#include "los_typedef.h"
#include "stm32f10x.h"


void ADCx_Init(ADC_TypeDef* ADCx, uint16_t ADCx_Channel);
uint16_t ADCx_GetValue(ADC_TypeDef* ADCx, uint16_t ADCx_Channel);



#endif


