/*
 *  ADC.c - Analog to Digital Converter  ($vision: V1.0$)
 *
 *  Copyright (C) 2001, 2002 Will Liu <liushenglin@cryo-service.com.cn>
 *
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * @file    ADC.c
 * @author  sqdtek team
 * @version V1.0.0
 * @date    2017-06-07
 *
 * This program developed by Beijing SQD Technology CO.,LTD.;
 * @brief   This file defines ADC1,2 hardware drivers.
 *
 *   This file is confi dential. Recipient(s) named above is(are) obligated
 * to maintain secrecy and is(are) not permitted to disclose the contents
 * of this communication to others. Thanks!
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 */

#include "adc.h"
#include "stm32f10x_dma.h"
#include "stm32f10x_gpio.h"
#include "stm32f10x_rcc.h"
#include "uart.h"

void ADCx_Init(ADC_TypeDef* ADCx, uint16_t ADCx_Channel) {
    ADC_InitTypeDef ADC_InitStructure;
    GPIO_InitTypeDef GPIO_InitStructure;
	GPIO_TypeDef * ADC_GPIOx;
	
	
	if (!(ADCx == ADC1 || ADCx == ADC2 || ADCx == ADC3)) {
		printf("ADC Type error\r\n");
		return ;
	} 
	
	if (ADCx_Channel > 15) {
		printf("ADC channel error\r\n");
		return ;
	} 
	
	
	
   /* Enable ADC1 and GPIOA clock */
	if (ADCx_Channel <= 7) {
		RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOA, ENABLE);
		ADC_GPIOx = GPIOA;
	} else if (ADCx_Channel <= 9){
		RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOB, ENABLE);	
		ADC_GPIOx = GPIOB;	
	} else if (ADCx_Channel <= 15){
		RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOC, ENABLE);	
		ADC_GPIOx = GPIOC;	
	}
	
	if (ADCx == ADC1) {
		RCC_APB2PeriphClockCmd(RCC_APB2Periph_ADC1, ENABLE);
	} else if (ADCx == ADC2) {
		RCC_APB2PeriphClockCmd(RCC_APB2Periph_ADC2, ENABLE);
	} else if (ADCx == ADC3) {
		if ((ADCx_Channel <= 3) || (ADCx_Channel >= 10 && ADCx_Channel <= 13)) {
			RCC_APB2PeriphClockCmd(RCC_APB2Periph_ADC3, ENABLE);
		}
	} 
	
	
	
    RCC_ADCCLKConfig(RCC_PCLK2_Div6);//12MHZ 
    
	/* Configure PA.1 (ADC Channel) as analog input -------------------------*/
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_3;//GPIO_Pin_1 + ADCx_Channel - 1;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AIN;
	//GPIO_Init(ADC_GPIOx, &GPIO_InitStructure);
	GPIO_Init(GPIOC, &GPIO_InitStructure);

	//ADC_DeInit(ADC1);//?????????????????,?????

	  /* ADC1 configuration ------------------------------------------------------*/
	ADC_InitStructure.ADC_Mode = ADC_Mode_Independent;//???????????,??????????
	ADC_InitStructure.ADC_ScanConvMode = DISABLE;
	ADC_InitStructure.ADC_ContinuousConvMode = DISABLE;
	ADC_InitStructure.ADC_ExternalTrigConv = ADC_ExternalTrigConv_None;
	ADC_InitStructure.ADC_DataAlign = ADC_DataAlign_Right;
	ADC_InitStructure.ADC_NbrOfChannel = 1;
	ADC_Init(ADCx, &ADC_InitStructure);

	/* Enable ADC1 *///??????????????????????????????????????
	//????????,???????????????????????????????????

	ADC_Cmd(ADCx, ENABLE);

	/* Enable ADC1 reset calibration register */   
	ADC_ResetCalibration(ADCx);
	/* Check the end of ADC1 reset calibration register */
	while(ADC_GetResetCalibrationStatus(ADCx));

	/* Start ADC1 calibration */
	ADC_StartCalibration(ADCx);
	/* Check the end of ADC1 calibration */
	while(ADC_GetCalibrationStatus(ADCx));
}

uint16_t ADCx_GetValue(ADC_TypeDef* ADCx, uint16_t ADCx_Channel) {
    uint16_t DataValue;   
	/* ADC1 regular channel14 configuration */ 
	ADC_RegularChannelConfig(ADCx, ADCx_Channel, 1, ADC_SampleTime_239Cycles5);
    
    /* Start ADC1 Software Conversion */ 
	ADC_SoftwareStartConvCmd(ADCx, ENABLE);
    
    /* Test if the ADC1 EOC flag is set or not */ 
    while(!ADC_GetFlagStatus(ADCx, ADC_FLAG_EOC));
	
	/*Returns the ADC1 Master data value of the last converted channel*/
	DataValue = ADC_GetConversionValue(ADCx); 
	
	return DataValue; 
}


