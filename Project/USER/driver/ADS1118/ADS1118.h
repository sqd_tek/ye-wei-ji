#ifndef __ADS1118_H
#define __ADS1118_H

#include "config.h"

enum ADS8344_Mode {
	ADS8344_Mode_signel = 0,
	ADS8344_Mode_diff = 1,
};

#define ADS_CS_PORT     GPIOB
#define ADS_CS_PIN      GPIO_Pin_12

#define ADS_CS_Reset()  GPIO_ResetBits(ADS_CS_PORT, ADS_CS_PIN)
#define ADS_CS_Set()    GPIO_SetBits(ADS_CS_PORT, ADS_CS_PIN)

float ADS1118_ReadVal(void);
void ADS_Init(void);
void AD1118_ChannelMode(uint8_t ADS_channel, uint8_t ADS_mode, uint8_t ADS_pos);



#endif
