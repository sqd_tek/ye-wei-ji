#include "ADS1118.h"
#include "spi.h"
#include "Los_Task.h"

// read/write register
uint8_t ADS1118_RW(uint8_t data) {
	return SPI2_RW(data);
}

// delay
void ADS1118_delay_ms(uint16_t ms) {
	LOS_TaskDelay(ms);
}


void AD1118_Mode(unsigned char sw) {
    if (sw) {
        ADS_CS_Reset();
        ADS1118_RW(0x52);
        ADS1118_RW(0x0B);
        Spi_Delay();
        ADS_CS_Set();
    } else {
        ADS_CS_Reset();
        ADS1118_RW(0x42);
        ADS1118_RW(0x0B);
        Spi_Delay();
        ADS_CS_Set();
    }
}

float ADS1118_ReadVal(void) {
    int val = 0;
    unsigned char Data_MSD = 0;
    unsigned char Data_LSB = 0;

    ADS_CS_Reset();
    Data_MSD = ADS1118_RW(0x00);
    Data_LSB = ADS1118_RW(0x00);
    ADS_CS_Set();
    val = (Data_MSD << 8) | Data_LSB;
	return val * 0.000125;
}
/**********************************************
 * ADS_mode：1：差分
 * 		   ：0：单端
 * ADS_channel：           AINP     AINN
 *              差分  1：   0        1
 *              差分  2：   0        3
 *              差分  3：   1        3
 *              差分  4：   2        3
 *              单端  1：   0        GND
 *              单端  2：   1        GND
 *              单端  3：   2        GND
 *              单端  4：   3        GND
 **********************************************/

void AD1118_ChannelMode(uint8_t ADS_channel, uint8_t ADS_mode, uint8_t ADS_pos) { // pos 没有使用
	uint8_t data_h = 0, data_l = 0;
	
	if (ADS_channel > 4) {
		printf("channel out of range\r\n");
		return ;
	}
	
	
	if (ADS_mode) { // 差分
		data_h = 0x00 | (ADS_channel - 1);
	} else {
		data_h = 0x04 | (ADS_channel - 1);
	}
	data_h = (data_h << 4) |0x02;
	data_l = 0x0b;
	
	
	ADS_CS_Reset();
	ADS1118_RW(data_h);
	ADS1118_RW(data_l);
	Spi_Delay();
	ADS_CS_Set();
}
void ADS_Init(void) {
    GPIO_InitTypeDef GPIO_InitStructure;
	
    RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOB, ENABLE);
    
	GPIO_InitStructure.GPIO_Pin = ADS_CS_PIN;
    GPIO_InitStructure.GPIO_Mode = GPIO_Mode_Out_PP;
    GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
    GPIO_Init(ADS_CS_PORT, &GPIO_InitStructure);
    GPIO_SetBits(ADS_CS_PORT, ADS_CS_PIN);
    
	ADS_CS_Set();
	
	SPI2_Init(SPI_CPOL_Low, SPI_CPHA_2Edge);
	
    AD1118_Mode(0);
}


