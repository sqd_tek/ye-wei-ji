#ifndef __GPIO_H__
#define __GPIO_H__
#include "los_typedef.h"
#include "stm32f10x.h"
#include "stm32f10x_gpio.h"


enum GPIO_SetMode {
	GPIO_SetModeInput = 0,
	GPIO_SetModeOutput = !GPIO_SetModeInput,
};

bool GPIOX_Init(GPIO_TypeDef* Port, uint16_t GPIO_Pin, GPIOMode_TypeDef GPIO_Mode, bool Init_Value);
uint8_t GPIOX_GetPinBit(GPIO_TypeDef* Port, uint16_t GPIO_Pin, enum GPIO_SetMode GPIO_Mode);
#endif
