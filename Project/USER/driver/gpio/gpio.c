#include "gpio.h"

/*******************************************************************************
 * Function Name  : GPIO_Init
 * Description    : This function Init GPIO.
 * Input          : GPIO_Pin(GPIO_Pin_0-GPIO_Pin_15,GPIO_Pin_All) 
                    GPIO_Mode(GPIO_Mode_AIN,
                              GPIO_Mode_IN_FLOATING,
                              GPIO_Mode_IPD,
                              GPIO_Mode_IPU,
                              GPIO_Mode_Out_OD,
                              GPIO_Mode_Out_PP,
                              GPIO_Mode_AF_OD,
                              GPIO_Mode_AF_PP)
                    Init_Value(true, false)
 * Output         : bool
 * Return         : None
 *******************************************************************************/
bool GPIOX_Init(GPIO_TypeDef* Port, uint16_t GPIO_Pin, GPIOMode_TypeDef GPIO_Mode, bool Init_Value)
{
    GPIO_InitTypeDef GPIO_InitStructure;
    
    if (Port == GPIOA) {
        RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOA, ENABLE);
    } else if (Port == GPIOB){
        RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOB, ENABLE);
    } else if (Port == GPIOC){
        RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOC, ENABLE);
    } else if (Port == GPIOD){
        RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOD, ENABLE);
    } else if (Port == GPIOE){
        RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOE, ENABLE);
    } else if (Port == GPIOF){
        RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOF, ENABLE);
    } else if (Port == GPIOG){
        RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOG, ENABLE);
    } else {
       return OS_ERROR; 
    }
    
    GPIO_InitStructure.GPIO_Pin = GPIO_Pin ;
    GPIO_InitStructure.GPIO_Mode = GPIO_Mode;
    GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
    GPIO_Init(Port, &GPIO_InitStructure);
    if ((GPIO_Mode == GPIO_Mode_AIN) || 
        (GPIO_Mode == GPIO_Mode_IN_FLOATING) || 
        (GPIO_Mode == GPIO_Mode_IPD) || 
        (GPIO_Mode == GPIO_Mode_IPU)) {
            return LOS_OK;
    }
    if (Init_Value) {
        GPIO_SetBits(Port, GPIO_Pin);
    } else {
        GPIO_ResetBits(Port, GPIO_Pin);
    }
    return LOS_OK;
}


uint8_t GPIOX_GetPinBit(GPIO_TypeDef* Port, uint16_t GPIO_Pin, enum GPIO_SetMode GPIO_Mode) {
	if (GPIO_Mode == GPIO_SetModeInput) {
		return GPIO_ReadInputDataBit(Port, GPIO_Pin);
	} else {
		return GPIO_ReadOutputDataBit(Port, GPIO_Pin);
	}
}


