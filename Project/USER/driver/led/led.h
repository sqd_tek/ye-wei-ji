#ifndef __LED_H
#define __LED_H

#include "stm32f10x.h"

#define LED0_PORT	GPIOB
#define LED0_PIN	GPIO_Pin_9
#define LED1_PORT	GPIOE
#define LED1_PIN	GPIO_Pin_9
#define LED2_PORT	GPIOE
#define LED2_PIN	GPIO_Pin_10
#define LED3_PORT	GPIOE
#define LED3_PIN	GPIO_Pin_11



enum LED {
	LED_HEARTBEAT = 0,
	LED_GPRS,
	LED_DIFFPRESSURE,
	LED_PRESSURE,
	LED_MAX,
};




bool Led_Init(enum LED Led_num, FlagStatus light);
bool LED_SetLight(enum LED Led_num, FlagStatus light);
bool LED_GetLight(enum LED Led_num);
void LED_SetPeriod(int num, int light, int period);
void Led_IRQhandle(void);
#endif
