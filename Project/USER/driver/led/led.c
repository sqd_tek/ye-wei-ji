#include "led.h"
#include "gpio.h"
#include "uart.h"
#include "Timer.h"

int led_count[LED_MAX] = {0};
int led_Period[LED_MAX] = {0};
int led_Light[LED_MAX] = {0};

void Led_IRQhandle(void) {
	int i = 0;
	
	//printf("led[0]:%d %d %d\r\n", led_count[i], led_Light[i], led_Period[i]);
	for (i = 0;i < LED_MAX;i++) {
		led_count[i]++;
		if (led_count[i] >= led_Period[i]) {
			led_count[i] = 0;
		}
		if (led_count[i] < led_Light[i]) {
			LED_SetLight(i, SET);
		} else {
			LED_SetLight(i, RESET);
		}
	}
}


/*******************************************************************************
 * Function Name  : Led_Init
 * Description    : This function Init LED.
 * Input          : Led_num(0: heartheat led)
                    light(true, false)         
 * Output         : bool
 * Return         : true, false
 *******************************************************************************/
bool Led_Init(enum LED Led_num, FlagStatus light) {
	GPIO_TypeDef* Port;
	uint16_t GPIO_Pin;
	bool Init_Value;
	
	Init_Value = light;
	
	switch (Led_num) {
		case LED_HEARTBEAT:
			Port = LED0_PORT;
			GPIO_Pin = LED0_PIN;
			break;
		
		case LED_GPRS:
			Port = LED1_PORT;
			GPIO_Pin = LED1_PIN;
			break;
		
		case LED_DIFFPRESSURE:
			Port = LED2_PORT;
			GPIO_Pin = LED2_PIN;
			break;
		
		case LED_PRESSURE:
			Port = LED3_PORT;
			GPIO_Pin = LED3_PIN;
			break;
		
		default:
			return false;
	}
	
	GPIOX_Init(Port, GPIO_Pin, GPIO_Mode_Out_PP, light);
	
	if (GPIOX_GetPinBit(Port, GPIO_Pin, GPIO_SetModeOutput) != Init_Value) {
		return false;
	}
	
	return true;
	
}

/*******************************************************************************
 * Function Name  : LED_SetLight
 * Description    : This function set Led port.
 * Input          : Led_num(0: heartheat led)
                    light(true, false)         
 * Output         : bool
 * Return         : true, false
 *******************************************************************************/
bool LED_SetLight(enum LED Led_num, FlagStatus light) {
	GPIO_TypeDef* Port;
	uint16_t GPIO_Pin;
	
	switch (Led_num) {
		case LED_HEARTBEAT:
			Port = LED0_PORT;
			GPIO_Pin = LED0_PIN;
			break;
		
		case LED_GPRS:
			Port = LED1_PORT;
			GPIO_Pin = LED1_PIN;
			break;
		
		case LED_DIFFPRESSURE:
			Port = LED2_PORT;
			GPIO_Pin = LED2_PIN;
			break;
		
		case LED_PRESSURE:
			Port = LED3_PORT;
			GPIO_Pin = LED3_PIN;
			break;
	
		default:
			return false;
	}
	
	if (light == RESET) {
		GPIO_ResetBits(Port, GPIO_Pin);
	} else {
		GPIO_SetBits(Port, GPIO_Pin);
	}
	
	if (GPIOX_GetPinBit(Port, GPIO_Pin, GPIO_SetModeOutput) != (bool)light) {
		return false;
	}
	
	return true;
}

/*******************************************************************************
 * Function Name  : LED_SetLight
 * Description    : This function get Led port.
 * Input          : Led_num(0: heartheat led)      
 * Output         : bool
 * Return         : true, false
 *******************************************************************************/
bool LED_GetLight(enum LED Led_num) {
	GPIO_TypeDef* Port;
	uint16_t GPIO_Pin;
	
	
	switch (Led_num) {
		case LED_HEARTBEAT:
			Port = LED0_PORT;
			GPIO_Pin = LED0_PIN;
			break;
		
		case LED_GPRS:
			Port = LED1_PORT;
			GPIO_Pin = LED1_PIN;
			break;
		
		case LED_DIFFPRESSURE:
			Port = LED2_PORT;
			GPIO_Pin = LED2_PIN;
			break;
		
		case LED_PRESSURE:
			Port = LED3_PORT;
			GPIO_Pin = LED3_PIN;
			break;

		default:
			return false;
	}
	
	return GPIOX_GetPinBit(Port, GPIO_Pin, GPIO_SetModeOutput);
}

void LED_SetPeriod(int num, int light, int period) {
	if (num >= LED_MAX) {
		printf("LED num too big\r\n");
		return ;
	}
	
	led_Period[num] = period;
	led_Light[num] = light;
}

