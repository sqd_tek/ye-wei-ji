#ifndef __SPI2_H__
#define __SPI2_H__

#include "los_typedef.h"
#include "stm32f10x.h"

void Spi_Delay(void);

void SPI1_Init(uint16_t SPI_CPOL, uint16_t SPI_CPHA);
unsigned char SPI1_RW(unsigned char dat);

void SPI2_Init(uint16_t SPI_CPOL, uint16_t SPI_CPHA);
unsigned char SPI2_RW(unsigned char dat);

#endif
