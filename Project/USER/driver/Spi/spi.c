/*
 *  spi.c - Serial Peripheral Interface Driver ($vision: V1.0$)
 *
 *  Copyright (C) 2001, 2002 Will Liu <liushenglin@cryo-service.com.cn>
 *
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * @file    spi.c
 * @author  sqdtek team
 * @version V1.0.0
 * @date    2017-06-07
 *
 * This program developed by Beijing SQD Technology CO.,LTD.;
 * @brief   This file defines SPI 1,2 hardware drivers.
 *
 *   This file is confidential. Recipient(s) named above is(are) obligated
 * to maintain secrecy and is(are) not permitted to disclose the contents
 * of this communication to others. Thanks!
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 */
#include "stm32f10x_gpio.h"
#include "stm32f10x_rcc.h"
#include "spi.h"

void Spi_Delay(void) {
    int i = 360;
    int j = 0;
    for(j=0;j<i;j++);
}

/********************************SPI_1***************************************/

/*******************************************************************************
 * Function Name :SPI1_Init
 * Description   :Init SPI1 Function.
 * Input         :SPI_CPOL(SPI_CPOL_Low or SPI_CPOL_High)
                  SPI_CPHA(SPI_CPHA_1Edge or SPI_CPHA_2Edge)
 * Output        :void
 * Other         :
 * Date          :2017.06.07
 *******************************************************************************/
void SPI1_Init(uint16_t SPI_CPOL, uint16_t SPI_CPHA) {
    GPIO_InitTypeDef GPIO_InitStructure;   
    SPI_InitTypeDef  SPI_InitStructure; 

    SPI_Cmd(SPI1, DISABLE);
    RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOA, ENABLE);
    RCC_APB2PeriphClockCmd(RCC_APB2Periph_SPI1, ENABLE); 

    /* Config SPI1 SCK ,MISO, MOSI Pin, GPIOA^5, GPIOA^6, GPIOA^7 */
    GPIO_InitStructure.GPIO_Pin = GPIO_Pin_5 | GPIO_Pin_6 | GPIO_Pin_7; 
    GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF_PP; 
    GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz; 
    GPIO_Init(GPIOA, &GPIO_InitStructure); 

    SPI_InitStructure.SPI_Direction = SPI_Direction_2Lines_FullDuplex;  // Two wire full duplex.
    SPI_InitStructure.SPI_Mode = SPI_Mode_Master; // Mater mode
    SPI_InitStructure.SPI_DataSize = SPI_DataSize_8b; // Data size 8 bits.
    SPI_InitStructure.SPI_CPOL = SPI_CPOL;  // Polar 
    SPI_InitStructure.SPI_CPHA = SPI_CPHA; // Phas
    SPI_InitStructure.SPI_NSS = SPI_NSS_Soft; // CS produce by soft.
    SPI_InitStructure.SPI_BaudRatePrescaler = SPI_BaudRatePrescaler_128;  // Set SPI speed.
    SPI_InitStructure.SPI_FirstBit = SPI_FirstBit_MSB;  // High in the front/Low in the front
    SPI_InitStructure.SPI_CRCPolynomial = 7;  // CRC
	
    SPI_Init(SPI1, &SPI_InitStructure);
    SPI_Cmd(SPI1,ENABLE);
}
/*******************************************************************************
 * Function Name :SPI1_RW
 * Description   :SPI1 write and read function.
 * Input         :data(need send data)
 * Output        :8 bits data
 * Other         :
 * Date          :2017.06.07
 *******************************************************************************/
unsigned char SPI1_RW(unsigned char dat)
{
    while (SPI_I2S_GetFlagStatus(SPI1, SPI_I2S_FLAG_TXE) == RESET);
    SPI_I2S_SendData(SPI1, dat); 

    while (SPI_I2S_GetFlagStatus(SPI1, SPI_I2S_FLAG_RXNE) == RESET); 
    return SPI_I2S_ReceiveData(SPI1); 

}
/********************************SPI_1 END************************************/


/********************************SPI_2***************************************/

/*******************************************************************************
 * Function Name :SPI2_Init
 * Description   :Init SPI2 Function.
 * Input         :SPI_CPOL(SPI_CPOL_Low or SPI_CPOL_High)
                  SPI_CPHA(SPI_CPHA_1Edge or SPI_CPHA_2Edge)
 * Output        :void
 * Other         :
 * Date          :2017.06.07
 *******************************************************************************/
void SPI2_Init(uint16_t SPI_CPOL, uint16_t SPI_CPHA)
{
    GPIO_InitTypeDef GPIO_InitStructure;   
    SPI_InitTypeDef  SPI_InitStructure; 

    SPI_Cmd(SPI2, DISABLE);
    RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOB,ENABLE);
    RCC_APB1PeriphClockCmd(RCC_APB1Periph_SPI2, ENABLE); 

    /* Config SPI2 SCK ,MISO, MOSI Pin, GPIOB^13,GPIOB^14,GPIOB^15 */
    GPIO_InitStructure.GPIO_Pin = GPIO_Pin_13 | GPIO_Pin_15| GPIO_Pin_14; 
    GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF_PP;
    GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz; 
    GPIO_Init(GPIOB, &GPIO_InitStructure); 

    SPI_InitStructure.SPI_Direction = SPI_Direction_2Lines_FullDuplex;  // Two wire full duplex.
    SPI_InitStructure.SPI_Mode = SPI_Mode_Master; // Mater mode
    SPI_InitStructure.SPI_DataSize = SPI_DataSize_8b; // Data size 8 bits.
    SPI_InitStructure.SPI_CPOL = SPI_CPOL;  // Polar 
    SPI_InitStructure.SPI_CPHA = SPI_CPHA; // Phas
    SPI_InitStructure.SPI_NSS = SPI_NSS_Soft; // CS produce by soft.
    SPI_InitStructure.SPI_BaudRatePrescaler = SPI_BaudRatePrescaler_16;  // Set SPI speed.
    SPI_InitStructure.SPI_FirstBit = SPI_FirstBit_MSB;  // High in the front/Low in the front
    SPI_InitStructure.SPI_CRCPolynomial = 7;  // CRC
    SPI_Init(SPI2, &SPI_InitStructure);
    SPI_Cmd(SPI2,ENABLE);
}
/*******************************************************************************
 * Function Name :SPI2_RW
 * Description   :SPI2 write and read function.
 * Input         :data(need send data)
 * Output        :8 bits data
 * Other         :
 * Date          :2017.06.07
 *******************************************************************************/
unsigned char SPI2_RW(unsigned char dat)
{
    while (SPI_I2S_GetFlagStatus(SPI2, SPI_I2S_FLAG_TXE) == RESET);
    SPI_I2S_SendData(SPI2, dat); 

    while (SPI_I2S_GetFlagStatus(SPI2, SPI_I2S_FLAG_RXNE) == RESET); 
    return SPI_I2S_ReceiveData(SPI2); 

}
/********************************SPI_2 END************************************/







