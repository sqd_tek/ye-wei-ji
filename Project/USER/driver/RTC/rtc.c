#include "rtc.h"
#include "gpio.h"
#include "uart.h"

/*************************************
* reg0: sec:   1xxxxxxx
* reg1: min:   0xxxxxxx
* reg2: hour:  0xxxxxxx
* reg3: week:  0xxxxxxx  1-7
* reg4: day:   01xxxxxx
* reg5: mouth: 011xxxxx
* reg6: year:  xxxxxxxx  2001/1/1  -  2099/12/31
* reg7: A_min:
* reg8: A_hour:
* reg9: A_week:
* regA: A_day:
* regB: out_F:
* regC: T_stp:
* regD: T_cnt
* regE: ctrl 1:00000000
* regF: ctrl 2:00000000
* reg2: 
* reg1:
* reg2:
*
*************************************/

void RTC4574_Init(void) {
	GPIOX_Init(CE0_PORT,  CE0_PIN,  GPIO_Mode_Out_PP, true);
	GPIOX_Init(CE1_PORT,  CE1_PIN,  GPIO_Mode_Out_PP, true);
	GPIOX_Init(DCLK_PORT, DCLK_PIN, GPIO_Mode_Out_PP, true);
	GPIOX_Init(DATA_PORT, DATA_PIN, GPIO_Mode_Out_OD, false);
//	CE0_SET;
//	CE1_SET;
//	DCLK_SET;
//	DATA_SET;
//	printf("CE0:%d, CE1:%d, DCLK:%d, DATA:%d\r\n", GPIO_ReadOutputDataBit(CE0_PORT,CE0_PIN), GPIO_ReadOutputDataBit(CE1_PORT,CE1_PIN),
//												   GPIO_ReadOutputDataBit(DCLK_PORT,DCLK_PIN), GPIO_ReadOutputDataBit(DATA_PORT,DATA_PIN));
//	
//	
//	CE0_RESET;
//	CE1_RESET;
//	DCLK_RESET;
//	DATA_RESET;
//	printf("CE0:%d, CE1:%d, DCLK:%d, DATA:%d\r\n", GPIO_ReadOutputDataBit(CE0_PORT,CE0_PIN), GPIO_ReadOutputDataBit(CE1_PORT,CE1_PIN),
//												   GPIO_ReadOutputDataBit(DCLK_PORT,DCLK_PIN), GPIO_ReadOutputDataBit(DATA_PORT,DATA_PIN));
	
}



void RTC4574_delay(uint32_t us) {
	int i, j;
	
	for (i = 0;i < us;i++) {
		for (j = 0;j < 9;j++) {
		}
	}
}


void RTC4574_WriteData(uint8_t addr, uint8_t data) {
	int i = 0;
	

	addr <<= 4;
	addr |= 0x03;
	CS_SET;
	RTC4574_delay(800);
	for (i = 0;i < 8;i++) {
		DCLK_SET;
		RTC4574_delay(800);
		if (addr & 0x1) {
			DATA_SET;
		} else {
			DATA_RESET;
		}
		addr >>= 1;
		DCLK_RESET;
		RTC4574_delay(800);
	}
	
	for (i = 0;i < 8;i++) {
		DCLK_SET;
		RTC4574_delay(800);
		if (data & 0x1) {
			DATA_SET;
		} else {
			DATA_RESET;
		}
		data >>= 1;
		DCLK_RESET;
		RTC4574_delay(800);
	}
	
	CS_RESET;
}

uint8_t RTC4574_ReadData(uint8_t addr) {
	uint8_t ret = 0;
	int i = 0;
	
	addr <<= 4;
	addr |= 0x0C;
	
	CS_SET;

	RTC4574_delay(800);
	for (i = 0;i < 8;i++) {
		DCLK_SET;
		RTC4574_delay(800);
		if (addr & 0x1) {
			DATA_SET;
		} else {
			DATA_RESET;
		}
		addr >>= 1;
		DCLK_RESET;
		RTC4574_delay(800);
	}
	
	for (i = 0;i < 8;i++) {
		DCLK_SET;
		RTC4574_delay(800);
		ret |= DATA;
		ret <<= 1;
		DCLK_RESET;
		RTC4574_delay(800);
	}
	CS_RESET;
	return ret;
}


