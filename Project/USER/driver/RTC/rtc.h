#ifndef __RTC_H
#define __RTC_H

#include "stm32f10x.h"


#define CE0_PORT	GPIOE
#define CE0_PIN		GPIO_Pin_1

#define CE1_PORT	GPIOB
#define CE1_PIN		GPIO_Pin_8

#define AIRQ_PORT	GPIOE
#define AIRQ_PIN	GPIO_Pin_0

#define TIRQ_PORT	GPIOB
#define TIRQ_PIN	GPIO_Pin_9

#define DCLK_PORT	GPIOB
#define DCLK_PIN	GPIO_Pin_6

#define DATA_PORT	GPIOB
#define DATA_PIN	GPIO_Pin_7


#define CE0_SET		GPIO_SetBits(CE0_PORT, CE0_PIN)
#define CE0_RESET	GPIO_ResetBits(CE0_PORT, CE0_PIN)

#define CE1_SET		GPIO_SetBits(CE1_PORT, CE1_PIN)
#define CE1_RESET	GPIO_ResetBits(CE1_PORT, CE1_PIN)

#define CS_SET		CE0_SET;CE1_SET;
#define CS_RESET	CE0_RESET;CE1_RESET;

#define DCLK_SET	GPIO_SetBits(DCLK_PORT, DCLK_PIN)
#define DCLK_RESET	GPIO_ResetBits(DCLK_PORT, DCLK_PIN)

#define DATA_SET	GPIO_SetBits(DATA_PORT, DATA_PIN)
#define DATA_RESET	GPIO_ResetBits(DATA_PORT, DATA_PIN)

#define DATA		GPIO_ReadOutputDataBit(DATA_PORT,DATA_PIN) 

void RTC4574_Init(void);
void RTC4574_delay(uint32_t us);
void RTC4574_WriteData(uint8_t addr, uint8_t data);
uint8_t RTC4574_ReadData(uint8_t addr);


#endif
