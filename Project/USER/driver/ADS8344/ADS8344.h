#ifndef __ADS8344_H
#define __ADS8344_H
#include "stm32f10x.h"

#define 	ADS_CS_PORT		GPIOB
#define 	ADS_CS_PIN		GPIO_Pin_12
#define 	ADS_CLK_PORT	GPIOB
#define		ADS_CLK_PIN		GPIO_Pin_13
#define 	ADS_DIN_PORT	GPIOB
#define		ADS_DIN_PIN		GPIO_Pin_15
#define 	ADS_DOUT_PORT	GPIOB
#define 	ADS_DOUT_PIN	GPIO_Pin_14

#define    	ADS_CLK_0   	GPIO_ResetBits(ADS_CLK_PORT, ADS_CLK_PIN)  
#define    	ADS_CLK_1   	GPIO_SetBits(ADS_CLK_PORT, ADS_CLK_PIN)  
#define    	ADS_CS_0    	GPIO_ResetBits(ADS_CS_PORT, ADS_CS_PIN)
#define    	ADS_CS_1    	GPIO_SetBits(ADS_CS_PORT ,ADS_CS_PIN)
//#define    ADS_DIN     GPIO_WriteBit(GPIOG,GPIO_Pin_13) 
#define    	Din_1       	GPIO_SetBits(ADS_DIN_PORT, ADS_DIN_PIN)
#define    	Din_0       	GPIO_ResetBits(ADS_DIN_PORT, ADS_DIN_PIN)
//#define    ADS_BUSY    GPIO_ReadInputDataBit(GPIOG,GPIO_Pin_12)
#define    	ADS_DOUT    	GPIO_ReadInputDataBit(ADS_DOUT_PORT, ADS_DOUT_PIN)

enum ADS8344_Mode {
	ADS8344_Mode_signel = 0,
	ADS8344_Mode_diff = 1,
};
void ADS8344_GPIO_Config(void);
void ADS_COM(u8 common);
u16 ADS8344_ChannelMode(int channel_num, enum ADS8344_Mode mode, int pos);

#endif
