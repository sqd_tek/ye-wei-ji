#include"ADS8344.h"
/*************************************************
*  (MSB)                                  ( LSB)  * 
*  BIT7 BIT6 BIT5 BIT4 BIT3   BIT2   BIT1  BIT0   *
*   S    A2   A1   A0	-   SGL/DIF	  PD1  PD0	  *
*   1 	 0    0     0		  1		   1    1	  *
*****************s*********************************/
void ADS8344_GPIO_Config(void) {	
    GPIO_InitTypeDef GPIO_InitStructure;
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOB | RCC_APB2Periph_GPIOD, ENABLE); //使能D和E的时钟
	 
	GPIO_InitStructure.GPIO_Pin=GPIO_Pin_8;
	GPIO_InitStructure.GPIO_Mode=GPIO_Mode_Out_PP;                 //推挽输出模式
    GPIO_InitStructure.GPIO_Speed=GPIO_Speed_50MHz;
   	GPIO_Init(GPIOD,&GPIO_InitStructure);
	GPIO_SetBits(GPIOD, GPIO_Pin_8); 
	 //CS:PE_2  RS:PE_0  WR:PE_1    RESET:PE_3
	GPIO_InitStructure.GPIO_Pin=GPIO_Pin_12|GPIO_Pin_13|GPIO_Pin_15;
    GPIO_InitStructure.GPIO_Mode=GPIO_Mode_Out_PP;                 //推挽输出模式
    GPIO_InitStructure.GPIO_Speed=GPIO_Speed_50MHz;
    GPIO_Init(GPIOB,&GPIO_InitStructure);
	 //CS:PE_2  RS:PE_0  WR:PE_1    RESET:PE_3
	GPIO_InitStructure.GPIO_Pin=GPIO_Pin_14;
    GPIO_InitStructure.GPIO_Mode=GPIO_Mode_IPU;                 //推挽输出模式
    GPIO_Init(GPIOB,&GPIO_InitStructure);
 }
/*=======================================================
* 名称	    : ads8344_delay(uint t1,uint t2 )
* 功能描述	: 用于ADS8344的时间延时
* 输入参数	: uint t1 , uint t2,
* 输出参数	: void
* 返回值    : void
=========================================================*/
void ads8344_delay(u16 t1,u16 t2 )
{
	u16 i,j;
	for(i=0;i<t1;i++)
		for(j=0;j<t2;j++);
}
void ADS_COM(u8 common)
{
	u8 i=0;
   for(i=0;i<8;i++)
	{
		ADS_CLK_0;
		ads8344_delay(10,2);
		if(common&0x80)
			Din_1;
		else 
			Din_0;
		common<<=1;
		ADS_CLK_1;
		ads8344_delay(10,2);
	}
}


/**********************************
channel_num:通道号1-8
mode:    0：单通道，1：差分
pos:     0：正  1：反
**********************************/
u16 ADS8344_ChannelMode(int channel_num, enum ADS8344_Mode mode, int pos)
{
	u8 j;
	u16 data=0;
	u8 Reg_value = 0;
	
	Reg_value |= 0x82;
	
	if (mode == ADS8344_Mode_signel) {
		Reg_value |= 0x04;
		switch (channel_num) {
			case 1:
				Reg_value |= 0x00;
				break;
			
			case 2:
				Reg_value |= 0x40;
				break;
			
			case 3:
				Reg_value |= 0x10;
				break;
			
			case 4:
				Reg_value |= 0x50;
				break;
			
			case 5:
				Reg_value |= 0x20;
				break;
			
			case 6:
				Reg_value |= 0x60;
				break;
			
			case 7:
				Reg_value |= 0x30;
				break;
			
			case 8:
				Reg_value |= 0x70;
				break;
		}
	} else {
		if (!pos) {
			switch (channel_num) {
				case 1:
					Reg_value |= 0x00;
					break;
				
				case 2:
					Reg_value |= 0x10;
					break;
				
				case 3:
					Reg_value |= 0x20;
					break;
				
				case 4:
					Reg_value |= 0x30;
					break;
			}
		} else {
			switch (channel_num) {
				case 1:
					Reg_value |= 0x40;
					break;
				
				case 2:
					Reg_value |= 0x50;
					break;
				
				case 3:
					Reg_value |= 0x60;
					break;
				
				case 4:
					Reg_value |= 0x70;
					break;
			}
		}
	}
	
	//printf("ads8344_reg:%x\r\n", Reg_value);
	
	//ADS_CS_1;
	ADS_CLK_0;
	ADS_CS_0;
	ads8344_delay(10,4);
	ADS_COM(Reg_value);
	ADS_CLK_0;	
	ADS_CS_1;
    //while(ADS_BUSY);				  //自己测，试试while（）中   ADS_BUSY与 ！ADS_BUSY哪个更准确
	ads8344_delay(10,4);
    ADS_CS_0;
  
	ads8344_delay(10,1000);
	ads8344_delay(10,1000);
	ads8344_delay(10,1000);
	ads8344_delay(10,1000);
	ads8344_delay(10,1000);
	
	ADS_CLK_1;					 
	ads8344_delay(10,2);
	ADS_CLK_0;
	ads8344_delay(10,2);

	for(j=0;j<16;j++)
	{	
		ADS_CLK_1;
		ads8344_delay(10,2);
		data=(data<<1)|ADS_DOUT;
		ADS_CLK_0;
		ads8344_delay(10,2);
	 }
	  ADS_CS_1;
	  return data;
}




