#include "battery.h"
#include "adc.h"
#include "uart.h"
#include "gpio.h"
#include "los_task.ph"


static ADC_TypeDef* Battery_adc = 0;
static uint16_t Battery_channel = 0;

void Battery_delay(uint16_t ms) {
	LOS_TaskDelay(ms);
}


int Battery_InsertStatus(void) {
	int res = 0;
	int i = 0;
	int ret = 0;
	
	res = Battery_chargeStatus();
	while ((res == Battery_chargeStatus()) && (i < 1000)) {
		Battery_delay(1); // 1ms
		i++;
	}
	
	if (i == 1000) {
		ret = 0;
	} else {
		ret = 1;
	}
	
	while (i < 1000) {
		Battery_delay(1); // 1ms
	}
	
	return ret;
}

/*********************************
* 0: success
* 1: error
*********************************/
int Battery_chargeStatus(void) {
	return GPIOX_GetPinBit(BATTERY_CHRG_PORT, BATTERY_CHRG_PIN, GPIO_SetModeInput);
}
int Battery_FillStatus(void) {
	return GPIOX_GetPinBit(BATTERY_DONE_PORT, BATTERY_DONE_PIN, GPIO_SetModeInput);
}

void Battery_Init(ADC_TypeDef* ADCx, uint16_t ADCx_Channel) {	
	Battery_adc = ADCx;
	Battery_channel = ADCx_Channel;
	printf("Battery_adc:%x Battery_channel:%d\r\n", (uint32_t)Battery_adc, Battery_channel);
	Battery_GpioInit();
	ADCx_Init(ADCx, ADCx_Channel);
	
	
//	while (Battery_InsertStatus()) {
//		IWDG_ReloadCounter();
//		printf("Please insert the Battery!\r\n");
//		Battery_delay(1000);
//	}
	
//	printf("insert the battery successly\r\n");
}

// ����mV
unsigned int Battery_GetVoltage(uint32_t avg) {
	uint32_t voltage = 0, BatteryValueMv = 0;
	int i = 0;
    
	for (i = 0;i < avg;i++) {
		voltage += ADCx_GetValue(Battery_adc, Battery_channel);
	}
	voltage /= avg;
	BatteryValueMv = (uint32_t)(((voltage * 3300) >> 12) * (30 + 12) / 12);
	
	return BatteryValueMv;
}
void Battery_GpioInit(void) {
	GPIOX_Init(BATTERY_CHRG_PORT, BATTERY_CHRG_PIN, GPIO_Mode_IPU, 0);
	GPIOX_Init(BATTERY_DONE_PORT, BATTERY_DONE_PIN, GPIO_Mode_IPU, 0);
}

