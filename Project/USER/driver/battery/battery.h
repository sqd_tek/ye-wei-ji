#ifndef __BATTERY_H
#define __BATTERY_H

#include "stm32f10x.h"


#define BATTERY_CHRG_PORT	GPIOA
#define BATTERY_CHRG_PIN	GPIO_Pin_0
#define BATTERY_DONE_PORT	GPIOA
#define BATTERY_DONE_PIN	GPIO_Pin_1

#define BATTERY_NOT_CHARGE		-2
#define BATTERY_NOT_INSERT		-1
#define BATTERY_CHARGE			0
#define BATTERY_FILL			1


void Battery_Init(ADC_TypeDef* ADCx, uint16_t ADCx_Channel);
unsigned int Battery_GetVoltage(uint32_t avg);
void Battery_GpioInit(void);
int Battery_chargeStatus(void);
int Battery_InsertStatus(void);
int Battery_FillStatus(void);

#endif
