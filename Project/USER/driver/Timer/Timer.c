/*
 *  timer.c - TIMER Driver ($vision: V1.0$)
 *
 *  Copyright (C) 2001, 2002 Will Liu <liushenglin@cryo-service.com.cn>
 *
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * @file    timer.c
 * @author  sqdtek team
 * @version V1.0.0
 * @date    2017-06-07
 *
 * This program developed by Beijing SQD Technology CO.,LTD.;
 * @brief   This file defines timer2,3,4 hardware drivers.
 *
 *   This file is confidential. Recipient(s) named above is(are) obligated
 * to maintain secrecy and is(are) not permitted to disclose the contents
 * of this communication to others. Thanks!
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 */
 
#include "stm32f10x_tim.h"
#include "stm32f10x_gpio.h"
#include "stm32f10x_rcc.h"
#include "Timer.h"
#include "stdio.h"
#include "delay.h"
#include "los_hwi.h"
#include "beep.h"

/********************************TIMER 2***************************************/
static void (*Timer2_IRQHandler)(void) = NULL;

/*******************************************************************************
 * Function Name :void Timer2Init(void)
 * Description   :Common timer2 init 
 * Input         :T=(TIM_Period)*(TIM_Prescaler)/72M  s
 * Output        :
 * Other         :
 * Date          :2013.01.27
 *******************************************************************************/
void Timer2Init(uint16_t TIM_Period, uint16_t TIM_Prescaler) 
{
    NVIC_InitTypeDef NvicInitdef;
    TIM_TimeBaseInitTypeDef timbase;    

    RCC_APB1PeriphClockCmd(RCC_APB1Periph_TIM2, ENABLE);            //ENABLE rcc    
    
	TIM_DeInit(TIM2); 
    
	timbase.TIM_CounterMode = TIM_CounterMode_Up;
    timbase.TIM_ClockDivision = TIM_CKD_DIV1;
    timbase.TIM_Period = TIM_Period - 1;
    timbase.TIM_Prescaler = TIM_Prescaler - 1;                                  //frequency division
    TIM_TimeBaseInit(TIM2, &timbase);

    NvicInitdef.NVIC_IRQChannel = TIM2_IRQn;
    NvicInitdef.NVIC_IRQChannelPreemptionPriority = 0;             //lowest priority
    NvicInitdef.NVIC_IRQChannelSubPriority = 10;
    NvicInitdef.NVIC_IRQChannelCmd = ENABLE;
    NVIC_Init(&NvicInitdef);
	
    TIM_SetCounter(TIM2, 1);
    
	TIM_ClearFlag(TIM2,TIM_FLAG_Update);
    TIM_ITConfig(TIM2, TIM_IT_Update, ENABLE);
}

/*******************************************************************************
 * Function Name :void TIM2_IRQHandler(void)
 * Description   :Timer2 Interrupt Function
 * Input         :none
 * Output        :none
 * Other         :
 * Date          :2017.06.07
 *******************************************************************************/
void TIM2_IRQHandler(void)                                          
{       
    if (TIM_GetITStatus(TIM2, TIM_IT_Update) != RESET) {  // interrupt handler
        TIM_ClearITPendingBit(TIM2, TIM_IT_Update);
        if (Timer2_IRQHandler != NULL) {
            (*Timer2_IRQHandler)();
        }        
    }  
}
/*******************************************************************************
 * Function Name :void Timer2_Open(void)
 * Description   :Timer2 open 
 * Input         :none
 * Output        :none
 * Other         :
 * Date          :2017.06.07
 *******************************************************************************/
bool Timer2_Open(uint16_t TIM_Period, uint16_t TIM_Prescaler)
{
    UINTPTR uvIntSave;
	uvIntSave = LOS_IntLock();
    Timer2Init(TIM_Period, TIM_Prescaler);
    LOS_HwiCreate(TIM2_IRQn, 0, 0, TIM2_IRQHandler, 0);
    TIM_Cmd(TIM2, ENABLE);
    LOS_IntRestore(uvIntSave);
    
    return LOS_OK;
}
/*******************************************************************************
 * Function Name :Timer2_Close(void)
 * Description   :Timer2 close
 * Input         :none
 * Output        :bool
 * Other         :
 * Date          :2017.06.07
 *******************************************************************************/
bool Timer2_Close(void)                                             
{
    TIM_ITConfig(TIM2, TIM_IT_Update, DISABLE);                     //timer2 stop
    TIM_Cmd(TIM2, DISABLE);
    
    return LOS_OK;
}
/*******************************************************************************
 * Function Name :Timer2SetIRQCallBack(void)
 * Description   :Timer2 set callback function.
 * Input         :void (*func)(void)
 * Output        :bool
 * Other         :
 * Date          :2017.06.07
 *******************************************************************************/
bool Timer2SetIRQCallBack(void *fun) 
{
    Timer2_IRQHandler = (void (*)(void))fun;
    return LOS_OK;
}
/********************************TIMER_2 END************************************/

/********************************TIMER 3***************************************/
static void (*Timer3_IRQHandler)(void) = NULL;

/*******************************************************************************
 * Function Name :void Timer3Init(void)
 * Description   :Common timer3 init 
 * Input         :T=(TIM_Period)*(TIM_Prescaler)/72
 * Output        :
 * Other         :
 * Date          :2013.01.27
 *******************************************************************************/
void Timer3Init(uint16_t TIM_Period, uint16_t TIM_Prescaler) 
{
    NVIC_InitTypeDef NvicInitdef;
    TIM_TimeBaseInitTypeDef timbase;    

    RCC_APB1PeriphClockCmd(RCC_APB1Periph_TIM3, ENABLE);  // ENABLE rcc    
    TIM_DeInit(TIM3); 
    timbase.TIM_CounterMode = TIM_CounterMode_Up;
    timbase.TIM_ClockDivision = TIM_CKD_DIV1;
    timbase.TIM_Period = TIM_Period - 1;
    timbase.TIM_Prescaler = TIM_Prescaler - 1;
    TIM_TimeBaseInit(TIM3, &timbase);

    NvicInitdef.NVIC_IRQChannel = TIM3_IRQn;
    NVIC_SetPriorityGrouping(NVIC_PriorityGroup_0);  
    NvicInitdef.NVIC_IRQChannelPreemptionPriority = 10;  // lowest priority
    NvicInitdef.NVIC_IRQChannelSubPriority = 0;
    NvicInitdef.NVIC_IRQChannelCmd = ENABLE;
    NVIC_Init(&NvicInitdef);
    TIM_SetCounter(TIM3, 1);
    TIM_ClearFlag(TIM3,TIM_FLAG_Update);
    TIM_ITConfig(TIM3, TIM_IT_Update, ENABLE);
    
}

/*******************************************************************************
 * Function Name :void TIM3_IRQHandler(void)
 * Description   :Timer3 Interrupt Function
 * Input         :none
 * Output        :none
 * Other         :
 * Date          :2017.06.07
 *******************************************************************************/
void TIM3_IRQHandler(void)                                          
{   
    if (TIM_GetITStatus(TIM3, TIM_IT_Update) != RESET) {  // interrupt handler
        TIM_ClearITPendingBit(TIM3, TIM_IT_Update);
        
        if (Timer3_IRQHandler != NULL) {
            (*Timer3_IRQHandler)();
        }        
    }  
}
/*******************************************************************************
 * Function Name :void Timer3_Open(void)
 * Description   :Timer3 open 
 * Input         :none
 * Output        :none
 * Other         :
 * Date          :2017.06.07
 *******************************************************************************/
bool Timer3_Open(uint16_t TIM_Period, uint16_t TIM_Prescaler)
{
    UINTPTR uvIntSave;
    
    TIM_SetCounter(TIM3, 1);
	uvIntSave = LOS_IntLock();
    Timer3Init(TIM_Period, TIM_Prescaler);
    LOS_HwiCreate(TIM3_IRQn, 0, 0, TIM3_IRQHandler, 0);
    TIM_Cmd(TIM3, ENABLE);
    LOS_IntRestore(uvIntSave);
    
    return LOS_OK;
}
/*******************************************************************************
 * Function Name :Timer3_Close(void)
 * Description   :Timer3 close
 * Input         :none
 * Output        :bool
 * Other         :
 * Date          :2017.06.07
 *******************************************************************************/
bool Timer3_Close(void)                                             
{
    TIM_ITConfig(TIM3, TIM_IT_Update, DISABLE);                     //timer2 stop
    TIM_Cmd(TIM3, DISABLE);
    
    return LOS_OK;
}
/*******************************************************************************
 * Function Name :Timer3SetIRQCallBack(void)
 * Description   :Timer3 set callback function.
 * Input         :void (*func)(void)
 * Output        :bool
 * Other         :
 * Date          :2017.06.07
 *******************************************************************************/
bool Timer3SetIRQCallBack(void *fun) 
{
    Timer3_IRQHandler = (void (*)(void))fun;
    return LOS_OK;
}
/********************************TIMER_3 END************************************/

/********************************TIMER 4***************************************/
static void (*Timer4_IRQHandler)(void) = NULL;

/*******************************************************************************
 * Function Name :void Timer4Init(void)
 * Description   :Common timer4 init 
 * Input         :T=(TIM_Period)*(TIM_Prescaler)/72
 * Output        :
 * Other         :
 * Date          :2017.06.07
 *******************************************************************************/
void Timer4Init(uint16_t TIM_Period, uint16_t TIM_Prescaler) 
{
    NVIC_InitTypeDef NvicInitdef;
    TIM_TimeBaseInitTypeDef timbase;    

    RCC_APB1PeriphClockCmd(RCC_APB1Periph_TIM4, ENABLE);  // ENABLE rcc    
    TIM_DeInit(TIM4); 
    timbase.TIM_CounterMode = TIM_CounterMode_Up;
    timbase.TIM_ClockDivision = TIM_CKD_DIV1;
    timbase.TIM_Period = TIM_Period - 1;
    timbase.TIM_Prescaler = TIM_Prescaler - 1;
    TIM_TimeBaseInit(TIM4, &timbase);

    NvicInitdef.NVIC_IRQChannel = TIM4_IRQn;
    NVIC_SetPriorityGrouping(NVIC_PriorityGroup_0);  
    NvicInitdef.NVIC_IRQChannelPreemptionPriority = 2;  // lowest priority
    NvicInitdef.NVIC_IRQChannelSubPriority = 3;
    NvicInitdef.NVIC_IRQChannelCmd = ENABLE;
    NVIC_Init(&NvicInitdef);
    TIM_SetCounter(TIM4, 1);
    TIM_ClearFlag(TIM4,TIM_FLAG_Update);
    TIM_ITConfig(TIM4, TIM_IT_Update, ENABLE);
    
}

/*******************************************************************************
 * Function Name :void TIM4_IRQHandler(void)
 * Description   :Timer4 Interrupt Function
 * Input         :none
 * Output        :none
 * Other         :
 * Date          :2017.06.07
 *******************************************************************************/
void TIM4_IRQHandler(void)                                          
{   
    if (TIM_GetITStatus(TIM4, TIM_IT_Update) != RESET) {  // interrupt handler
        TIM_ClearITPendingBit(TIM4, TIM_IT_Update);
        
        if (Timer4_IRQHandler != NULL) {
            (*Timer4_IRQHandler)();
        }        
    }  
}
/*******************************************************************************
 * Function Name :void Timer4_Open(void)
 * Description   :Timer4 open 
 * Input         :none
 * Output        :none
 * Other         :
 * Date          :2017.06.07
 *******************************************************************************/
bool Timer4_Open(uint16_t TIM_Period, uint16_t TIM_Prescaler)
{
    UINTPTR uvIntSave;
    
    TIM_SetCounter(TIM4, 1);
	uvIntSave = LOS_IntLock();
    Timer4Init(TIM_Period, TIM_Prescaler);
    LOS_HwiCreate(TIM4_IRQn, 0, 0, TIM4_IRQHandler, 0);
    TIM_Cmd(TIM4, ENABLE);
    LOS_IntRestore(uvIntSave);
    
    return LOS_OK;
}
/*******************************************************************************
 * Function Name :Timer4_Close(void)
 * Description   :Timer4 close
 * Input         :none
 * Output        :bool
 * Other         :
 * Date          :2017.06.07
 *******************************************************************************/
bool Timer4_Close(void)                                             
{
    TIM_ITConfig(TIM4, TIM_IT_Update, DISABLE);  // timer4 stop
    TIM_Cmd(TIM4, DISABLE);
    
    return LOS_OK;
}
/*******************************************************************************
 * Function Name :Timer4SetIRQCallBack(void)
 * Description   :Timer4 set callback function.
 * Input         :void (*func)(void)
 * Output        :bool
 * Other         :
 * Date          :2017.06.07
 *******************************************************************************/
bool Timer4SetIRQCallBack(void *fun) 
{
    Timer4_IRQHandler = (void (*)(void))fun;
    return LOS_OK;
}
/********************************TIMER_4 END************************************/






/********************** END ***************************************************/

