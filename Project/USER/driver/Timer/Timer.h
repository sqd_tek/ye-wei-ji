#ifndef __TIMER_H__
#define __TIMER_H__

#include "los_typedef.h"
#include "stm32f10x.h"

void Timer2Init(uint16_t TIM_Period, uint16_t TIM_Prescaler) ;
bool Timer2SetIRQCallBack(void *fun);
bool Timer2_Open(uint16_t TIM_Period, uint16_t TIM_Prescaler);
bool Timer2_Close(void);

void Timer3Init(uint16_t TIM_Period, uint16_t TIM_Prescaler);
bool Timer3SetIRQCallBack(void *fun);
bool Timer3_Open(uint16_t TIM_Period, uint16_t TIM_Prescaler);
bool Timer3_Close(void);

void Timer4Init(uint16_t TIM_Period, uint16_t TIM_Prescaler);
bool Timer4SetIRQCallBack(void *fun);
bool Timer4_Open(uint16_t TIM_Period, uint16_t TIM_Prescaler);
bool Timer4_Close(void);

#endif
/*********************************** END **************************************/


