#include "GT20.h"
#include "config.h"
#include "gpio.h"
#include "my_lib.h"
#include "string.h"

uint8_t buff_bitmap32[] = {
0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0,
0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0
};
 
uint8_t GT20_RW(uint8_t data) {
	return SPI1_RW(data);
}
/*
 * 这个公式在数据手册上
 */
void S1Y_gt16_GetData (uint8_t MSB, uint8_t LSB, uint8_t *S1YDZ_Data) {
	uint32_t temp = (MSB - 0xB0) * 94 + LSB - 0xA1;
	uint32_t BaseAdd = 0, Address;
  
	if ((MSB == 0xA9) && (LSB >=0xA1)) {
		Address = (282 + (LSB - 0xA1)) * 32 + BaseAdd;
	} else if (MSB >=0xA1 && MSB <= 0xA3 && LSB >=0xA1) {
        Address =( (MSB - 0xA1) * 94 + (LSB - 0xA1))*32+ BaseAdd;
	} else if ((MSB >=0xB0) && (MSB <= 0xF7) && (LSB >=0xA1)) {
        Address = (846 + temp) * 32 + BaseAdd;
	}
	
	// 读取16*16 bit 字符，需要的字节数为 16*16/8=32
	S1Y_rdat_bat(Address, 32, S1YDZ_Data);
} 
/*
 * 
 */
uint8_t S1Y_rdat_bat(uint32_t address, uint8_t byte_long, uint8_t * p_arr) {
    unsigned int j = 0;
	
	GT20_CS_RESET();
	// 快速读取模式
	GT20_RW(0x0b);
	GT20_RW((address >> 16) & 0xff);
	GT20_RW((address >> 8) & 0xff);
	GT20_RW((address >> 0) & 0xff);
	GT20_RW(0x00);
	for (j = 0;j < byte_long;j++) {
		p_arr[j] = GT20_RW(00);
    }
    GT20_CS_SET();
	
    return j+1;	
}

void GT20_Init(SPI_TypeDef * SPIx) {
	if (SPIx == SPI1) {	
		GPIOX_Init(GT20_CS_PORT, GT20_CS_PIN, GPIO_Mode_Out_PP, true);
		SPI1_Init(SPI_CPOL_Low, SPI_CPHA_1Edge);
	}
}
