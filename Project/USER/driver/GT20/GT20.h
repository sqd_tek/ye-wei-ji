#ifndef __GT20_H
#define __GT20_H

#include "spi.h"


#define GT20_CS_PORT	GPIOA
#define GT20_CS_PIN		GPIO_Pin_4

#define GT20_CS_SET()	GPIO_SetBits(GT20_CS_PORT, GT20_CS_PIN)
#define GT20_CS_RESET()	GPIO_ResetBits(GT20_CS_PORT, GT20_CS_PIN)

void GT20_Init(SPI_TypeDef * SPIx);
uint8_t S1Y_rdat_bat(uint32_t address, uint8_t byte_long, uint8_t * p_arr);
#endif
