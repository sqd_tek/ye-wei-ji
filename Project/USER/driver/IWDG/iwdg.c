#include "iwdg.h"
#include "stm32f10x_iwdg.h"


void IWDG_Init(void)
{
    int LsiFreq = 40000;
    
    IWDG_WriteAccessCmd(IWDG_WriteAccess_Enable);
    /* IWDG counter clock: LSI/128 */
    IWDG_SetPrescaler(IWDG_Prescaler_256);

    /* Set counter reload value to obtain 250ms IWDG TimeOut.
       Counter Reload Value = 16s/IWDG counter clock period
       = 16s / (LSI/256)
       = 16s / (LsiFreq/256)
       = 16*LsiFreq/(256)
       = LsiFreq/16
       */
    IWDG_SetReload(LsiFreq/16);

    /* Reload IWDG counter */
    IWDG_ReloadCounter();

    /* Enable IWDG (the LSI oscillator will be enabled by hardware) */
    IWDG_Enable();
}

void IWDG_Init_3S(void) 
{
    IWDG_WriteAccessCmd(IWDG_WriteAccess_Enable);
    IWDG_SetPrescaler(IWDG_Prescaler_64);   // 最小
    IWDG_SetReload(0x9C0);      // 40KHz内部时钟 (1/40000 * 64 * 0x9C0 = 4s)
    IWDG_WriteAccessCmd(IWDG_WriteAccess_Disable);
    IWDG_Enable();
    IWDG_ReloadCounter();
}





