#include "delay.h"
#include <stdio.h>
#include "los_swtmr.h"
#include "time.h"
#include "los_sys.h"
#include "stm32f10x.h"

#define dprintf printf
void Timer1_Callback  (UINT32 arg);  // callback fuction 
                  
void Timer2_Callback	(UINT32 arg);//????


UINT32 g_timercount1 = 0;  
UINT32 g_timercount2 = 0; 

void delay_1us(int i)
{
    int j, k;
    for(j = 0;j < i;j++)
        for(k = 0; k < 8; k++)
            ;
}
void delay_ms(u16 nms) {
	int i;
	for(i = 0;i < nms;i++) {
		delay_1us(1000);
	}
}

void Timer2_Callback(UINT32 arg)
{
    unsigned long tick_last2;  
    tick_last2 = (UINT32)LOS_TickCountGet();
    g_timercount2 ++;
    dprintf("g_timercount2=%d\n", g_timercount2);
    dprintf("tick_last2=%ld\n", tick_last2);
}
UINT16 id2;
void Example_swTimer(void)  
{                                                         
    LOS_SwtmrCreate(100, LOS_SWTMR_MODE_PERIOD, Timer2_Callback, &id2, 1);
    LOS_SwtmrStart(id2);
    dprintf("start Timer2\n");
 
    return ;
}
volatile int g_Tim2Array[(int)eTimMax] = {0,};

