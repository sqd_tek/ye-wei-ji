#ifndef __DELAY_H__
#define __DELAY_H__

#include "stm32f10x.h"

//如果index对应的时间超时，清零并返回1，未超时返回0
#define IS_TIMEOUT_1MS(index, count)    ((g_Tim2Array[(unsigned int)(index)] >= (count))?  \
                                        ((g_Tim2Array[(unsigned int)(index)] = 0) == 0): 0)
                                            
enum {
    eTim1,
    eTim2,
    eTimUpdata,
    eTimYModem,
    eGprsDelay,
    eGprs,
    eTest,
    eGpsDelay,
    eWindDelay,
    eTimMax,
};
extern volatile int g_Tim2Array[(unsigned int)eTimMax];
void Example_swTimer(void);
void delay_us(u32 nus);
void delay_ms(u16 nms);
void delay_init(void);
#endif

