/*
 *  Uart.c - Serial Interface Driver ($vision: V1.0$)
 *
 *  Copyright (C) 2001, 2002 Will Liu <liushenglin@cryo-service.com.cn>
 *
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * @file    uart.c
 * @author  sqdtek team
 * @version V1.0.0
 * @date    2017-06-07
 *
 * This program developed by Beijing SQD Technology CO.,LTD.;
 * @brief   This file defines usart1,2,3\uart4,5 hardware drivers.
 *
 *   This file is confi dential. Recipient(s) named above is(are) obligated
 * to maintain secrecy and is(are) not permitted to disclose the contents
 * of this communication to others. Thanks!
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 */
#include "stm32f10x_gpio.h"
#include "stm32f10x_usart.h"
#include "stm32f10x_rcc.h"
#include "uart.h"
#include "los_hwi.h"
#include "stdio.h"
#include "config.h"
#include "led.h"
// define printf fuction
#define __PRINTF__

static void (*Uart1_IRQHandler)(void) = NULL;
static void (*Uart2_IRQHandler)(void) = NULL;
static void (*Uart3_IRQHandler)(void) = NULL;
static void (*Uart4_IRQHandler)(void) = NULL;
static void (*Uart5_IRQHandler)(void) = NULL;

/*******************************************************************************
 * Function Name  : Uart1_Init
 * Description    : This function handles UART1  INIT .
 * Input          : bandrate
 * Output         : None
 * Return         : None
 *******************************************************************************/
void Usart1_Init(long int Bandrate)
{
    UINTPTR uvIntSave;
    GPIO_InitTypeDef GPIO_InitStructure;
    USART_InitTypeDef USART_InitStructure;
    NVIC_InitTypeDef NVIC_InitStructure;

    RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOA, ENABLE);
    RCC_APB2PeriphClockCmd(RCC_APB2Periph_USART1, ENABLE); 
    /***TXD**/
    GPIO_InitStructure.GPIO_Pin = GPIO_Pin_9;
    GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF_PP;
    GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
    GPIO_Init(GPIOA, &GPIO_InitStructure);

    /***RXD**/
    GPIO_InitStructure.GPIO_Pin = GPIO_Pin_10;
    GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IPU;
    GPIO_Init(GPIOA, &GPIO_InitStructure);

    /********Init Usart1*******/
    USART_InitStructure.USART_BaudRate = Bandrate;
    USART_InitStructure.USART_WordLength = USART_WordLength_8b;
    USART_InitStructure.USART_StopBits = USART_StopBits_1;
    USART_InitStructure.USART_Parity = USART_Parity_No;
    USART_InitStructure.USART_HardwareFlowControl = USART_HardwareFlowControl_None;
    USART_InitStructure.USART_Mode = USART_Mode_Rx | USART_Mode_Tx; 
    USART_Init(USART1, &USART_InitStructure);
    uvIntSave = LOS_IntLock();
    LOS_HwiCreate(USART1_IRQn, 0, 0, USART1_IRQHandler, 0);
    /********Enable Usart1****/
    USART_Cmd(USART1, ENABLE);
    LOS_IntRestore(uvIntSave);
    /*******Open IT*********/
    USART_ITConfig(USART1, USART_IT_RXNE, ENABLE);
    /*******Set NVIC***********/
    NVIC_InitStructure.NVIC_IRQChannel = USART1_IRQn;
    NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 0;
    NVIC_InitStructure.NVIC_IRQChannelSubPriority = 0;
    NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
    NVIC_Init(&NVIC_InitStructure);	
}

/*******************************************************************************
 * Function Name  : Uart1_Put_Char
 * Description    : This function handles UART1  SEND CHAR .
 * Input          : CHAR 
 * Output         : PUT TO CHAR
 * Return         : None
 *******************************************************************************/
void Usart1_Put_Char(char c)
{
    USART_SendData(USART1, c);
    while ((!(USART1->SR & USART_FLAG_TXE))); 
}
/*******************************************************************************
 * Function Name  : Uart1_Put_Str
 * Description    : This function handles UART1  SEND STRING.
 * Input          : CHAR * 
 * Output         : PUT STRING TO ANY
 * Return         : None
 *******************************************************************************/
void Usart1_Put_Str(char *s)
{
    int i = 0;
    while (s[i] != '\0') {
        Usart1_Put_Char(s[i++]);
    }
}
/*******************************************************************************
 * Function Name :void BspUsart1Close(void)
 * Description   :Close Usart1
 * Input         :
 * Output        :
 * Other         :
 * Date          :2016.08.31
 *******************************************************************************/
void BspUsart1Close(void)
{
    USART_Cmd(USART1, DISABLE);

    USART_ITConfig(USART1, USART_IT_TXE, DISABLE);
    USART_ITConfig(USART1, USART_IT_RXNE, DISABLE);
    USART_ITConfig(USART1, USART_IT_TC, DISABLE);

    USART_ClearITPendingBit(USART1, USART_IT_TXE);
    USART_ClearITPendingBit(USART1, USART_IT_TC);
    USART_ClearITPendingBit(USART1, USART_IT_RXNE);	
}

/*******************************************************************************
 * Function Name :Uart1SetIRQCallBack(void)
 * Description   :UART1 set callback function.
 * Input         :void (*func)(void)
 * Output        :bool
 * Other         :
 * Date          :2017.06.07
 *******************************************************************************/
bool Usart1SetIRQCallBack(void *fun) 
{
    Uart1_IRQHandler = (void (*)(void))fun;
    return LOS_OK;
}

/*******************************************************************************
 * Function Name :USART1_IRQHandler(void)
 * Description   :UART1 IRQ function.
 * Input         :void
 * Output        :void
 * Other         :
 * Date          :2017.06.07
 *******************************************************************************/
void USART1_IRQHandler(void)
{
    if (USART_GetITStatus(USART1, USART_IT_RXNE) != RESET) {
        if (Uart1_IRQHandler != NULL) {
            (*Uart1_IRQHandler)();
        }  
    }
    
    USART_ClearFlag(USART1, USART_FLAG_RXNE);
}

/*******************************************************************************
 * Function Name  : Uart2_Init
 * Description    : This function handles UART2  INIT .
 * Input          : bandrate
 * Output         : None
 * Return         : None
 *******************************************************************************/
void Usart2_Init(long int Bandrate)
{
    UINTPTR uvIntSave;
    GPIO_InitTypeDef GPIO_InitStructure;
    USART_InitTypeDef USART_InitStructure;
    NVIC_InitTypeDef NVIC_InitStructure;

    RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOA, ENABLE);
    RCC_APB1PeriphClockCmd(RCC_APB1Periph_USART2, ENABLE); 
    /***TXD**/
    GPIO_InitStructure.GPIO_Pin = GPIO_Pin_2;
    GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF_PP;
    GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
    GPIO_Init(GPIOA, &GPIO_InitStructure);

    /***RXD**/
    GPIO_InitStructure.GPIO_Pin = GPIO_Pin_3;
    GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IPU;
    GPIO_Init(GPIOA, &GPIO_InitStructure);

    /********Init Usart2*******/
    USART_InitStructure.USART_BaudRate = Bandrate;
    USART_InitStructure.USART_WordLength = USART_WordLength_8b;
    USART_InitStructure.USART_StopBits = USART_StopBits_1;
    USART_InitStructure.USART_Parity = USART_Parity_No;
    USART_InitStructure.USART_HardwareFlowControl = USART_HardwareFlowControl_None;
    USART_InitStructure.USART_Mode = USART_Mode_Rx | USART_Mode_Tx; 
    USART_Init(USART2, &USART_InitStructure);
    uvIntSave = LOS_IntLock();
    LOS_HwiCreate(USART2_IRQn, 0, 0, USART2_IRQHandler, 0);
    /********Enable Uart2****/
    USART_Cmd(USART2, ENABLE);
    LOS_IntRestore(uvIntSave);
    
    /*******Open IT*********/
    USART_ITConfig(USART2, USART_IT_RXNE, ENABLE);
    /*******Set NVIC***********/
    NVIC_InitStructure.NVIC_IRQChannel = USART2_IRQn;
    NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 1;
    NVIC_InitStructure.NVIC_IRQChannelSubPriority = 1;
    NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
    NVIC_Init(&NVIC_InitStructure);	
}

/*******************************************************************************
 * Function Name  : Uart2_Put_Char
 * Description    : This function handles UART2  SEND CHAR .
 * Input          : CHAR 
 * Output         : PUT TO CHAR
 * Return         : None
 *******************************************************************************/
void Usart2_Put_Char(char c)
{
    USART_SendData(USART2, c);
    while ((!(USART2->SR & USART_FLAG_TXE))); 
}
/*******************************************************************************
 * Function Name  : Uart2_Put_Str
 * Description    : This function handles UART2  SEND STRING.
 * Input          : CHAR * 
 * Output         : PUT STRING TO ANY
 * Return         : None
 *******************************************************************************/
void Usart2_Put_Str(char *s)
{
    int i = 0;
    while (s[i] != '\0') {
        Usart2_Put_Char(s[i++]);
    }
}
/*******************************************************************************
 * Function Name :void BspUsart2Close(void)
 * Description   :Close Usart2
 * Input         :
 * Output        :
 * Other         :
 * Date          :2016.08.31
 *******************************************************************************/
void BspUsart2Close(void)
{
    USART_Cmd(USART2, DISABLE);

    USART_ITConfig(USART2, USART_IT_TXE, DISABLE);
    USART_ITConfig(USART2, USART_IT_RXNE, DISABLE);
    USART_ITConfig(USART2, USART_IT_TC, DISABLE);

    USART_ClearITPendingBit(USART2, USART_IT_TXE);
    USART_ClearITPendingBit(USART2, USART_IT_TC);
    USART_ClearITPendingBit(USART2, USART_IT_RXNE);	
}

/*******************************************************************************
 * Function Name :Uart2SetIRQCallBack(void)
 * Description   :UART2 set callback function.
 * Input         :void (*func)(void)
 * Output        :bool
 * Other         :
 * Date          :2017.06.07
 *******************************************************************************/
bool Usart2SetIRQCallBack(void *fun) 
{
    Uart2_IRQHandler = (void (*)(void))fun;
    return LOS_OK;
}

/*******************************************************************************
 * Function Name :USART2_IRQHandler(void)
 * Description   :UART2 IRQ function.
 * Input         :void
 * Output        :void
 * Other         :
 * Date          :2017.06.07
 *******************************************************************************/
void USART2_IRQHandler(void)
{
	if (USART_GetITStatus(USART2, USART_IT_RXNE) != RESET) {
        if (Uart2_IRQHandler != NULL) {
            (*Uart2_IRQHandler)();
        }  
    }
    
    USART_ClearFlag(USART2, USART_FLAG_RXNE);
}

/*******************************************************************************
 * Function Name  : Uart3_Init
 * Description    : This function handles UART3  INIT .
 * Input          : bandrate
 * Output         : None
 * Return         : None
 *******************************************************************************/
void Usart3_Init(long int Bandrate)
{
    UINTPTR uvIntSave;
    GPIO_InitTypeDef GPIO_InitStructure;
    USART_InitTypeDef USART_InitStructure;
    NVIC_InitTypeDef NVIC_InitStructure;

    RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOB, ENABLE);
    RCC_APB1PeriphClockCmd(RCC_APB1Periph_USART3, ENABLE); 
    /***TXD**/
    GPIO_InitStructure.GPIO_Pin = GPIO_Pin_10;
    GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF_PP;
    GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
    GPIO_Init(GPIOB, &GPIO_InitStructure);

    /***RXD**/
    GPIO_InitStructure.GPIO_Pin = GPIO_Pin_11;
    GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IPU;
    GPIO_Init(GPIOB, &GPIO_InitStructure);

    /********Init Usart3*******/
    USART_InitStructure.USART_BaudRate = Bandrate;
    USART_InitStructure.USART_WordLength = USART_WordLength_8b;
    USART_InitStructure.USART_StopBits = USART_StopBits_1;
    USART_InitStructure.USART_Parity = USART_Parity_No;
    USART_InitStructure.USART_HardwareFlowControl = USART_HardwareFlowControl_None;
    USART_InitStructure.USART_Mode = USART_Mode_Rx | USART_Mode_Tx; 
    USART_Init(USART3, &USART_InitStructure);
    uvIntSave = LOS_IntLock();
    LOS_HwiCreate(USART3_IRQn, 0, 0, USART3_IRQHandler, 0);
    /********Enable Usart3****/
    USART_Cmd(USART3, ENABLE);
    LOS_IntRestore(uvIntSave);
    
    /*******Open IT*********/
    USART_ITConfig(USART3, USART_IT_RXNE, ENABLE);
    /*******Set NVIC***********/
    NVIC_InitStructure.NVIC_IRQChannel = USART3_IRQn;
    NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 1;
    NVIC_InitStructure.NVIC_IRQChannelSubPriority = 1;
    NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
    NVIC_Init(&NVIC_InitStructure);	
}

/*******************************************************************************
 * Function Name  : Usart3_Put_Char
 * Description    : This function handles UART3  SEND CHAR .
 * Input          : CHAR 
 * Output         : PUT TO CHAR
 * Return         : None
 *******************************************************************************/
void Usart3_Put_Char(char c)
{
    USART_SendData(USART3, c);
    while ((!(USART3->SR & USART_FLAG_TXE))); 
}
/*******************************************************************************
 * Function Name  : Uart3_Put_Str
 * Description    : This function handles UART3  SEND STRING.
 * Input          : CHAR * 
 * Output         : PUT STRING TO ANY
 * Return         : None
 *******************************************************************************/
void Usart3_Put_Str(char *s)
{
    int i = 0;
    while (s[i] != '\0') {
        Usart3_Put_Char(s[i++]);
    }
}
/*******************************************************************************
 * Function Name :void BspUsart3Close(void)
 * Description   :Close Usart3
 * Input         :
 * Output        :
 * Other         :
 * Date          :2016.08.31
 *******************************************************************************/
void BspUsart3Close(void)
{
    USART_Cmd(USART3, DISABLE);

    USART_ITConfig(USART3, USART_IT_TXE, DISABLE);
    USART_ITConfig(USART3, USART_IT_RXNE, DISABLE);
    USART_ITConfig(USART3, USART_IT_TC, DISABLE);

    USART_ClearITPendingBit(USART3, USART_IT_TXE);
    USART_ClearITPendingBit(USART3, USART_IT_TC);
    USART_ClearITPendingBit(USART3, USART_IT_RXNE);	
}

/*******************************************************************************
 * Function Name :Uart1SetIRQCallBack(void)
 * Description   :UART1 set callback function.
 * Input         :void (*func)(void)
 * Output        :bool
 * Other         :
 * Date          :2017.06.07
 *******************************************************************************/
bool Usart3SetIRQCallBack(void *fun) 
{
    Uart3_IRQHandler = (void (*)(void))fun;
    return LOS_OK;
}

/*******************************************************************************
 * Function Name :USART3_IRQHandler(void)
 * Description   :UART3 IRQ function.
 * Input         :void
 * Output        :void
 * Other         :
 * Date          :2017.06.07
 *******************************************************************************/
void USART3_IRQHandler(void)
{
    if (USART_GetITStatus(USART3, USART_IT_RXNE) != RESET) {
        if (Uart3_IRQHandler != NULL) {
            (*Uart3_IRQHandler)();
        }  
    }
    
    USART_ClearFlag(USART3, USART_FLAG_RXNE);
}

/*******************************************************************************
 * Function Name  : Uart4_Init
 * Description    : This function handles UART4  INIT .
 * Input          : None
 * Output         : None
 * Return         : None
 *******************************************************************************/
void Uart4_Init(long int Bandrate)
{
    UINTPTR uvIntSave;
    GPIO_InitTypeDef GPIO_InitStructure;
    USART_InitTypeDef USART_InitStructure;
    NVIC_InitTypeDef NVIC_InitStructure;

    RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOC, ENABLE);
    RCC_APB1PeriphClockCmd(RCC_APB1Periph_UART4, ENABLE); 
    /***TXD**/
    GPIO_InitStructure.GPIO_Pin = GPIO_Pin_10;
    GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF_PP;
    GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
    GPIO_Init(GPIOC, &GPIO_InitStructure);

    /***RXD**/
    GPIO_InitStructure.GPIO_Pin = GPIO_Pin_11;
    GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IPU;
    GPIO_Init(GPIOC, &GPIO_InitStructure);

    /********Init Uart4*******/
    USART_InitStructure.USART_BaudRate = Bandrate;
    USART_InitStructure.USART_WordLength = USART_WordLength_8b;
    USART_InitStructure.USART_StopBits = USART_StopBits_1;
    USART_InitStructure.USART_Parity = USART_Parity_No;
    USART_InitStructure.USART_HardwareFlowControl = USART_HardwareFlowControl_None;
    USART_InitStructure.USART_Mode = USART_Mode_Rx | USART_Mode_Tx; 
    USART_Init(UART4, &USART_InitStructure);
    uvIntSave = LOS_IntLock();
    LOS_HwiCreate(UART4_IRQn, 0, 0, UART4_IRQHandler, 0);
    /********Enable UART4****/
    USART_Cmd(UART4, ENABLE);
    LOS_IntRestore(uvIntSave);
    
    /*******Set IT*********/
    USART_ITConfig(UART4, USART_IT_RXNE, ENABLE);
    /*******Set NVIC***********/
    NVIC_InitStructure.NVIC_IRQChannel = UART4_IRQn;
    NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 1;
    NVIC_InitStructure.NVIC_IRQChannelSubPriority = 1;
    NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
    NVIC_Init(&NVIC_InitStructure);	
}

/*******************************************************************************
 * Function Name  : Uart4_Put_Char
 * Description    : This function handles UART4  SEND CHAR .
 * Input          : CHAR 
 * Output         : PUT TO CHAR
 * Return         : None
 *******************************************************************************/
void Uart4_Put_Char(char c)
{
    USART_SendData(UART4, c);
    while ((!(UART4->SR & USART_FLAG_TXE))); 
}
/*******************************************************************************
 * Function Name  : Uart4_Put_Str
 * Description    : This function handles UART4  SEND STRING.
 * Input          : CHAR * 
 * Output         : PUT STRING TO ANY
 * Return         : None
 *******************************************************************************/
void Uart4_Put_Str(char *s)
{
    int i = 0;
    while (s[i] != '\0') {
        Uart4_Put_Char(s[i++]);
    }
}
/*******************************************************************************
 * Function Name :void BspUart4Close(void)
 * Description   :Close Uart4
 * Input         :
 * Output        :
 * Other         :
 * Date          :2016.08.31
 *******************************************************************************/
void BspUart4Close(void)
{
    USART_Cmd(UART4, DISABLE);

    USART_ITConfig(UART4, USART_IT_TXE, DISABLE);
    USART_ITConfig(UART4, USART_IT_RXNE, DISABLE);
    USART_ITConfig(UART4, USART_IT_TC, DISABLE);

    USART_ClearITPendingBit(UART4, USART_IT_TXE);
    USART_ClearITPendingBit(UART4, USART_IT_TC);
    USART_ClearITPendingBit(UART4, USART_IT_RXNE);	
}

/*******************************************************************************
 * Function Name :Uart1SetIRQCallBack(void)
 * Description   :UART1 set callback function.
 * Input         :void (*func)(void)
 * Output        :bool
 * Other         :
 * Date          :2017.06.07
 *******************************************************************************/
bool Uart4SetIRQCallBack(void *fun) 
{
    Uart4_IRQHandler = (void (*)(void))fun;
    return LOS_OK;
}

/*******************************************************************************
 * Function Name :USART1_IRQHandler(void)
 * Description   :UART1 IRQ function.
 * Input         :void
 * Output        :void
 * Other         :
 * Date          :2017.06.07
 *******************************************************************************/
void UART4_IRQHandler(void)
{
    if (USART_GetITStatus(UART4, USART_IT_RXNE) != RESET) {
        if (Uart4_IRQHandler != NULL) {
            (*Uart4_IRQHandler)();
        }  
    }
    
    USART_ClearFlag(UART4, USART_FLAG_RXNE);
}

/*******************************************************************************
 * Function Name  : Uart5_Init
 * Description    : This function handles UART5  INIT .
 * Input          : None
 * Output         : None
 * Return         : None
 *******************************************************************************/
void Uart5_Init(long int Bandrate)
{
    UINTPTR uvIntSave;
    GPIO_InitTypeDef GPIO_InitStructure;
    USART_InitTypeDef USART_InitStructure;
    NVIC_InitTypeDef NVIC_InitStructure;

    RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOC, ENABLE);
    RCC_APB1PeriphClockCmd(RCC_APB2Periph_GPIOD, ENABLE);
    RCC_APB1PeriphClockCmd(RCC_APB1Periph_UART5, ENABLE); 
    /***TXD**/
    GPIO_InitStructure.GPIO_Pin = GPIO_Pin_12;
    GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF_PP;
    GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
    GPIO_Init(GPIOC, &GPIO_InitStructure);

    /***RXD**/
    GPIO_InitStructure.GPIO_Pin = GPIO_Pin_2;
    GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IPU;
    GPIO_Init(GPIOD, &GPIO_InitStructure);

    /********Init Uart5*******/
    USART_InitStructure.USART_BaudRate = Bandrate;
    USART_InitStructure.USART_WordLength = USART_WordLength_8b;
    USART_InitStructure.USART_StopBits = USART_StopBits_1;
    USART_InitStructure.USART_Parity = USART_Parity_No;
    USART_InitStructure.USART_HardwareFlowControl = USART_HardwareFlowControl_None;
    USART_InitStructure.USART_Mode = USART_Mode_Rx | USART_Mode_Tx; 
    USART_Init(UART5, &USART_InitStructure);
    uvIntSave = LOS_IntLock();
    LOS_HwiCreate(UART5_IRQn, 0, 0, UART5_IRQHandler, 0);
    /********Enable****/
    USART_Cmd(UART5, ENABLE);
    LOS_IntRestore(uvIntSave);
    
    /********Enable IT********/
    USART_ITConfig(UART5, USART_IT_RXNE, ENABLE);
    /********Set NVIC**********/
    NVIC_InitStructure.NVIC_IRQChannel = UART5_IRQn;
    NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 1;
    NVIC_InitStructure.NVIC_IRQChannelSubPriority = 1;
    NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
    NVIC_Init(&NVIC_InitStructure);	
}

/*******************************************************************************
 * Function Name  : Uart5_Put_Char
 * Description    : This function handles UART5  SEND CHAR .
 * Input          : CHAR 
 * Output         : PUT TO CHAR
 * Return         : None
 *******************************************************************************/
void Uart5_Put_Char(char c)
{
    USART_SendData(UART5, c);
    while ((!(UART5->SR & USART_FLAG_TXE))); 
}
/*******************************************************************************
 * Function Name  : Uart5_Put_Str
 * Description    : This function handles UART5  SEND STRING.
 * Input          : CHAR * 
 * Output         : PUT STRING TO ANY
 * Return         : None
 *******************************************************************************/
void Uart5_Put_Str(char *s)
{
    int i = 0;
    while (s[i] != '\0') {
        Uart5_Put_Char(s[i++]);
    }
}
/*******************************************************************************
 * Function Name :void BspUsart5Close(void)
 * Description   :Close Uart5
 * Input         :
 * Output        :
 * Other         :
 * Date          :2016.08.31
 *******************************************************************************/
void BspUart5Close(void)
{
    USART_Cmd(UART5, DISABLE);

    USART_ITConfig(UART5, USART_IT_TXE, DISABLE);
    USART_ITConfig(UART5, USART_IT_RXNE, DISABLE);
    USART_ITConfig(UART5, USART_IT_TC, DISABLE);

    USART_ClearITPendingBit(UART5, USART_IT_TXE);
    USART_ClearITPendingBit(UART5, USART_IT_TC);
    USART_ClearITPendingBit(UART5, USART_IT_RXNE);	
}

/*******************************************************************************
 * Function Name :Uart1SetIRQCallBack(void)
 * Description   :UART1 set callback function.
 * Input         :void (*func)(void)
 * Output        :bool
 * Other         :
 * Date          :2017.06.07
 *******************************************************************************/
bool Uart5SetIRQCallBack(void *fun) 
{
    Uart5_IRQHandler = (void (*)(void))fun;
    return LOS_OK;
}

/*******************************************************************************
 * Function Name :USART1_IRQHandler(void)
 * Description   :UART1 IRQ function.
 * Input         :void
 * Output        :void
 * Other         :
 * Date          :2017.06.07
 *******************************************************************************/
void UART5_IRQHandler(void)
{
    if (USART_GetITStatus(UART5, USART_IT_RXNE) != RESET) {
        if (Uart5_IRQHandler != NULL) {
            (*Uart5_IRQHandler)();
        }  
    }
    
    USART_ClearFlag(UART5, USART_FLAG_RXNE);
}

#ifdef __PRINTF__
int fputc(int ch, FILE *f)  
{
	USART_TypeDef * UARTX;
#ifdef USARTX_1
	UARTX = USART1;
#elif defined USARTX_2
	UARTX = USART2;
#elif defined USARTX_3
	UARTX = USART3;
#elif defined USARTX_4
	UARTX = UART4;
#elif defined USARTX_5
	UARTX = UART5;
#endif
	
    UARTX->DR = (u8) ch;
    /* Loop until the end of transmission */  
    while (USART_GetFlagStatus(UARTX, USART_FLAG_TXE) == RESET) {
    }
    
    return ch;  
}
#endif

/*************************END**************************/

