#ifndef __UART4_H__
#define __UART4_H__

#include "los_typedef.h"
#include "stm32f10x.h"
#include "stdio.h"

/*********************
*********UART5********
**********************/
void Uart5_Init(long int Bandrate);
void Uart5_Put_Char(char c);
void Uart5_Put_Str(char *s);
void BspUart5Close(void);
/*********************
*********UART4********
**********************/
void Uart4_Init(long int Bandrate);
void Uart4_Put_Char(char c);
void Uart4_Put_Str(char *s);
void BspUart4Close(void);

/*********************
*********UART3********
**********************/
void Usart3_Init(long int Bandrate);
void Usart3_Put_Char(char c);
void Usart3_Put_Str(char *s);
void BspUsart3Close(void);
/*********************
*********UART2********
**********************/
void Usart2_Init(long int Bandrate);
void Usart2_Put_Char(char c);
void Usart2_Put_Str(char *s);
void BspUsart2Close(void);
/*********************
*********UART1********
**********************/
void Usart1_Init(long int Bandrate);
void Usart1_Put_Char(char c);
void Usart1_Put_Str(char *s);
void BspUsart1Close(void);




bool Usart1SetIRQCallBack(void *fun);
void USART1_IRQHandler(void);
bool Usart2SetIRQCallBack(void *fun);
void USART2_IRQHandler(void);
bool Usart3SetIRQCallBack(void *fun);
void USART3_IRQHandler(void);
bool Uart4SetIRQCallBack(void *fun);
void UART4_IRQHandler(void);
bool Uart5SetIRQCallBack(void *fun);
void UART5_IRQHandler(void);

#endif 


