#include "stm32f10x_gpio.h"
#include "stm32f10x_rcc.h"
#include "beep.h"

#define Beep_Port    GPIO_Pin_1		//GPIOB1

#define uchar   unsigned char
/*******************************************************************************
 * Function Name  : Beep_Init
 * Description    : This function handles Init beep .
 * Input          : None
 * Output         : None
 * Return         : None
 *******************************************************************************/
void Beep_Init(void)
{
    GPIO_InitTypeDef GPIO_InitStructure;
    RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOA, ENABLE);
    GPIO_InitStructure.GPIO_Pin = Beep_Port ;
    GPIO_InitStructure.GPIO_Mode = GPIO_Mode_Out_PP;
    GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
    GPIO_Init(GPIOA, &GPIO_InitStructure);
    GPIO_ResetBits(GPIOA, Beep_Port );
}
/*******************************************************************************
 * Function Name  : Beep_On
 * Description    : This function open Beep.
 * Input          : None
 * Output         : None
 * Return         : None
 *******************************************************************************/
void Beep_On(void)
{
    GPIO_SetBits(GPIOA, Beep_Port);
}
/*******************************************************************************
 * Function Name  : Beep_Off
 * Description    : This function Close Beep.
 * Input          : None
 * Output         : None
 * Return         : None
 *******************************************************************************/
void Beep_Off(void)
{
    GPIO_ResetBits(GPIOA, Beep_Port );
}
/*******************************************************************************
 * Function Name  : Beep_Beep
 * Description    : This function beep Beep.
 * Input          : None
 * Output         : None
 * Return         : None
 *******************************************************************************/
void Beep_Beep(int time, uchar frequency)
{
    uchar i = 0;
    for(i = 0;i < time;i++ )
    {
        switch(frequency)
        {
        case 0:
            Beep_On();
            Beep_Delay(2000000);
            Beep_Off();
            Beep_Delay(2000000);
            break;
        case 1:
            Beep_On();
            Beep_Delay(3000000);
            Beep_Off();
            Beep_Delay(3000000);
            break;
        case 2:
            Beep_On();
            Beep_Delay(4000000);
            Beep_Off();
            Beep_Delay(4000000);
            break;
        case 3:
            Beep_On();
            Beep_Delay(1000000);
            Beep_Off();
            Beep_Delay(1000000);
            break;
        }
    }
}
/*******************************************************************************
 * Function Name  : Beep_Delay
 * Description    : This function delay.
 * Input          : None
 * Output         : None
 * Return         : None
 *******************************************************************************/
void Beep_Delay(int i)
{
    int j = 0;
    for(j = 0; j < i; j++)
        ;
}


/*************************END**************************/

