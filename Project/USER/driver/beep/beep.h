#ifndef __BEEP_H__
#define __BEEP_H__
void Beep_Init(void);
void Beep_On(void);
void Beep_Off(void);
void Beep_Delay(int i);
void Beep_Beep(int  time, unsigned char  frequency);



#endif
/*********************END*****************/


