#ifndef _DOWNLOAD_H_
#define _DOWNLOAD_H_

#include "stm32f10x_gpio.h"

typedef enum {
    D_START,  // the first packet
    D_DATA,   // normal data, will be cached by page
    D_STOP,   // the last packet
} AppWriteStage;


#define STM_SECTOR_SIZE	2048
#define STM32_FLASH_BASE 0x08000000 	//STM32 FLASH的起始地址
#define STM32_FLASH_SIZE 512 	 		//所选STM32的FLASH容量大小(单位为K)

void FLASH_ProgramStart(void);
void FLASH_ProgramDone(void);
void STMFLASH_Write_NoCheck(u32 WriteAddr,u16 *pBuffer,u16 NumToWrite);
u32 FLASH_WriteBank(u8 *pData, u32 addr, u16 size);
void STMFLASH_Read(u32 ReadAddr,u16 *pBuffer,u16 NumToRead);
void iap_write_appbin(u32 addr,u8 *buf,u32 len,AppWriteStage stage);

#endif

