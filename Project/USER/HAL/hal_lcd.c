#include "hal_lcd.h"
#include "los_hwi.h"
#include "los_hwi.h"
#include "los_task.ph"
#include "los_sem.h"
#include "los_event.h"
#include "los_memory.h"
#include "los_queue.ph"
#include "los_sem.h"
#include "uart.h"
#include "string.h"
#include "stdlib.h"
#include "hal_eeprom.h"
#include "oled.h"
#include "config.h"
#include "my_lib.h"
#include "hal_senser.h"
#include "battery.h"

extern AD_Config AD_cfg[];
extern float gas_k[5][4];
extern int g_sensor_num_id;
extern int8_t BatteryStatus;
extern uint16_t BatteryValue;
extern u16 g_version;
//当前菜单结构体
extern Menu *currentMenu;
uint32_t root_count_old;
//主菜单(一级菜单)
extern Menu *mainMenu;
// 显示值中间变量
extern float LCD_DisplayTempValue;
uint8_t Fill_status = 0;
extern FlagStatus f_LCD_clear;
extern uint32_t LCD_DisplayTask;
u16 AlarmBits = 0;
//键盘按键值
uint32_t Key_value = 0;

//密码结构体
struct pwd Pwd;

uint32_t board_id;
// 报警开关状态值改变
FlagStatus alarm_off_on_status = RESET;

struct ValueRange funcRange[RANGE_INDEX_MAX];
range_index funcRangeSelect = 0;

uint32_t g_temp = 0;
/* GPRS发送周期时间设置 
 * 如果超过1天的话，需要更改数据类型 
 * uint16_t max = 65535 s
 *              = 65535 / 60 = 1092 min
 *              = 1092 / 60 = 18.2 hour
 */
uint16_t timer_intervel[9] = {30*TIME_SEC,  1*TIME_MIN,  5*TIME_MIN,
                              10*TIME_MIN, 30*TIME_MIN,  1*TIME_HOUR,
                              2*TIME_HOUR,  3*TIME_HOUR, 4*TIME_HOUR};

char timer_name[][4] = {"30S", " 1M"," 5M",
                        "10M", "30M"," 1H",
                        " 2H", " 3H"," 4H"};

						

						
//一级菜单
Menu fMenu = {
    // form, to, count, selected
    0, 3, 6, 0, 
    // items string
    {
		"液位校准      > ",
		"压力校准      > ",
		"罐体信息      > ",
		"发送时间      > ",
		"调试信息      > ",
		"版本信息      > ",
    },
};

Menu sMenuDiffpre = {
	// form, to, count, selected
    0, 3, 6, 0, 
    // items string
    {
		"液位高校准    > ",
		"液位低校准    > ",
		"电压高校准    > ",
		"电压低校准    > ",
		"通道          > ",
		"模式          > ",
    },
};

Menu sMenuPre = {
	// form, to, count, selected
    0, 3, 6, 0, 
    // items string
    {
		"压力高校准    > ",
		"压力低校准    > ",
		"电压高校准    > ",
		"电压低校准    > ",
		"通道          > ",
		"模式          > ",
    },
};

Menu sMenuTankInfo = {
	// form, to, count, selected
    0, 3, 6, 0, 
    // items string
    {
		"直径          > ",
		"长度          > ",
		"罐体类型      > ",
		"最大容积      > ",
		"id号          > ",
		"介质更改      > ",
	},
};

/*
 * 函数：LCD_displaySendTimeSet
 * 作用：GPRS发送时间间隔设置界面
 * 输入：无
 * 输出：无
 * 返回：无
 */
int LCD_displaySendTimeSet(void) {
    uint8_t i = 0, j = 0;
    
    funcRangeSelect = RANGE_TIMER_INDEX;
    for (i = 0;i < 3;i++) {
        for (j = 0;j < 3;j++) {
            if ((i*3+j) != (uint8_t)LCD_DisplayTempValue) {
                LCD_displayStr(i, j*5, timer_name[i*3+j]);
            } else {
                LCD_displayStrRev(i, j*5, timer_name[i*3+j]);                
            }
        }
    }
    
    return 0;
}

int LCD_displayLevel(void) {
    char str[20];
    funcRangeSelect = RANGE_LEVEL_INDEX;
    
    LCD_displayStr(0, 0, "压差");
    sprintf(str, "% 8d %s", (uint32_t)LCD_DisplayTempValue, "Pa");
    LCD_displayStr(1, 2, str);
	return 0;
}

int LCD_displayVoltage(void) {
    char str[20];
    
    funcRangeSelect = RANGE_VOLTAGE_INDEX;
    
    LCD_displayStr(0, 0, "电压值");
    sprintf(str, "% 6d %s", (uint32_t)LCD_DisplayTempValue, "mV");
    LCD_displayStr(1, 2, str);
    
	return 0;
}

int LCD_displayPressure(void) {
    char str[20];
    
    funcRangeSelect = RANGE_PRESSURE_INDEX;
    
    LCD_displayStr(0, 0, "压力");
    sprintf(str, "% 6d %s", (uint32_t)LCD_DisplayTempValue, "KPa");
    LCD_displayStr(1, 2, str);
    
	return 0;
}
int LCD_displayChannel(void) {
    char str[20];
    
    funcRangeSelect = RANGE_CHANNEL_INDEX;
    
    LCD_displayStr(0, 0, "通道");
    sprintf(str, "% 6d", (uint32_t)LCD_DisplayTempValue);
    LCD_displayStr(1, 2, str);
    
	return 0;
}

int LCD_displayMode(void) {
    char str[20];
    
    funcRangeSelect = RANGE_MODE_INDEX;
    
    LCD_displayStr(0, 0, "模式");
	if ((uint32_t)LCD_DisplayTempValue) { // 差分
	    sprintf(str, "差分");
	} else {
	    sprintf(str, "单端");
	}
    LCD_displayStr(1, 2, str);
    
	return 0;	
}
int LCD_displayDiameter(void) {
	char str[20];
    
    funcRangeSelect = RANGE_DIAMETER_INDEX;
    
    LCD_displayStr(0, 0, "直径");
    sprintf(str, "% 7d %s", (uint32_t)LCD_DisplayTempValue, "mm");
    LCD_displayStr(1, 2, str);
    
	return 0;
}

int LCD_displayMaxVolume(void) {
	char str[20];
    
    funcRangeSelect = RANGE_VOLUME_INDEX;
    
    LCD_displayStr(0, 0, "最大容积");
    sprintf(str, "% 7d %s", (uint32_t)LCD_DisplayTempValue, "L");
    LCD_displayStr(1, 2, str);
    
	return 0;
}
int LCD_displayLength(void) {
	char str[20];
    
    funcRangeSelect = RANGE_LENGTH_INDEX;
    
    LCD_displayStr(0, 0, "长度");
    sprintf(str, "% 7d %s", (uint32_t)LCD_DisplayTempValue, "mm");
    LCD_displayStr(1, 2, str);
    
	return 0;
}

int LCD_displayTankType(void) {
	char str[20];
    
    funcRangeSelect = RANGE_STATUS_INDEX;
    
    LCD_displayStr(0, 0, "罐体类型");
	if ((uint32_t)LCD_DisplayTempValue) {
		sprintf(str, "%s", "卧式罐");
	} else {
		sprintf(str, "%s", "立式罐");
	}
    LCD_displayStr(1, 2, str);
    
	return 0;
}

int LCD_displayGasIndex(void) {
	char str[20];
    
    funcRangeSelect = RANGE_GAS_INDEX;
    
    LCD_displayStr(0, 0, "介质");
	if ((uint32_t)LCD_DisplayTempValue == 0) {
		sprintf(str, "%s", "液氧");
	} else if ((uint32_t)LCD_DisplayTempValue == 1) {
		sprintf(str, "%s", "液氩");
	} else if ((uint32_t)LCD_DisplayTempValue == 2) {
		sprintf(str, "%s", "二氧化碳");
	} else if ((uint32_t)LCD_DisplayTempValue == 3) {
		sprintf(str, "%s", "液氮");
	} else if ((uint32_t)LCD_DisplayTempValue == 4) {
		sprintf(str, "%s", "液化天然气");
	}
    LCD_displayStr(1, 2, DISPLAY_LCD_NULL);
    LCD_displayStr(1, 2, str);
    
	return 0;
}



int LCD_displayID(void) {
	char str[20];
    
    funcRangeSelect = RANGE_ID_INDEX;
    
    LCD_displayStr(0, 0, "ID");
	LCD_displayStr(0, 2, "号");
	sprintf(str, "%08d", (uint32_t)LCD_DisplayTempValue);
	LCD_displayStr(1, 2, str);
    
	return 0;
}
int LCD_displayDubug(void) {
	char str[20];
	
	sprintf(str, "%s", g_board_info[0].sim);
	LCD_displayStr(0, 0, str);
	
	sprintf(str, "%06d %04d", debugInfo[0].dp_ad, debugInfo[0].dp_val);
	LCD_displayStr(1, 0, str);
	
	sprintf(str, "K:%.2f", g_diffpressure_k[0]);
	LCD_displayStr(2, 0, str);
	
	sprintf(str, "%06d %04d", debugInfo[0].p_ad, debugInfo[0].p_val);
	LCD_displayStr(3, 0, str);
	
	sprintf(str, "K:%.2f", g_pressure_k[0]);
	LCD_displayStr(2, 8, str);
	
	return 0;
}

int LCD_displayVersion(void) {
	char str[20];
	
	sprintf(str, "版本号");
	LCD_displayStr(1, 0, str);
	
	sprintf(str, "%X", g_version);
	LCD_displayStr(2, 3, str);
}


void LCD_displayBattery(void) {
	
}

void lcd_delay_ms(int ms) {
	LOS_TaskDelay(ms);
}


void LCD_displaySet(range_index Rindex, float dsp_value, int dsp_integer, int dsp_decimal, char* dsp_unit) {
	char str[20];
	char dsp_format[20];
	
	memset(str, 0, sizeof(str));
	memset(dsp_format, 0, sizeof(dsp_format));

	funcRangeSelect = Rindex;
	
	sprintf(dsp_format, "%%%d.%df %s", dsp_integer, dsp_decimal, dsp_unit);
	sprintf(str, dsp_format, dsp_value);
	LCD_displayStr(1, 2, str);
}


/*
 * 函数：Menu_setFuncRange
 * 作用：设置设置值的最大值，最小值，短步长，长步长
 * 输入：index        ：索引值，用于在数组中选择合适的项
 *       valueMin     ：最小值
 *       valueMax     ：最大值
 *       valueAddSmall：短步长
 *       valueAddBig  ：长步长
 * 输出：无
 * 返回：无
 */
void Menu_setFuncRange(range_index index, float valueMin, float valueMax, float valueAddSmall, float valueAddBig)
{
    funcRange[index].valueMax = valueMax;
    funcRange[index].vlaueMin = valueMin;
    funcRange[index].valueAddSmall = valueAddSmall;
    funcRange[index].valueAddBig = valueAddBig;
}

/*
 * 函数：Menu_mainMenuInit
 * 作用：LCD主菜单初始化
 * 输入：无
 * 输出：无
 * 返回：无
 */
void MENU_mainMenuInit(void)
{
    char str[20];
    
    funcRangeSelect = RANGE_LEVEL_INDEX;
    // mainMneu 取值
    mainMenu = &fMenu;
	mainMenu->menuId[0] = 0;
    
    if(mainMenu == NULL){
		printf("menu list init error\r\n");
        return ;
    }else{
		mainMenu->menuId[0] = 0;
    }

    // 设置范围
    Menu_setFuncRange(RANGE_VOLTAGE_INDEX,  RANGE_VOLTAGE_MIN,  RANGE_VOLTAGE_MAX,  	RANGE_VOLTAGE_SMALL,  	RANGE_VOLTAGE_BIG);
    Menu_setFuncRange(RANGE_STATUS_INDEX,  	RANGE_STATUS_MIN,  	RANGE_STATUS_MAX,  		RANGE_STATUS_SMALL,  	RANGE_STATUS_BIG);
    Menu_setFuncRange(RANGE_LEVEL_INDEX,  	RANGE_LEVEL_MIN,  	RANGE_LEVEL_MAX,  		RANGE_LEVEL_SMALL,  	RANGE_LEVEL_BIG);
    Menu_setFuncRange(RANGE_TIMER_INDEX,  	RANGE_TIMER_MIN,  	RANGE_TIMER_MAX,  		RANGE_TIMER_SMALL,  	RANGE_TIMER_BIG);
    Menu_setFuncRange(RANGE_GAS_INDEX,    	RANGE_GAS_MIN,    	RANGE_GAS_MAX,    		RANGE_GAS_SMALL,    	RANGE_GAS_BIG); 
    Menu_setFuncRange(RANGE_DIAMETER_INDEX, RANGE_DIAMETER_MIN, RANGE_DIAMETER_MAX,     RANGE_DIAMETER_SMALL,   RANGE_DIAMETER_BIG);
    Menu_setFuncRange(RANGE_LENGTH_INDEX, 	RANGE_LENGTH_MIN,   RANGE_LENGTH_MAX,     	RANGE_LENGTH_SMALL,     RANGE_LENGTH_BIG);
    Menu_setFuncRange(RANGE_VOLUME_INDEX,   RANGE_VOLUME_MIN,   RANGE_VOLUME_MAX,    	RANGE_VOLUME_SMALL,     RANGE_VOLUME_BIG);
    Menu_setFuncRange(RANGE_ID_INDEX,     	RANGE_ID_MIN,     	RANGE_ID_MAX,     		RANGE_ID_SMALL,     	RANGE_ID_BIG);
    Menu_setFuncRange(RANGE_CHANNEL_INDEX, 	RANGE_CHANNEL_MIN,	RANGE_CHANNEL_MAX,     	RANGE_CHANNEL_SMALL,    RANGE_CHANNEL_BIG);
    Menu_setFuncRange(RANGE_PRESSURE_INDEX, RANGE_PRESSURE_MIN, RANGE_PRESSURE_MAX,     RANGE_PRESSURE_SMALL,   RANGE_PRESSURE_BIG);
    Menu_setFuncRange(RANGE_MODE_INDEX,     RANGE_MODE_MIN,     RANGE_MODE_MAX,    	 	RANGE_MODE_SMALL,     	RANGE_MODE_BIG);
    
	
	// 液位高设置
    Menu_addFunc(&sMenuDiffpre, &LCD_displayLevel, 		0, &(g_board_info[0].diffpressure_ValueHigh));
	// 液位低设置
    Menu_addFunc(&sMenuDiffpre, &LCD_displayLevel, 		1, &(g_board_info[0].diffpressure_ValueLow));
	// 液位高设置电压
    Menu_addFunc(&sMenuDiffpre, &LCD_displayVoltage, 	2, &(g_board_info[0].diffpressure_VoltageHigh));
	// 液位低设置电压
    Menu_addFunc(&sMenuDiffpre, &LCD_displayVoltage, 	3, &(g_board_info[0].diffpressure_VoltageLow));
	// 液位传感器通道设置
    Menu_addFunc(&sMenuDiffpre, &LCD_displayChannel, 	4, &(AD_cfg[DIFFPRESSURE_SENSOR].AD_Channel));
	// 液位传感器模式设置
    Menu_addFunc(&sMenuDiffpre, &LCD_displayMode, 	    5, &(AD_cfg[DIFFPRESSURE_SENSOR].AD_Mode));
	
	
	// 压力高设置
    Menu_addFunc(&sMenuPre, &LCD_displayPressure, 	0, &(g_board_info[0].pressure_ValueHigh));
	// 压力低设置
    Menu_addFunc(&sMenuPre, &LCD_displayPressure, 	1, &(g_board_info[0].pressure_ValueLow));
	// 压力高设置电压
    Menu_addFunc(&sMenuPre, &LCD_displayVoltage, 	2, &(g_board_info[0].pressure_VoltageHigh));
	// 压力低设置电压
    Menu_addFunc(&sMenuPre, &LCD_displayVoltage, 	3, &(g_board_info[0].pressure_VoltageLow));
	// 压力传感器通道设置
    Menu_addFunc(&sMenuPre, &LCD_displayChannel, 	4, &(AD_cfg[INPUTPRESSURE_SENSOR].AD_Channel));
	// 压力传感器模式设置
    Menu_addFunc(&sMenuPre, &LCD_displayMode, 	    5, &(AD_cfg[INPUTPRESSURE_SENSOR].AD_Mode));
	
	
	
	// 直径
	Menu_addFunc(&sMenuTankInfo, &LCD_displayDiameter, 	0, &(g_board_info[0].diameter));
	// 长度
	Menu_addFunc(&sMenuTankInfo, &LCD_displayLength, 	1, &(g_board_info[0].length));
	// 罐体类型
	Menu_addFunc(&sMenuTankInfo, &LCD_displayTankType, 	2, &(g_board_info[0].tank_type));
	// 最大容积
	Menu_addFunc(&sMenuTankInfo, &LCD_displayMaxVolume, 3, &(g_board_info[0].maxVolume));
	// 罐体ID
	Menu_addFunc(&sMenuTankInfo, &LCD_displayID, 		4, &(g_board_info[0].id));
	// 介质设置
	Menu_addFunc(&sMenuTankInfo, &LCD_displayGasIndex,  5, &(g_board_info[0].gas_index));
	
	
	// 液位校准
	Menu_addItem(mainMenu, &sMenuDiffpre, 0);
	// 压力校准
	Menu_addItem(mainMenu, &sMenuPre, 1);
	// 罐体信息设置
	Menu_addItem(mainMenu, &sMenuTankInfo, 2);
	// 发送时间设置
	Menu_addFunc(mainMenu, &LCD_displaySendTimeSet, 3, &(g_board_info[0].sendTimeIndex));
	// debug界面
	Menu_addFunc(mainMenu, &LCD_displayDubug, 4, &(g_temp));
	// version 界面
	Menu_addFunc(mainMenu, &LCD_displayVersion, 5, &(g_temp));
  
	// 当前菜单为mainMenu
    currentMenu = mainMenu;
}

/*
 * 函数：LCD_displayLogin
 * 作用：LCD显示登录
 * 输入：无
 * 输出：无
 * 返回：无
 */
/*
 *  ------------------
 *  |                |
 *  | Password:      |
 *  |     ******     |
 *  |                |
 *  ------------------
 */
void LCD_displayLogin(void)
{
    int i, pwd_len;

    pwd_len = strlen(BOARD_PWD_USER);
        
    LCD_displayStr(1, 0, "密码");
        
    if(Key_value & KEY_MENU){
        strcpy(Pwd.value, "0000");
        Pwd.selected = 0;
    }
    //上键
    if(Key_value & KEY_DOWN){
        //索引值增加
        Pwd.selected++;
        printf("pwd selected:%d\r\n", Pwd.selected);
        if(Pwd.selected == pwd_len){
            Pwd.selected = 0;
        }
    }
    if(Key_value & KEY_UP){
        //被选中的值增加
        Pwd.value[Pwd.selected]++;
        printf("pwd selected item value:%d\r\n", Pwd.value[Pwd.selected] - '0');
        if(Pwd.value[Pwd.selected] > '9')
            Pwd.value[Pwd.selected] = '0';
    }
    if(Key_value & KEY_ENTER){
        if(!strcmp(Pwd.value, BOARD_PWD_ROOT)){
            //ROOT登录
            LCD_DisplayTask |= TASK_ROOT;
            LCD_DisplayTask |= TASK_LOGIN;
            LCD_displayStr(3, 0, "管理员登录");
        }else if(!strcmp(Pwd.value, BOARD_PWD_USER)){
            //user登录
            LCD_DisplayTask &= ~TASK_ROOT;
            LCD_DisplayTask |= TASK_LOGIN;
            LCD_displayStr(3, 0, "用户登录");            
        }else{
            //清除登录信息
            LCD_DisplayTask &= ~TASK_ROOT;
            LCD_DisplayTask &= ~TASK_LOGIN;
            //显示错误信息
            LCD_displayStr(3, 0, "密码错误");
        }
        
        //清除密码
        for(i = 0;i < strlen(BOARD_PWD_USER);i++){
            Pwd.value[i] = '0';
        }
        //密码索引值为0，从第一个开始更改
        Pwd.selected = 0;
    }
    
    //显示登录界面
    for(i = 0;i < strlen(BOARD_PWD_USER);i++){
        if(Pwd.selected != i){
            LCD_displayChar(2, i+3, Pwd.value[i]);
        }else{
            LCD_displayCharRev(2, i+3, Pwd.value[i]);
        }
    }
}

/*
 * 函数：LCD_displayFirstPage
 * 作用：LCD显示首页
 * 输入：无
 * 输出：无
 * 返回：无
 */
extern int gprs_signal;
void LCD_displayFirstPage(void)
{
    char str_display[20];
	char str[10];
	char len = 0;
	static int displayIndex = 0;
	static char batDisIndex = 0;
	static int temp = 0;
    if(Key_value & KEY_MENU){  //显示主页
        f_LCD_clear = SET;
        LCD_DisplayTask |= TASK_DFP;
        return ;
    } else if (Key_value & KEY_UP) {
		// up
	} else if (Key_value & KEY_DOWN) {
		// down
    } else if ((LCD_DisplayTask & TASK_ROOT) && (Key_value & KEY_ENTER)) {
        
    }
    
    LCD_displayStr(0, 0, DISPLAY_LCD_NULL);
    LCD_displayStr(1, 0, DISPLAY_LCD_NULL);
    LCD_displayStr(2, 0, DISPLAY_LCD_NULL);
    LCD_displayStr(3, 0, DISPLAY_LCD_NULL);
    
    memset(str_display, 0, sizeof(str_display));
	if (displayIndex <= REF_SPEED) {
		LCD_displayStr(0, 0, "液位mm");
		LCD_displayInt4824(1, 0, debugInfo[0].level);
	} else if (REF_SPEED < displayIndex && displayIndex <= 2 * REF_SPEED){
		LCD_displayStr(0, 0, "水柱mm");
		LCD_displayInt4824(1, 0, debugInfo[0].h20_mm);			
	} else if (REF_SPEED * 2 < displayIndex && displayIndex <= 3 * REF_SPEED) {
		LCD_displayStr(0, 0, "重量Kg");
		LCD_displayInt4824(1, 0, debugInfo[0].weight);			
	} else if (REF_SPEED * 3 < displayIndex && displayIndex <= 4 * REF_SPEED) {
		LCD_displayStr(0, 0, "压差Pa");
		LCD_displayInt4824(1, 0, debugInfo[0].dp_val);			
	} else if (REF_SPEED * 4 < displayIndex && displayIndex <= 5 * REF_SPEED) {
		LCD_displayStr(0, 0, "压力KPa");
		LCD_displayInt4824(1, 0, debugInfo[0].p_val);			
	}
	displayIndex = displayIndex >= 5*REF_SPEED ? 0 : displayIndex+1;
	
	if (BatteryStatus == BATTERY_NOT_CHARGE) { // 未充电
		if ((int)BatteryValue < 7000) {
			batDisIndex = batDisIndex >= 12 ? 0 : batDisIndex + 1;
			if (batDisIndex > 6) {
				OLED_DisplayBatteryStatus(4*16, 0, 1, 0);
			}
		} else {
			OLED_DisplayBatteryStatus(4*16, 0, 1, ((int)(BatteryValue>6600?BatteryValue:6600) - 6600) / ((8400-6600) / 4));
		}	
	} else if (BatteryStatus == BATTERY_NOT_INSERT) { // 无电池
		batDisIndex = 0;
	} else if (BatteryStatus == BATTERY_CHARGE) { // 充电中
		batDisIndex = batDisIndex >= 18 ? 0 : batDisIndex + 1;
		OLED_DisplayBatteryStatus(4*16, 0, 1, batDisIndex / 6 + 1);
	} else if (BatteryStatus == BATTERY_FILL) { // 充满
		batDisIndex = 0;
		OLED_DisplayBatteryStatus(4*16, 0, 1, ((int)(BatteryValue>6600?BatteryValue:6600) - 6600) / ((8400-6600) / 4));
	}
	
	gprs_signal = gprs_signal > 4 ? 4 : gprs_signal;
	OLED_DisplayAngle(7*16, 0, 1, gprs_signal);
}
/*
 * 函数：LCD_displayMenu
 * 作用：LCD显示菜单，以及菜单的选择、进入、退出以及调用函数进行参数设置
 * 输入：无
 * 输出：无
 * 返回：无
 */
void LCD_displayMenu(void)
{    
    if(Key_value == KEY_MENU){
        Menu_return(currentMenu);
    }else if(Key_value == KEY_MENU_C){
        if(currentMenu == mainMenu){
            f_LCD_clear = SET;
            LCD_DisplayTask &= ~TASK_LOGIN;
            LCD_DisplayTask &= ~TASK_DFP;
        }
    }else if(Key_value == KEY_UP){
        if(currentMenu->selected < currentMenu->itemCount - 1){
            currentMenu->selected++;
        }else{
            currentMenu->selected = 0;
        }
        //更新当前菜单显示索引区间
        currentMenu->range_from = (currentMenu->selected/4)*4;
        currentMenu->range_to = currentMenu->range_from+3;
        if(currentMenu->range_to > currentMenu->itemCount-1){
            currentMenu->range_to = currentMenu->itemCount-1;
        }        
    }else if(Key_value == KEY_DOWN){
        if(currentMenu->selected > 0){
            currentMenu->selected--;
        }else{
            currentMenu->selected = currentMenu->itemCount-1;
        }
        //更新当前菜单显示索引区间
        currentMenu->range_from = (currentMenu->selected/4)*4;
        currentMenu->range_to = currentMenu->range_from+3;
        if(currentMenu->range_to > currentMenu->itemCount-1){
            currentMenu->range_to = currentMenu->itemCount-1;
        }
    }else if(Key_value == KEY_ENTER) {
		printf("enter menu\r\n");
        Menu_enter(currentMenu, currentMenu->selected);
    }
    
    //显示当前菜单
    Menu_displayNor(currentMenu);
}

/*
 * 函数：LCD_runFunc
 * 作用：设置函数运行按键监测
 * 输入：无
 * 输出：无
 * 返回：无
 */
void LCD_runFunc(void)
{
	char str[20];
    if(Key_value == KEY_MENU) {
        if(LCD_DisplayTask & TASK_SET) {            
            // LCD清屏操作
            f_LCD_clear = SET;
            // 清除设置任务标志
            LCD_DisplayTask &= ~TASK_SET;
        }
    }else if(LCD_DisplayTask & TASK_ROOT) {  // ROOT权限
        if(Key_value == KEY_UP) {  // 值增加
            LCD_DisplayTempValue += funcRange[funcRangeSelect].valueAddSmall;
        
            if(LCD_DisplayTempValue > funcRange[funcRangeSelect].valueMax) {
                LCD_DisplayTempValue = funcRange[funcRangeSelect].vlaueMin;
            }
			
			printf("value:%f\r\n", LCD_DisplayTempValue);
        }else if(Key_value == KEY_UP_C) { // 值长按增加
            LCD_DisplayTempValue += funcRange[funcRangeSelect].valueAddBig;
            
            if(LCD_DisplayTempValue > funcRange[funcRangeSelect].valueMax) {
                LCD_DisplayTempValue = funcRange[funcRangeSelect].vlaueMin;
            }
        }else if(Key_value == KEY_DOWN) {
            LCD_DisplayTempValue -= funcRange[funcRangeSelect].valueAddSmall;
            
            if(LCD_DisplayTempValue < funcRange[funcRangeSelect].vlaueMin) {
                LCD_DisplayTempValue = funcRange[funcRangeSelect].valueMax;
            }
        }else if(Key_value == KEY_DOWN_C) {
            LCD_DisplayTempValue -= funcRange[funcRangeSelect].valueAddBig;
            
            if(LCD_DisplayTempValue < funcRange[funcRangeSelect].vlaueMin) {
                LCD_DisplayTempValue = funcRange[funcRangeSelect].valueMax;
            }
        }else if(Key_value == KEY_ENTER){
            // 保存函数显示值
			if ((uint32_t)currentMenu->funcValue[currentMenu->selected] == (uint32_t)(g_board_info[0].id)) {
				my_itoa((uint32_t)LCD_DisplayTempValue, str, 8);
				strcpy((char *)currentMenu->funcValue[currentMenu->selected], str);
			} else {
				*(currentMenu->funcValue[currentMenu->selected]) = (uint32_t)LCD_DisplayTempValue;
			}
            if(LCD_DisplayTask & TASK_SET) {
				LCD_displayStr(3, 0, "保存中");
				LCD_Dispaly();
                eeprom_SaveBoardInfo();
				eeprom_GetBoardInfo();
				calc_KValue();
                // LCD清屏操作
                f_LCD_clear = SET;
                // 清除设置任务标志
                LCD_DisplayTask &= ~TASK_SET;
            }
        }
    }
}

/*
 * 函数：PWD_setValue
 * 作用：密码值重设
 * 输入：my_pwd：密码结构体指针  my_passwordStr：更改的字符串
 * 输出：无
 * 返回：无
 */
void PWD_setValue(struct pwd * my_pwd, char * my_passwordStr)
{
    strcpy(my_pwd->value, my_passwordStr);
}
/*
 * 函数：PWD_init
 * 作用：密码初始化函数
 * 输入：my_pwd：密码结构体指针
 * 输出：无
 * 返回：无
 */
bool PWD_init(struct pwd * pwd_InitStruct)
{    
    memset(pwd_InitStruct, 0, sizeof(struct pwd));
    PWD_setValue(pwd_InitStruct, BOARD_PWD_USER);
    pwd_InitStruct->selected = 0;
    
    return TRUE;
}

void lcd_task(void) {
	const uint16_t MENU_DELAY = 100; 
	uint32_t a = 0;
	OLED_Init();			//初始化OLED      
  	
	key_init();
	
	// 自动登录
    LCD_DisplayTask |= TASK_LOGIN;
    //密码初始化
    PWD_init(&Pwd);
    printf("default Pwd [%s]\r\n", Pwd.value);
    //菜单初始化
    MENU_mainMenuInit();
    MSG_printfln("menu init successfully\r\n");
    LCD_Clear();
	while(TRUE){
		Key_value = key_get_status();
		// 如果需要清除屏幕
        if(f_LCD_clear == SET){
            f_LCD_clear = RESET;
            LCD_Clear();
        }
        
        if(LCD_DisplayTask & TASK_LOGIN) {
            //不显示首页(display first page)
            if(LCD_DisplayTask & TASK_DFP) {
                if(!(LCD_DisplayTask & TASK_SET)) {
                    // 显示菜单
                    LCD_displayMenu();
                }else{
                    // 进入设置函数
                    (*currentMenu->func[currentMenu->selected])();
                    // 执行判别函数
                    LCD_runFunc();
                }
            }else{
                LCD_displayFirstPage();
            }
        }else{
            LCD_displayLogin();
        }
        
        Key_value = 0;
        lcd_delay_ms(MENU_DELAY);
		LCD_Dispaly();
	}
}
