#include "display.h"
#include "stdlib.h"
#include "oled.h"
#include "config.h"
#include "my_lib.h"
Menu *currentMenu = NULL;//当前的菜单
Menu *mainMenu = NULL;

uint32_t LCD_DisplayTask;
FlagStatus f_LCD_clear;
extern uint32_t board_id;
// 显示值中间变量
float LCD_DisplayTempValue = 0;
//
// 显示菜单选项
// menu：菜单指针
//
void Menu_displayNor(Menu *menu) {
    uint8_t i, count = menu->range_to - menu->range_from + 1;
 
    if (menu == NULL) {
        DEBUGOUT("menu_displayNor");
        return ;
    }

    if (count > menu->itemCount) {
        DEBUGOUT("menu count out");
        printf("to=%d, form=%d\r\n", menu->range_to, menu->range_from);
        printf("count=%d, itemcount=%d\r\n", count, menu->itemCount);
        menu->range_to = menu->itemCount - 1;
        count = menu->itemCount;
    }
    
    for (i = 0;i<count;i++) {
        if (i+menu->range_from != menu->selected) {  // 不是当前被选项
            LCD_displayStr(i, 0, menu->menuItems[i+menu->range_from]);
        } else {
            LCD_displayStrRev(i, 0, menu->menuItems[i+menu->range_from]);
        } 
    }
    // 补全剩余行数 为空行
    for (;i<4;i++) {
        LCD_displayStr(i, 0, DISPLAY_LCD_NULL);
    }
}



/*
 * 添加下级单项菜单函数
 * menu：        菜单指针
 * submenus：    下级单项菜单
 * index：       索引值
 * return：索引值 error：-1  
 */
int Menu_addItem(Menu *menu, Menu * subMenu, uint8_t index) {
    if (menu == NULL) {
        DEBUGOUT("menu is null");
        return -1;
    }
    
    if (index > menu->itemCount) {
        DEBUGOUT("index too big");
        return -2;
    }
   
    //menu的下级菜单索引值[i]为subMenu
    menu->subMenus[index] = subMenu; 
    menu->menuId[index+1] = (menu->menuId[0] << 4) | (index + 1);
    subMenu->menuId[0] = menu->menuId[index+1];
    //subMenu的上级菜单为menu
    subMenu->parent = menu;
    //printf("menu->id=%02x, submenu->id=%02x\r\n", menu->menuId[0], subMenu->menuId[0]);
    return index;
}

/*
 * 添加下级全部项菜单函数
 * menu：        菜单指针
 * submenus：    下级全部项菜单
 * parent：      上级菜单指针
 * return：subMenus成功加入的菜单值 
 */
uint8_t Menu_addItems(Menu *menu, Menu subMenus[],uint16_t count){
    int i, success_num = 0;
    
    if (NULL == subMenus) {
        DEBUGOUT("menu->subMenus is null");
        return 0;
    }
    if (count > menu->itemCount) {
        DEBUGOUT("count is too big");
        return 0;
    }
    if (menu == 0) {
        DEBUGOUT("menu is null");
        return 0;
    }
    
    for (i = 0;i < count;i++) {
        //如果下级菜单有数据的话
        if (subMenus[i].itemCount) {
            success_num++;
            menu->subMenus[i] = &subMenus[i];
        } else {
            menu->subMenus[i] = 0;
        }
        //子菜单的双亲节点为本节点
        subMenus[i].parent = menu;
    } 
    
    return success_num;
}

/*
 * 返回上级菜单
 * menu：        菜单指针
 */
void Menu_return(Menu * menu) {
    if (menu == 0){
        DEBUGOUT("menu is null");
        return ;
    }
    // LCD清屏操作
    f_LCD_clear = SET;
    
    //显示菜单
    if (LCD_DisplayTask & TASK_DFP) {
        //如果有上级菜单的话
        if (menu->parent) {
            currentMenu = menu->parent;
        } else if (menu == mainMenu) {
            //清除显示菜单位
            LCD_DisplayTask &= ~TASK_DFP;
        }
    }
}

/*
 * 进入子菜单
 * menu：        菜单指针 
 * index：       子菜单索引
 * 如果存在函数，首先运行函数。
 */
void Menu_enter(Menu * menu, uint16_t index) {
    // LCD清屏操作
    f_LCD_clear = SET;

    //索引值大于项目总数 且 不具有下级子菜单
    if (index >= menu->itemCount 
     || (menu->subMenus[index] == NULL 
     || menu->subMenus == NULL)) {
        //如果存在函数  执行函数
        if (menu->func != NULL && menu->func[index] != NULL) {
            // 设置函数设置位
            LCD_DisplayTask |= TASK_SET;
            // 取出函数显示值
			LCD_DisplayTempValue = (float)(*(menu->funcValue[index]));
			printf("funcvalue:%d, f:%f\r\n", (*(menu->funcValue[index])), LCD_DisplayTempValue);
            // 执行函数
			(*menu->func[index])();
        }
    } else {
        // 进入子菜单
        currentMenu = menu->subMenus[index];
    }
}


/*
 * 设置上级菜单
 * menu：        菜单指针 
 * parent：      上级菜单节点
 */
void Menu_setParent(Menu * menu,Menu * parent) {
    menu->parent = parent;
}
/*
 * 设置菜单项总数
 * menu：        菜单指针 
 * count：       菜单项总数
 */
void Menu_setCount(Menu * menu, uint16_t count) {
    menu->itemCount = count;
}

/*
 * 添加下级子任务函数
 * menu：       菜单指针
 * fun ：       函数指针
 * index：      索引值
 * valueAddr：  需要显示的值的地址
 */
bool Menu_addFunc(Menu * menu, int(*fun)(void), uint8_t index, uint32_t * valueAddr) {
    if (menu == NULL) {
        DEBUGOUT("menu is null");
        return FALSE;
    }
    
    if (index > menu->itemCount) {
        DEBUGOUT("index is too big");
        return FALSE;
    }
    
    if (fun == NULL) {
        DEBUGOUT("function is null");
        return FALSE;
    }
    //添加调用函数
    menu->func[index] = fun;
    menu->menuId[index+1] = (menu->menuId[0] << 4) | (index + 1);
    menu->funcValue[index] = valueAddr;
    
    return TRUE;
}
/*
 * 设置菜单项总数
 * menu：        菜单指针 
 * count：       菜单项总数
 */
int Menu_addFuncs(Menu * menu, int (**funs)(void), uint8_t count) {
    uint8_t i;
    
    if (menu == NULL) {
        DEBUGOUT("menu is null");
        return 0;
    }
    
    if (funs == NULL) {
        DEBUGOUT("function is null");
        return 0;
    }
    if (count > menu->itemCount) {
        DEBUGOUT("index is too big");
        return 0;
    }
    
    for (i = 0;i < count;i++) {
        Menu_addFunc(menu, funs[i], i, 0);
    }
    
    return count;
}

void LCD_Screen_Init(void) {
    uint8_t row;

	// 擦除屏幕
    for (row = 0;row < (DISPLAY_HEIGHT / DISPLAY_FONT_SIZE);row++) {
        LCD_displayStr(row, 0, DISPLAY_LCD_NULL);
    }
}
/*
 * 刷新屏幕
 */
void LCD_Dispaly(void) {
    OLED_Refresh_Gram();
}
/*
 * 显示字符串
 * x, y 按照字符显示的位置
 * displayStr  需要显示的值
 */
void my_LCD_Display_String(unsigned char x, unsigned char y, char *displayStr) {
    uint8_t col, len;

    if (x >= (DISPLAY_HEIGHT / DISPLAY_FONT_SIZE) || y >= (DISPLAY_WIDTH / DISPLAY_FONT_SIZE * 2) || displayStr == 0 || *displayStr == 0) {
        DEBUGOUT("");
        printf("x = %d, y = %d\r\n", x, y);
        return ;
    }
    for (col = y, len = 0;displayStr[len] != '\0'; col++, len++) {
        // 可打印字符范围
        if (displayStr[len] >= ' ' && displayStr[len] <= '~') {
            my_LCD_Display_Char(x, col, displayStr[len]);
        } else if (((uint8_t)displayStr[len] >= 0x81 && (uint8_t)displayStr[len] <= 0xFE) && ((uint8_t)(displayStr[len + 1] >= 0X40 && (uint8_t)displayStr[len + 1] <= 0XFE))){
			int msb = 0, msl = 0;
			msb = displayStr[len];
			msl = displayStr[len+1];
			my_LCD_Display_Hanzi(x, col, msb, msl);
			col++;
			len++;
		} else {
			printf("displayStr[%d]=%c\r\n", col, displayStr[len]);
        }
    }	
}
/*
 * 反向显示字符
 */
void my_LCD_Display_String_Reverse(unsigned char x, unsigned char y, char *displayStr) {
    uint8_t col, len;

    if (x >= (DISPLAY_HEIGHT / DISPLAY_FONT_SIZE) || y >= (DISPLAY_WIDTH / DISPLAY_FONT_SIZE * 2) || displayStr == 0 || *displayStr == 0) {
        DEBUGOUT("");
        printf("x = %d, y = %d\r\n", x, y);
        return ;
    }

    for (col = y, len = 0;displayStr[len] != '\0'; col++, len++) {
        // 可打印字符范围
        if (displayStr[len] >= 0x20 && displayStr[len] <= 0x7E) {
            my_LCD_Display_Char_Reverse(x, col, displayStr[len]);
        } else if (((uint8_t)displayStr[len] >= 0x81 && (uint8_t)displayStr[len] <= 0xFE) && ((uint8_t)(displayStr[len + 1] >= 0X40 && (uint8_t)displayStr[len + 1] <= 0XFE))){
			int msb = 0, msl = 0;
			msb = displayStr[len];
			msl = displayStr[len+1];
			my_LCD_Display_Hanzi_Reverse(x, col, msb, msl);
			col++;
			len++;
		} else {
            printf("displayStr[%d]=%c\r\n", col, displayStr[len]);
        }
    }
}

void my_LCD_Display_Char(unsigned char x, unsigned char y, char displayChar) {
    if (x >= (DISPLAY_HEIGHT / DISPLAY_FONT_SIZE) || y >= (DISPLAY_WIDTH / DISPLAY_FONT_SIZE * 2) || displayChar < ' ' || displayChar > '~') {
        DEBUGOUT("");
        printf("x = %d, y = %d\r\n", x, y);
        return ;
    }

	OLED_ShowChar(y * DISPLAY_FONT_SIZE / 2, x * DISPLAY_FONT_SIZE, displayChar, DISPLAY_FONT_SIZE, 1);
}

void my_LCD_Display_Char_Reverse(unsigned char x, unsigned char y, char displayChar) {
    if (x >= (DISPLAY_HEIGHT / DISPLAY_FONT_SIZE) || y >= (DISPLAY_WIDTH / DISPLAY_FONT_SIZE * 2) || displayChar < ' ' || displayChar > '~') {
        DEBUGOUT("");
        printf("x = %d, y = %d\r\n", x, y);
        return ;
    }
    
	OLED_ShowChar(y * DISPLAY_FONT_SIZE / 2, x * DISPLAY_FONT_SIZE, displayChar, DISPLAY_FONT_SIZE, 0);
}
#ifdef USE_HANZI
void my_LCD_Display_Hanzi(unsigned char x, unsigned char y, u8 msb, u8 msl) {
	if (x >= (DISPLAY_HEIGHT / DISPLAY_FONT_SIZE) || y >= (DISPLAY_WIDTH / DISPLAY_FONT_SIZE * 2)) {
        DEBUGOUT("");
        printf("x = %d, y = %d\r\n", x, y);
        return ;
    }
	
	OLED_DisplayHanzi(y * DISPLAY_FONT_SIZE / 2, x * DISPLAY_FONT_SIZE, msb, msl, DISPLAY_FONT_SIZE, 1);
}


void my_LCD_Display_Hanzi_Reverse(unsigned char x, unsigned char y, u8 msb, u8 msl) {
	if (x >= (DISPLAY_HEIGHT / DISPLAY_FONT_SIZE) || y >= (DISPLAY_WIDTH / DISPLAY_FONT_SIZE * 2)) {
        DEBUGOUT("");
        printf("x = %d, y = %d\r\n", x, y);
        return ;
    }
	
	OLED_DisplayHanzi(y * DISPLAY_FONT_SIZE / 2, x * DISPLAY_FONT_SIZE, msb, msl, DISPLAY_FONT_SIZE, 0);
}


void my_LCD_Display_Hanzi_String_Reverse(unsigned char x, unsigned char y, char *displayStr) {
    uint8_t col, len;
	uint8_t msb, msl;
	
    if (x >= (DISPLAY_HEIGHT / DISPLAY_FONT_SIZE) || y >= (DISPLAY_WIDTH / DISPLAY_FONT_SIZE * 2) || displayStr == 0 || *displayStr == 0) {
        DEBUGOUT("");
        printf("x = %d, y = %d\r\n", x, y);
        return ;
    }
    for (col = y, len = 0;displayStr[len] != '\0'; col+=2, len+=2) {
		msb = displayStr[len];
		msl = displayStr[len+1];
        my_LCD_Display_Hanzi_Reverse(x, col, msb, msl);
    }
}
void my_LCD_Display_Hanzi_String(unsigned char x, unsigned char y, char *displayStr) {
    uint8_t col, len;
	uint8_t msb, msl;
	
    if (x >= (DISPLAY_HEIGHT / DISPLAY_FONT_SIZE) || y >= (DISPLAY_WIDTH / DISPLAY_FONT_SIZE * 2) || displayStr == 0 || *displayStr == 0) {
        DEBUGOUT("");
        printf("x = %d, y = %d\r\n", x, y);
        return ;
    }
    for (col = y, len = 0;displayStr[len] != '\0'; col+=2, len+=2) {
		msb = displayStr[len];
		msl = displayStr[len+1];
        my_LCD_Display_Hanzi(x, col, msb, msl);
    }
}
#endif
void my_LCD_Display_Clear(void) {
    uint8_t row;

    for (row = 0;row < (DISPLAY_HEIGHT / DISPLAY_FONT_SIZE);row++) {
        my_LCD_Display_String(row, 0, DISPLAY_LCD_NULL);
    }
}

