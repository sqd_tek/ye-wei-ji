#ifndef __HAL_EEPROM_H
#define __HAL_EEPROM_H

#include "24cxx.h" 
#include "config.h"

#ifndef TANK_MAX_LEN
	#define TANK_MAX_LEN				1       // 支持最多罐的数据上传，在config.h里有定义，如果没有定义，此处默认为1个罐
#endif
#define EEPROM_ID_ADDR					0		// 32bit

#define EEPROM_DP_VOL_HIGH_ADDR			12		// 32bit
#define EEPROM_DP_VOL_LOW_ADDR			16		// 32bit
#define EEPROM_P_VOL_HIGH_ADDR			20		// 32bit
#define EEPROM_P_VOL_LOW_ADDR			24		// 32bit
#define EEPROM_DP_VALUE_HIGH_ADDR		28		// 32bit
#define EEPROM_DP_VALUE_LOW_ADDR		32		// 32bit
#define EEPROM_P_VALUE_HIGH_ADDR		36		// 32bit
#define EEPROM_P_VALUE_LOW_ADDR			40		// 32bit

#define EEPROM_T_VOL_HIGH_ADDR			(EEPROM_PAR_MAX_ADDR + 20)		// 32bit
#define EEPROM_T_VOL_LOW_ADDR			(EEPROM_PAR_MAX_ADDR + 24)		// 32bit
#define EEPROM_T_VALUE_HIGH_ADDR		(EEPROM_PAR_MAX_ADDR + 36)		// 32bit
#define EEPROM_T_VALUE_LOW_ADDR			(EEPROM_PAR_MAX_ADDR + 40)		// 32bit




#define EEPROM_FLAG_ADDR				44		// 16bit

#define EEPROM_LENGTH_ADDR				48
#define EEPROM_DIAMETER_ADDR			52
#define EEPROM_DP_A_ADDR				56
#define EEPROM_DP_B_ADDR				60
#define EEPROM_IP_A_ADDR				64
#define EEPROM_IP_B_ADDR				68
#define EEPROM_MASK_DATA_ADDR			72	
#define EEPROM_MASK_ALARM_ADDR			76	

#define EEPROM_ALARM_DPHH_ADDR			80
#define EEPROM_ALARM_DPH_ADDR			84
#define EEPROM_ALARM_DPLL_ADDR			88
#define EEPROM_ALARM_DPL_ADDR			92
#define EEPROM_ALARM_IPH_ADDR			96

#define EEPROM_MAX_VOLUME_ADDR			100

#define EEPROM_TANK_TYPE_ADDR			104
#define EEPROM_SEND_TIME_ADDR			108
#define EEPROM_GAS_INDEX_ADDR			112
#define EEPROM_PAR_MAX_ADDR				224		


#define EEPROM_AD_BASE_ADDR				224
#define EEPROM_AD_END_ADDR				(EEPROM_AD_BASE_ADDR + 4 * 8 - 1)  // 最多可以保存8组AD配置信息 224 + 4 * 8 - 1 = 255




//#define EEPROM_ALARM_DPHH_ADDR			40
//#define EEPROM_ALARM_DPH_ADDR			44
//#define EEPROM_ALARM_DPLL_ADDR			48
//#define EEPROM_ALARM_DPL_ADDR			52
//#define EEPROM_ALARM_IPHH_ADDR			56
//#define EEPROM_ALARM_IPH_ADDR			60
//#define EEPROM_ALARM_OPHH_ADDR			64
//#define EEPROM_ALARM_OPH_ADDR			68

//#define EEPROM_ALARM_TL_ADDR			72
//#define EEPROM_ALARM_TLL_ADDR			76

//#define EEPROM_ALARM_DENS1HH_ADDR		80
//#define EEPROM_ALARM_DENS1H_ADDR		84
//#define EEPROM_ALARM_DENS2HH_ADDR		88
//#define EEPROM_ALARM_DENS2H_ADDR		92

	
#define BOARD_ID						0//"00000034"
#define BOARD_DP_VOL_HIGH				4500    //mV
#define BOARD_DP_VOL_LOW			 	500    	//mV
#define BOARD_P_VOL_HIGH				4500	//mV
#define BOARD_P_VOL_LOW					500	    //mV
#define BOARD_T_VOL_HIGH				4500	//mV
#define BOARD_T_VOL_LOW					500	    //mV
#define BOARD_DP_VALUE_HIGH				50000	//Pa
#define BOARD_DP_VALUE_LOW				0
#define BOARD_P_VALUE_HIGH				4000	//KPa
#define BOARD_P_VALUE_LOW				0
#define BOARD_T_VALUE_HIGH				323	//
#define BOARD_T_VALUE_LOW				73
#define BOARD_FLAG						0x55


#define BOARD_AD						((100 << 12) | (0 << 10) | (0 << 8) | 0)

#define BOARD_MAX_VOLUME				60000  // L
#define BOARD_LENGTH					13908  // mm
#define BOARD_DIAMETER					2500   // mm

#define BOARD_SEND_TIME_INDEX			0
#define BOARD_GAS_INDEX					0

#define BOARD_DP_A						1
#define BOARD_DP_B						(0)
#define BOARD_IP_A						1
#define BOARD_IP_B						0

#define BOARD_MASK_DATA					0X0F
#define BOARD_MASK_ALARM				0x3F


#define BOARD_ALARM_DPHH				1200	
#define BOARD_ALARM_DPH					1100
#define BOARD_ALARM_DPLL				80
#define BOARD_ALARM_DPL					20
#define BOARD_ALARM_IPH					1050

#define BOARD_PWD_USER					"1234"
#define BOARD_PWD_ROOT					"2048"

#define BOARD_TANK_NUM_MAX				2


void eeprom_task(void);
void hal_eeprom_Init(void);
uint16_t eeprom_Read16Bits(uint32_t ReadAddr);
void eeprom_Write16Bits(uint32_t WriteAddr, uint16_t WriteData);
uint32_t eeprom_Read32Bits(uint32_t ReadAddr);
void eeprom_Write32Bits(uint32_t WriteAddr, uint32_t data);
void eeprom_SetBoardInfo(void);
void eeprom_SetBoardDefault(void);
void eeprom_SaveBoardInfo(void);
void eeprom_GetBoardInfo(void);
void readID(void);
void writeID(uint32_t id);
int getTankNumID(void);
void setTankNumID(int id);
uint32_t getMask(int id, int mask_mode);
void setMask(int id, int mask_mode, uint32_t mask_value);
#endif
