#include "hal_gprs.h"
#include "stdio.h"
#include "string.h"
#include "los_task.ph"
#include "gprsdev.h"
#include "time.h"
#include "common.h"
#include "download.h"
#include "FlashCrc.h"
#include "led.h"
#include "iwdg.h"
#include "my_lib.h"
#include "cJSON.h"
#include "hal_senser.h"

#define __DEBUG__

static char hbstr[64] = {0};
static char hbresp[128];  // what we get by heart beating
char TimeDisStr[24];
// http 上传数据的包头，可以判断出当前状态
HttpPackerHeader httpHeader;
cJSON retJson;
#ifdef __DEBUG__
	#define d_printf D_PRINTF
	extern int g_print_flag;
#else
	#define d_printf 
#endif

u32 CpuID[3];  // Hardware ID.
u16 g_version_bak = 0;
u16 g_version = 0xE101;  // Software Version.
u32 g_boardid = 0x00000001;  // BoardID.
bool new_app_flag = FALSE;
char CtrlZ[3] = {0x1A, 0x1D, 0x00};  // Ctrl + z.
GprsStage gprs_stage = STAGE_unknown;  // Global gprs status.
char g_server_ip[24];
char g_server_port[8];
int gprs_signal = 0;
int SIMCardStatus;
unsigned char SendOK;
void my_hex2str(u32 hex, char* buf, u16 len);
void check_and_clear_rxbuffer(void);


static char ftp_path[64];
static char ftp_fname[64];
static char ftp_ver[8];
static u32 ftp_length;
static bool new_response_flag = FALSE;
static int gprs_update_ready = FALSE;


char g_ntp_ip[32];
char g_ntp_port[8];
extern uint16_t timer_intervel[];
extern unsigned int sqd_send_flag;
extern uint32_t sqd_gprs_count;
// 发送过程罐子编号
int g_gprs_send_id = 0;
extern int g_tank_num_id;

u32 g_timestamp = 1502002618;
u32 timestamp_synced;

bool heart_beat(bool send_ready);
int shutdown_pdp(void);


void gprs_delay_msec(u16 ms) {
    LOS_TaskDelay(ms);  // LiteOS delay ms function.
}

void GetLockCode(void) {
	CpuID[0]=*(vu32*)(0x1ffff7e8);
	CpuID[1]=*(vu32*)(0x1ffff7ec);
	CpuID[2]=*(vu32*)(0x1ffff7f0);
}
bool get_cpuid_code(void) {
	char cpu_id[50];
	
	GetLockCode();
   
	memset(cpu_id, 0, sizeof(cpu_id));
    my_hex2str(CpuID[0], &cpu_id[0], 8);
    my_hex2str(CpuID[1], &cpu_id[8], 8);
    my_hex2str(CpuID[2], &cpu_id[16], 8);
    d_printf("CPUid = %s\r\n", cpu_id);
    return LOS_OK;
}

// Update heart beat packet data.
void update_hbreq(void) {
    memset(hbstr, 0, sizeof(hbstr));
    hbstr[0] = 'D';
    my_hex2str(g_version, &hbstr[1], 4);
    my_hex2str(CpuID[0], &hbstr[5], 8);
    my_hex2str(CpuID[1], &hbstr[13], 8);
    my_hex2str(CpuID[2], &hbstr[21], 8);
    hbstr[29] = 'E';
    hbstr[30] = 'E';
    hbstr[31] = 'E';
    hbstr[32] = 'E';
    hbstr[33] = 'E';
    d_printf("heart:%s\r\n",hbstr);
    d_printf("ver:%x\r\n",g_version);
}


void init_gprs_service(void) {
    memset(g_server_ip, 0, sizeof(g_server_ip));
    memset(g_server_port, 0, sizeof(g_server_port));
    gprs_stage = STAGE_unknown;
}
// valid repsonse is something like <CR><LF><response><CR><LF>
char* read_response(char* buf) {
    char *ptr1;
    char *ptr2;

    // The first CRLF
    ptr1 = strstr(buf, "\r\n");
    if (ptr1 == NULL)
        return NULL;

    // The second CRLF
    ptr2 = ptr1 + 2;
    if (strstr(ptr2, "\r\n") != NULL)
        return ptr2;
    else
        d_printf("*** Misaligned response: [%s]\r\n", buf);

    return NULL;
}

// NOTE: buffer contents should not be changed
void check_urc_code(const char* buf) {
    // Only after ready the module could response input commands
    if (strstr(buf, "RDY") != NULL) {
        if (gprs_stage < STAGE_poweron)
            gprs_stage = STAGE_poweron;
        d_printf("GPRS is ready now.\r\n");
    }
	
	if (strstr(buf, "NORMAL POWER DOWN") != NULL) {
        gprs_stage = STAGE_poweroff;
        d_printf("*** normal power down\r\n");
    }
	
	if (strstr(buf, "ERROR") != NULL) {
        d_printf("*** Error: %s\r\n", buf);
    }
	
	if (strstr(buf, "+PDP:DEACT") != NULL) {
        gprs_stage = STAGE_shutdown;
        d_printf("*** PDP DEACT, will be shutdown\r\n");
    }
	
	if (strstr(buf, "YES") != NULL) {
        d_printf("*** SEND OK, DATA will Display\r\n");
    }
	
	if (strstr(buf, "CLOSED") != NULL) {
        // TCP connection is closed unexpected by server
        if (gprs_stage == STAGE_ready)
            gprs_stage = STAGE_idle;
        d_printf("***TCP connection is closed\r\n");
    }
	
	if (strstr(buf, "+CIPRXGET: 1") != NULL) {
        new_response_flag = TRUE;
        d_printf("*** new data are ready\r\n");
    }
	
    if (strstr(buf, "+FTPGET: 1,1") != NULL) {
		//d_printf("buf:%s",buf);
        new_app_flag = TRUE;
        d_printf("*** FTP new data are ready\r\n");
    }
}
void check_and_clear_rxbuffer(void) {
    if (strlen(GPRS_RxBuffer) > 0) {
        check_urc_code(GPRS_RxBuffer);
    }
    memset_rx_buf();
}
// cmd string needs not ending with CRLF
int send_at_command(char* cmd, char* expect, u16 timeout) {
    char *buf = NULL;
    int t = 0;
    check_and_clear_rxbuffer();
    
    send_string(cmd);
    if (strstr(cmd, "\r\n") == NULL)
        send_string("\r\n");
    gprs_delay_msec(DEFAULT_DELAY_MS);
    t = timeout / 100 + 1;
    while (t != 0) {
        buf = read_response(GPRS_RxBuffer);
        if (buf != NULL)
            break;
        gprs_delay_msec(100);
        t--;
    }
    if (buf == NULL) {
        d_printf("Command %s timeout.\r\n", cmd);
		d_printf("%s\r\n", GPRS_RxBuffer);
        return E_TIMEOUT;
    }
	
    if (strstr(buf, expect) != NULL) {
        return E_OK;
    } else {
        d_printf("Command %s failed, buf=[%s].\r\n", cmd, GPRS_RxBuffer);
        return E_FAIL;
    }
}
// Override
bool gprs_init(void) {
    GPRS_Init_Hardware();
    return LOS_OK;
}
int gprs_tcpclose(void) {
    if (send_at_command("AT+CIPCLOSE", "CLOSE OK", 10000) != E_OK) {
        d_printf("Failed to disconnect TCP connection.\r\n");
        return E_FAIL;
    } else {
        d_printf("Successfully disconnect TCP connection.\r\n");
        gprs_stage = STAGE_idle;
    }
    // Check after default stage setting, in case connection is closed unexpected
    check_and_clear_rxbuffer();
    return E_OK;
}
/*
 * 连接TCP
 * ip和port都必须传入正确，函数内部没有判断，只判断了长度
 */
int gprs_connect(char* ip, char* port) {
    u16 timeout;
    u16 len;
    char buf[64];
    
    if (gprs_stage == STAGE_ready) {
        if ((strcmp(ip, g_server_ip) != 0) || (strcmp(port, g_server_port) != 0)) {
            gprs_tcpclose();
        }
    }
        
    len = strlen(ip);
    if (len > sizeof(g_server_ip)) {
        d_printf("Error: too long ip [%s], will be cut.\r\n", ip);
        len = sizeof(g_server_ip);
    }
    memcpy(g_server_ip, ip, len);
    len = strlen(port);
    if (len > sizeof(g_server_port)) {
        d_printf("Error: too long port [%s], will be cut.\r\n", port);
        len = sizeof(g_server_port);
    }
    memcpy(g_server_port, port, len);

	if (send_at_command("AT+CSQ", "CSQ:", 1000) != E_OK)
        return E_FAIL;
    if (send_at_command("AT+CIPRXGET=1", "OK", 1000) != E_OK)
		;
  
	if (send_at_command("AT+CIPSHUT", "OK", 1000) != E_OK)
        return E_FAIL;
    if (send_at_command("AT+CSTT=CMNET", "OK", 5000) != E_OK)
        return E_FAIL;
	
	if (send_at_command("AT+CIICR", "OK", 20000) != E_OK)
        return E_FAIL;
	
	if (send_at_command("AT+CIFSR", ".", 5000) != E_OK)
        return E_FAIL;
	if (send_at_command("AT+CIPSTATUS", "OK", 500) != E_OK)
        return E_FAIL;
	if (send_at_command("AT+CIPQSEND=0", "OK", 1000) != E_OK)
        return E_FAIL;

	check_and_clear_rxbuffer();
	
	memset(buf, 0, sizeof(buf));
    memset_rx_buf();
	sprintf(buf, "AT+CIPSTART=\"TCP\",\"%s\",%s\r\n", ip, port);
	send_string(buf);

    timeout = DELAY_CIPSTART / 100 + 1;
    while (timeout != 0) {
        if (strstr(GPRS_RxBuffer, "CONNECT OK") != NULL) {
            break;
        }
        gprs_delay_msec(200);
        timeout --;
    }
	
	printf("%s\r\n", GPRS_RxBuffer);
    if (timeout == 0) {
        d_printf("Connect TCP server timeouted\r\n");
        return E_TIMEOUT;
    }

    check_and_clear_rxbuffer();
    gprs_stage = STAGE_ready;
    return E_OK;
}


/*******************************************************************************
 * Function Name :gprs_reset(bool force_reset)
 * Description   :GPRS moudle reset function.
 * Input         :none
 * Output        :none
 * Other         :Override
 * Date          :2017.06.07
 *******************************************************************************/
void gprs_reset(void) {
    int i = 0;
	
	
	d_printf("++reset++\r\n"); 
   
	/* 关机电路图:
	 * PWRKEY:   _____________  |<--- 1s~t~33s --->|  ____________
	 *                        \ |                  | /
	 *                         \|                  |/
	 *                          \__________________/
	 */
    if (GPRS_STATUS_OK()) {
        d_printf("GPRS POWERKEY DOWN.\r\n");
        GPRS_PWERKEY_Low();
        LOS_TaskDelay(2000);
        GPRS_PWERKEY_High();
        LOS_TaskDelay(1000); 
    }
    Power_GPRS_Off();
	
    GPRS_PWERKEY_Low();
    LOS_TaskDelay(2000);
    GPRS_PWERKEY_High();
    LOS_TaskDelay(1000);
    LOS_TaskDelay(30000);
    Power_GPRS_On();
    LOS_TaskDelay(30000);
	
    if (!GPRS_STATUS_OK()) {
        printf("POWERKEY UP.\r\n");
        GPRS_PWERKEY_Low();
        gprs_delay_msec(2000);
        GPRS_PWERKEY_High();
        gprs_delay_msec(1000); 
    }
	d_printf("power on\r\n");

	while ((!GPRS_STATUS_OK()) && (i < 100)) {
		i++;
		LOS_TaskDelay(100);	
	}
	if (i == 100) {
		d_printf("GPRS moudle power on faild\r\n");
	}
    // check 'RDY' message, but it may not appear
    check_urc_code(GPRS_RxBuffer);
    memset_rx_buf();
	

    gprs_delay_msec(10000); 
	
    d_printf("--reset--\r\n");
}
/*******************************************************************************
 * Function Name :gprs_poweron(void)
 * Description   :GPRS moudle poweron function.
 * Input         :none
 * Output        :none
 * Other         :Override
 * Date          :2017.06.07
 *******************************************************************************/
bool gprs_poweron(void) {

	d_printf("++poweron++\r\n");
    gprs_stage = STAGE_poweroff;
    gprs_reset();
	
    gprs_delay_msec(4000);
	if (send_at_command("AT", "OK", 200) != E_OK)
        return FALSE;
    if (send_at_command("ATE0", "OK", 1000) != E_OK)
        return FALSE;
    // CPIN is longer when first boot
    if (send_at_command("AT+CPIN?", "+CPIN: READY", 5000) != E_OK)
        return FALSE;
    if (send_at_command("AT+CSQ", "CSQ:", 700) != E_OK)
        return FALSE;

    gprs_stage = STAGE_poweron;
    memset_rx_buf();

	
	d_printf("--poweron--\r\n");
    return TRUE;
}
/*******************************************************************************
 * Function Name :gprs_poweroff(void)
 * Description   :GPRS moudle reset function.
 * Input         :none
 * Output        :none
 * Other         :Override
 * Date          :2017.06.07
 *******************************************************************************/
bool gprs_poweroff(void) {
	if (send_at_command("AT+CPOWD=1", "NORMAL POWER DOWN", 1000) != E_OK)
		;
	
	Power_GPRS_Off();  // 关闭电源
	gprs_stage = STAGE_poweroff;	// gprs 状态为关机
	httpHeader.status = HTTP_LoginOut;	// 登陆状态为 退出登陆
	
    return TRUE;
}


/*******************************************************************************
 * Function Name :gprs_moudleEnterSleep(void)
 * Description   :GPRS moudle enter sleep mode function.
 * Input         :none
 * Output        :none
 * Other         :Override
 * Date          :2017.06.07
 *******************************************************************************/
bool gprs_moudleEnterSleep(void) {
	if (send_at_command("AT+CSCLK=1", "OK", 1000) != E_OK) {
		return false;
	}
	
    return TRUE;
}


/*******************************************************************************
 * Function Name :gprs_moudleExitSleep(void)
 * Description   :GPRS moudle exit sleep mode function.
 * Input         :none
 * Output        :none
 * Other         :Override
 * Date          :2017.06.07
 *******************************************************************************/
bool gprs_moudleExitSleep(void) {
	char i = 0;
	
	for (i = 0;i < 5;i++) {
		if (send_at_command("AT", "OK", 1000) == E_OK) {
			return true;
		}
		gprs_delay_msec(10);
	}
	
	
	/*
	
	
	*/
    return false;
}


/*******************************************************************************
 * Function Name :gprs_moudleCloseSleep(void)
 * Description   :GPRS moudle exit sleep mode function.
 * Input         :none
 * Output        :none
 * Other         :Override
 * Date          :2017.06.07
 *******************************************************************************/
bool gprs_moudleCloseSleep(void) {
	if (send_at_command("AT+CSCLK=0", "OK", 1000) != E_OK) {
		return false;
	}
	
    return TRUE;
}

bool gprs_shutdown(void) {
    // already powered on
    if (GPRS_STATUS_OK() != 0 && gprs_stage < STAGE_poweron) {
        return FALSE;
    }

    if (send_at_command("AT+CIPSHUT", "OK", 200) != E_OK)
        return FALSE;

    gprs_stage = STAGE_poweron;
    memset_rx_buf();

    return TRUE;
}
bool gprs_check_registeration(void) {
    if (gprs_stage < STAGE_poweron) {
        return FALSE;
    }

    if (send_at_command("AT+CREG?", "+CREG: 1", 1000) != E_OK) {
		if (send_at_command("AT+CREG=1", "OK", 1000) != E_OK) {
			return FALSE;
		}
    }
	printf("%s\r\n", GPRS_RxBuffer);
			
    if (send_at_command("AT+CGATT?", "+CGATT: 1", 1000) != E_OK) {
        if (send_at_command("AT+CGATT=1", "OK", 5000) != E_OK) {
            return FALSE;
    	}
	}


    gprs_stage = STAGE_idle;
    memset_rx_buf();

    d_printf("GPRS module is registered to network\r\n");
    return TRUE;
}
u32 get_file_info(char* buf, char* path, char* fname, char* ver, int maxlen) {
    char* ptr;
    int len;
    u32 file_len = 0;

    d_printf("Got server response: %s\r\n", hbresp);

    ptr = strstr(hbresp, "VER=");
    if (ptr != NULL) {
        len = 0;
        ptr += strlen("VER=");
        while (ptr) {
            if (*ptr == ':' || *ptr == '\0' || *ptr == '\n')
                break;
            if (len >= 4) {
                d_printf("Error: too long version\r\n");
                break;
            }
            ver[len] = *ptr;
            ptr++;
            len++;
        }
        if (len <= 0) {
            d_printf("Invalid version\r\n");
            return 0;
        }
    } else {
        d_printf("Invalid heatbeat response VER.\r\n");
        return 0;
    }
    d_printf("server VER=%s\r\n", ver);

    ptr = strstr(hbresp, "PATH=");
    if (ptr != NULL) {
        len = 0;
        ptr += strlen("PATH=");
        while (ptr) {
            if (*ptr == ':' || *ptr == '\0' || *ptr == '\n')
                break;
            if (len >= maxlen) {
                d_printf("Error: too long path\r\n");
                break;
            }
            path[len] = *ptr;
            ptr++;
            len++;
        }
        if (len <= 0) {
            d_printf("Invalid file path\r\n");
            return 0;
        }
    } else {
        d_printf("Invalid heatbeat response PATH.\r\n");
        return 0;
    }
    d_printf("server PATH=%s\r\n", path);

    ptr = strstr(hbresp, "FNAME=");
    if (ptr != NULL) {
        len = 0;
        ptr += strlen("FNAME=");
        while (ptr) {
            if (*ptr == ':' || *ptr == '\0' || *ptr == '\n')
                break;
            if (len >= maxlen) {
                d_printf("Error: too long file name\r\n");
                break;
            }
            fname[len] = *ptr;
            ptr++;
            len++;
        }
        if (len <= 0) {
            d_printf("Invalid file name\r\n");
            return 0;
        }
    } else {
        d_printf("Invalid heatbeat response FNAME.\r\n");
        return 0;
    }
    d_printf("server FNAME=%s\r\n", fname);

    ptr = strstr(hbresp, "LEN=");
    if (ptr != NULL) {
        len = 0;
        ptr += strlen("LEN=");
        file_len = my_str2hex(ptr);
    } else {
        d_printf("Invalid heatbeat response.\r\n");
        return 0;
    }
    d_printf("server LEN=0x%x\r\n", file_len);

    return file_len;
}

bool is_gprs_update_ready(void) {
    // at least one page is downloaded
    return (gprs_update_ready && ftp_length > 2048) ? TRUE : FALSE;
}
int ftp_read_file(char* path, char* fname, u32* size_read) {
    char buf[64];
    char* ptr;
    char* ptr2 = NULL;
    u16 timeout;
    u32 file_size = 0;
    int len = 0;
    int code;
    u16 packet;
	
    // default size of read
    *size_read = 0;
	d_printf("ftp:gprs_stage = %d\r\n", gprs_stage);
    if (gprs_stage < STAGE_idle) {
        gprs_check_registeration();
        if (gprs_stage < STAGE_idle) {
            d_printf("Network is not ready\r\n");
            return E_BUSY;
        }
    }

	d_printf("gprs_stage = %d\r\n", gprs_stage);
    check_and_clear_rxbuffer();

    if (send_at_command("AT+SAPBR=3,1,\"CONTYPE\",\"GPRS\"", "OK", 200) != E_OK) {
        d_printf("FTPSAPBR contype failed\r\n");
        return E_FAIL;
    }
	
	d_printf("gprs_stage = %d\r\n", gprs_stage);
    if (send_at_command("AT+SAPBR=3,1,\"APN\",\"CMNET\"", "OK", 200) != E_OK) {
        d_printf("FTPSAPBR apn failed\r\n");
        return E_FAIL;
    }
	
	d_printf("gprs_stage = %d\r\n", gprs_stage);
    if (send_at_command("AT+SAPBR=1,1", "OK", 2000) != E_OK) {
//		IWDG_Init_3S();
//		while(1);
//		return E_FAIL;
    }
	
	d_printf("gprs_stage = %d\r\n", gprs_stage);
    if (send_at_command("AT+FTPCID=1", "OK", 200) != E_OK) {
        d_printf("FTPCID failed\r\n");
        return E_FAIL;
    }
	
	d_printf("gprs_stage = %d\r\n", gprs_stage);
    if (send_at_command("AT+FTPMODE=0", "OK", 200) != E_OK) {
        d_printf("FTPMODE failed\r\n");
        return E_FAIL;
    }
	
	d_printf("gprs_stage = %d\r\n", gprs_stage);
    if (send_at_command("AT+FTPTYPE=\"I\"", "OK", 200) != E_OK) {
        d_printf("FTPTYPE failed\r\n");
        return E_FAIL;
    }
	
	d_printf("gprs_stage = %d\r\n", gprs_stage);
    memset(buf, 0, sizeof(buf));
    sprintf(buf, "AT+FTPSERV=\"%s\"", UPDATE_IP);
    if (send_at_command(buf, "OK", 200) != E_OK) {
        d_printf("FTPSERV failed\r\n");
        return E_FAIL;
    }
	
	d_printf("gprs_stage = %d\r\n", gprs_stage);
    memset(buf, 0, sizeof(buf));
    sprintf(buf, "AT+FTPUN=\"%s\"", SERVER_USER);
    if (send_at_command(buf, "OK", 200) != E_OK) {
        d_printf("FTPUN failed\r\n");
        return E_FAIL;
    }
	
	d_printf("gprs_stage = %d\r\n", gprs_stage);
    memset(buf, 0, sizeof(buf));
    sprintf(buf, "AT+FTPPW=\"%s\"", SERVER_PASS);
    if (send_at_command(buf, "OK", 200) != E_OK) {
        d_printf("FTPPW failed\r\n");
        return E_FAIL;
    }
	
	d_printf("gprs_stage = %d\r\n", gprs_stage);
    memset(buf, 0, sizeof(buf));
    sprintf(buf, "AT+FTPGETNAME=\"%s\"", fname);
    if (send_at_command(buf, "OK", 200) != E_OK) {
        d_printf("FTPGETNAME failed\r\n");
        return E_FAIL;
    }
	
	d_printf("gprs_stage = %d\r\n", gprs_stage);
    memset(buf, 0, sizeof(buf));
    sprintf(buf, "AT+FTPGETPATH=\"%s\"", path);
    if (send_at_command(buf, "OK", 200) != E_OK) {
        d_printf("FTPGETPATH failed\r\n");
        return E_FAIL;
    }

	d_printf("gprs_stage = %d\r\n", gprs_stage);
    if (send_at_command("AT+FTPGET=1", "OK", 200) != E_OK) {
        d_printf("FTPGET failed\r\n");
        return E_FAIL;
    }

	d_printf("gprs_stage = %d\r\n", gprs_stage);
    // Prepare to get real data
    packet = 0;
    code = 0;
    gprs_delay_msec(DEFAULT_DELAY_MS * 2);
	g_info.checksum = 0;
	d_printf("add\r\n");
    while (TRUE) {        // wait for URC message
        timeout = DELAY_FTPGET / 100 + 1;
        while (timeout != 0) {
            // FTPGET:1,1 means data available, 1,0 means no data
			ptr = strstr(GPRS_RxBuffer, "+FTPGET: 1,");
            if (ptr != NULL) {
                // wait extra time for remain data
                gprs_delay_msec(DEFAULT_DELAY_MS * 4);
                dump_hex(GPRS_RxBuffer, 64);
                code = my_atoi(ptr + strlen("+FTPGET: 1,"));
                break;
            }
            if (ptr2 != NULL) {
                ptr = strstr(ptr2 +  len, "+FTPGET: 1,");
                if (ptr != NULL) {
                    // wait extra time for remain data
                    gprs_delay_msec(DEFAULT_DELAY_MS * 4);
                    dump_hex(GPRS_RxBuffer, 1100);
                    code = my_atoi(ptr + strlen("+FTPGET: 1,"));
                    break;
                }
           }
            //d_printf("[%s]\r\n", GPRS_RxBuffer);
			gprs_delay_msec(100);
            timeout --;
        }
        if (timeout == 0) {
            d_printf("Command FTPGET timeout\r\n");
            return E_TIMEOUT;
        }
        if (code == 0) {
            *size_read = file_size;
            d_printf("No data can be download by FTP\r\n");
            return E_OK;
        } else if (code > 60) {
            d_printf("Error when downloading by FTP, code=%d\r\n", code);
            // TODO: these failures include file not found, etc., so return fail
            // may be not the best choice
            return E_FAIL;
        } else {
            // It's good
        }
        memset_rx_buf();   
        send_string("AT+FTPGET=2,1024\r\n");
        gprs_delay_msec(DEFAULT_DELAY_MS*2);  // delay for safe
        len = 0;
        code = 0;
        timeout = DELAY_FTPGET / 100 + 1;
        while (timeout != 0) {
			ptr = strstr(GPRS_RxBuffer, "+FTPGET: 2,");
            if (ptr != NULL) {
                // wait extra time for remain data
                gprs_delay_msec(DEFAULT_DELAY_MS * 2);
                len = my_atoi(ptr + strlen("+FTPGET: 2,"));
                d_printf("ftp.len=%d\r\n", len);
                break;
            }
            d_printf("ftp.ptr2\r\n");
            ptr2 = strstr(GPRS_RxBuffer, "+FTPGET: 1,");
            if (ptr2 != NULL) {
                code = my_atoi(ptr2 + strlen("+FTPGET: 1,"));
                if (code > 60) {
                    d_printf("Error when FTP downloading, code=%d\r\n", code);
                    // Should retry if fails at the middle of downloading
                    return E_RETRY;
                }
            }
            gprs_delay_msec(100);
            timeout --;
        }
        
		if (ptr == NULL) {
            d_printf("FTP get data timeout\r\n");
            break;
        }
        if (len == 0) {
            d_printf("Get FTP data done, total=%d bytes.\r\n", file_size);
            break;
        }
        ptr2 = strstr(ptr, "\r\n");
        ptr2 += 2;  // CRLF
        packet++;
        file_size += len;
        d_printf("Get FTP data %d bytes, packet number %d, file_zise %d\r\n", len, packet, file_size);
        gprs_get_data_callback(ptr2, len); 
		printf("chkek:%x\r\n", g_info.checksum);
//        memset_rx_buf();
    }

    check_and_clear_rxbuffer();
    *size_read = file_size;
    return E_OK;
}
// CSQ返回值为 0-32
// 设置4级信号强度 20以上为5格信号，20以下，0-4：1格信号
// 5-9：2格信号  10-14：3格信号   15-19：4格信号
int gprs_get_gprs_signal(void) {
	int  t = 0, timeout = 1000;
	char *buf;
	int ret = 0;
	
	memset_rx_buf();
	send_string("AT+CSQ\r\n");
    gprs_delay_msec(DEFAULT_DELAY_MS);
    t = timeout / 100 + 1;
    while (t != 0) {
        buf = read_response(GPRS_RxBuffer);
        if (buf != NULL)
            break;
        gprs_delay_msec(100);
        t--;
    }
    if (buf == NULL) {
        d_printf("get gprs signal error.\r\n");
        return 0;
    }
    if (strstr(buf, "CSQ") != NULL) {
		buf = buf + strlen("CSQ: ");
		ret = atoi(buf);
    }
	
	printf("CSQ:%s\r\n", GPRS_RxBuffer);
	return ret / 5;
}

// GPRS Service Cycle.
void gprs_service(void) {
    static uint16_t gprs_reg_count = 0;
	
	if (gprs_stage < STAGE_poweron) {
        if (!gprs_poweron()) {
			d_printf("Failed to power on GPRS module\r\n");
            return;
        } else {
			d_printf("GPRS module is powered on\r\n");
        }
        gprs_reg_count = 0;
    }
	if (gprs_stage == STAGE_poweron) {
        gprs_delay_msec(DEFAULT_DELAY_MS);
		gprs_reg_count++;
		if (gprs_reg_count > REG_TIMEOUT / DEFAULT_DELAY_MS) {
			gprs_stage = STAGE_checkreg;
			d_printf("[checkreg]  gprs_stage=%d\r\n", gprs_stage);
			gprs_reg_count = 0;
		}
    }
    if (gprs_stage == STAGE_shutdown) {
        gprs_shutdown();
    }
	
    if (gprs_stage == STAGE_checkreg) {
        if (!gprs_check_registeration()) {
            d_printf("GPRS module not work\r\n");
        }
        gprs_delay_msec(DEFAULT_DELAY_MS);
        gprs_reg_count++;
        if (gprs_reg_count >= (REG_TIMEOUT / DEFAULT_DELAY_MS)) {
            gprs_reg_count = 0;
            gprs_stage = STAGE_error;
            d_printf("Failed to register network in %d seconds\r\n",
                   REG_TIMEOUT * 2 / 1000);
            return;
        }
    }
	if (gprs_stage == STAGE_idle) {
        // the flag is set by heatbeating response
        if (new_app_flag){
			u32 filesize1;
            int ret;
            PartitionNum par;

            new_app_flag = FALSE;
            gprs_update_ready = FALSE;
			d_printf("***Update Project......\r\n");
            par = gprs_update_init();

            if (gprs_get_data_callback == NULL) {
                d_printf("FTP read callback is not set yet\r\n");
            }

            ret = ftp_read_file(ftp_path, ftp_fname, &filesize1);
			if (ret == E_BUSY) {
                // GPRS module may be reset and not ready yet
                return;
            }
            if (ret == E_RETRY || ret == E_TIMEOUT) {
                // failure occurs when downloading
                shutdown_pdp();
                return;
            }
            if (ret != E_OK) {
                // other failures for which GPRS module should be reset
                gprs_poweron();
                return;
            }
            d_printf("*****get file size = %d\r\n", filesize1);

            gprs_update_done();
            if (filesize1 == 0){
                return;
            }
			 d_printf("ftp download ok!.\r\n");
            // Maybe timeout or error, but useless after timeout
            if (filesize1 == ftp_length) {
                gprs_update_ready = TRUE;
				d_printf("FTP downloading is done successfully.\r\n");
			} else {
                d_printf("FTP downloading failed, length should be %d but get %d.\r\n",
                    ftp_length, filesize1);
				d_printf("Downloaded data will be deleted.\r\n");
                delete_partition(par);
                d_printf("GPPRS module will be restart\r\n");  
                gprs_poweron();
                return ;
            }
        }
		LOS_TaskDelay(100);
	}
}


int shutdown_pdp(void) {
    if (send_at_command("AT+CIPSHUT", "OK", 5000) != E_OK) {
        d_printf("Failed to shutdown GPRS.\r\n");
        return E_FAIL;
    } else {
        d_printf("Successfully shutdown GPRS.\r\n");
        gprs_stage = STAGE_idle;
    }

    // Check after default stage setting, in case connection is closed unexpected
    check_and_clear_rxbuffer();
    return E_OK;
}
int write_data(char* data, u16 len, bool send_ready) {
    char buf[32];
    char* ptr1;
    char* ptr2;
    u16 timeout;
	s16 error_code = 0;
	char get_json[128] = {0};

    if (gprs_stage < STAGE_ready) {
		if (send_ready) {
			if (gprs_connect(SERVER_IP, SERVER_PORT) != E_OK) {
				d_printf("Cannot connect to %s:%s\r\n", SERVER_IP, SERVER_PORT);
				return E_FAIL;
			}
		} else {
			if (gprs_connect(UPDATE_IP, UPDATE_PORT) != E_OK) {
				d_printf("Cannot connect to %s:%s\r\n", UPDATE_IP, UPDATE_PORT);
				return E_FAIL;
			}

		}
    }

    check_and_clear_rxbuffer();
    
    // Send command
    memset(buf, 0, sizeof(buf));
    sprintf(buf, "AT+CIPSEND=%d\r\n", len);
	printf("buf:%s\r\n", buf);
    send_string(buf);
    gprs_delay_msec(DEFAULT_DELAY_MS);
	printf("buf:%s\r\n", buf);

    timeout = DELAY_CIPSTART / 100 + 1;
    while (timeout != 0) {
        if (strstr(GPRS_RxBuffer, ">") != NULL) {
            gprs_delay_msec(DEFAULT_DELAY_MS);  // delay safe time before sending
            break;
        }
        gprs_delay_msec(100);
        timeout --;
    }
	printf("%s\r\n", GPRS_RxBuffer);
    if (timeout == 0) {
        d_printf("Wait CIPSEND prompt timeouted\r\n");
        return E_TIMEOUT;
    }
	check_and_clear_rxbuffer();
    // Send data
    memset_rx_buf();
    send_binary(data, len);
    send_binary(CtrlZ, 3);
	
    timeout = DELAY_CIPSTART / 100 + 1;
    while (timeout != 0) {
        // This message will override the previous "OK"!!!
        if (strstr(GPRS_RxBuffer, "+CIPRXGET: 1") != NULL) {
            d_printf("-------------------------\r\n");
            d_printf("%s\r\n", GPRS_RxBuffer);
            d_printf("-------------------------\r\n");
			if (send_ready) {
				//error_code = send_at_command("AT+CIPRXGET=2,128", "YES", 2000);
				error_code = send_at_command("AT+CIPRXGET=2,1024", "application", 2000);
				if (error_code == E_OK) {
					printf("rx[%s]\r\n", GPRS_RxBuffer);
					SendOK = 1;
			        ptr1 = strstr(GPRS_RxBuffer, "Date:");
                    ptr2 = strstr(ptr1, "GMT");
					if ((ptr1 != NULL) && (ptr2 != NULL)) {
						*ptr2 = '\0';
						ptr1 = strstr(ptr2 + 3, "{");
						ptr2 = strstr(ptr1, "}");
						if ((ptr1 != NULL) && (ptr2 != NULL)) {
							strncpy(get_json, ptr1, (ptr2 - ptr1 + 1));
							parseJson(&retJson, get_json);
						}
					}
				} else {
					d_printf("error code:%x\r\n", error_code);
					d_printf("%s\r\n", GPRS_RxBuffer);
				} 
			}
            break;
        }
        
        gprs_delay_msec(100);
        timeout --;
    }
	
	check_and_clear_rxbuffer();
    if (timeout == 0) {
        d_printf("Writing CIPSEND data timeouted\r\n");
        return E_TIMEOUT;
    }

    return E_OK;
}
int read_data(char* data, u16 len, u16 timeout) {
    char buf[32];
    u16 t;
    char *ptr = NULL;
    char *ptr2 = NULL;
    u16 bytes = 0;

    if (gprs_stage < STAGE_ready) {
        if (gprs_connect(UPDATE_IP, UPDATE_PORT) != E_OK) {
            d_printf("Cannot connect to %s:%s\r\n", UPDATE_IP, UPDATE_PORT);
            return E_FAIL;
        }
    }else{
		d_printf("gprs_stage == STAGE_ready\r\n");
	}

    t = timeout / 100 + 1;
    while (t != 0) {
        check_and_clear_rxbuffer();
        if (new_response_flag)
            break;
        gprs_delay_msec(100);
        t --;
    }
    if (!new_response_flag) {
        d_printf("No new data are ready\r\n");
        return E_FAIL;
    }
    new_response_flag = FALSE;

    // Send query command
    memset(buf, 0, sizeof(buf));
    sprintf(buf, "AT+CIPRXGET=2,%d\r\n", len);
    send_string(buf);
    gprs_delay_msec(DEFAULT_DELAY_MS);

    t = timeout / 100 + 1;
    while (t != 0) {
        d_printf("[%s]%s\r\n", __func__, GPRS_RxBuffer);
        ptr = strstr(GPRS_RxBuffer, "+CIPRXGET: 2,");
        if (ptr != NULL) {
            // wait extra time for remain data
            gprs_delay_msec(DEFAULT_DELAY_MS);
            bytes = my_atoi(ptr + strlen("+CIPRXGET: 2,"));
            break;
        }
        gprs_delay_msec(100);
        t --;
    }
    if (ptr == NULL) {
        d_printf("Command CIPRXGET timeouted\r\n");
        return E_TIMEOUT;
    }
    if (bytes == 0) {
        d_printf("No data is read\r\n");
        return E_OK;
    }

    // Receiving data
    t = timeout / 100 + 1;
    while (t != 0) {
        if (strstr(GPRS_RxBuffer, "OK\r\n") != NULL) {
            break;
        }
        gprs_delay_msec(100);
        t --;
    }
    if (t == 0) {
        d_printf("Reading CIPRXGET data timeouted\r\n");
        return E_TIMEOUT;
    }

    ptr2 = strstr(ptr, "\r\n");
    if (ptr2 == NULL) {
        d_printf("Reading data format error: [%s]\r\n", ptr2);
        return E_FAIL;
    }
    memcpy(data, ptr2 + 2, bytes);
    memset_rx_buf();
    d_printf("Read %d bytes\r\n", bytes);

    return bytes;
}
// Used by main thread to keep gprs alive, should be called every 120s or so.
// TODO: this command can quiry new firmware or other regular info
bool heart_beat(bool send_ready) {
    int len;

    if (hbstr[0] == 0) {
        update_hbreq();
    }

    if (write_data(hbstr, strlen(hbstr), send_ready) == E_OK) {
        memset(hbresp, 0, sizeof(hbresp));

        len = read_data(hbresp, sizeof(hbresp), DELAY_CIPRXGET);
        if (len > 0) {
            d_printf("Heat beat works! Got [%s]\r\n", hbresp);
            return TRUE;
        } else {
            d_printf("Heat beat fails!\r\n");
        }
    } else {
        d_printf("Sending heat beat message fails, server may be down!\r\n");
    }

    return FALSE;
}

int heart_com(char* data_send, bool data_ready) {
    int date_send_len = 0;
	
    if (gprs_stage >= STAGE_idle) {
        if (new_app_flag) {
            d_printf("Skip heat beat for there are FTP data\r\n");
            return E_BUSY;
        }

        // Polling by heatbeating
		if (data_ready) {
			if (gprs_connect(SERVER_IP, SERVER_PORT) != E_OK) {
				d_printf("no connect\r\n");
				if (gprs_tcpclose() != E_OK) {
					if (shutdown_pdp() != E_OK) {
						gprs_stage = STAGE_poweroff;
						return E_BUSY;
					}
				}
			}
		} else {
			if (gprs_connect(UPDATE_IP, UPDATE_PORT) != E_OK) {
				d_printf("no connect\r\n");
				if (gprs_tcpclose() != E_OK) {
					if (shutdown_pdp() != E_OK) {
						gprs_stage = STAGE_poweroff;
						return E_BUSY;
					}
				}
			}

		}
        if (gprs_stage == STAGE_ready) {
            if(data_ready){
                date_send_len = strlen(data_send);
                if (write_data(data_send, date_send_len, data_ready) == E_OK) {
                    memset(data_send, 0, date_send_len);
                    gprs_tcpclose();
                    return E_OK;
                } else {
                    d_printf("send data faild by will\r\n");
                }
			}else{
                if (heart_beat(data_ready)) {
                    memset(ftp_path, 0, sizeof(ftp_path));
                    memset(ftp_fname, 0, sizeof(ftp_fname));
                    memset(ftp_ver, 0, sizeof(ftp_ver));
                    ftp_length = get_file_info(hbresp, ftp_path, ftp_fname, ftp_ver, 64);
                    if (ftp_length > 0) {
                        d_printf("New application %s%s is ready, version=%s, len=%d.\r\n",
                        ftp_path, ftp_fname, ftp_ver, ftp_length);
                        g_version_bak = my_str2hex(ftp_ver) & 0xFFFF;
                        new_app_flag = TRUE;
                    }
                    memset(hbresp, 0, sizeof(hbresp));
                    gprs_tcpclose();
                    return E_OK;
                } else {
                    d_printf("Failed to get heart beat response\r\n");
                }
            }
        }
        check_and_clear_rxbuffer();
        // FIXME: ftp get needs disconnect TCP connection?
        if (gprs_stage == STAGE_ready) {
            gprs_tcpclose();
        }
    }
    return E_FAIL;
}
bool gprs_get_time(u32 *timestamp) {
    char *ptr;
    struct tm timer;
    u32 time1;
    u16 val;

	memset_rx_buf();
    if (send_at_command("AT+CCLK?", "OK", 500) != E_OK) {
        d_printf("CCLK inquery failed\r\n");
        return FALSE;
    }
    ptr = strstr(GPRS_RxBuffer, "+CCLK: \"");
    if (ptr == NULL) {
        d_printf("Getting time by CCLK error:%s\r\n", GPRS_RxBuffer);
        return FALSE;
    }
    ptr += strlen("+CCLK: \"");
    val = my_atoi(ptr);
    if (val < 16) {
        // older than year 2015/12/31
        d_printf("Warning: GPRS RTC is not initialized\r\n");
        return FALSE;
    }
    timer.tm_year = val + 100;
    ptr += 3;
    val = my_atoi(ptr);
    timer.tm_mon = val - 1;
    ptr += 3;
    val = my_atoi(ptr);
    timer.tm_mday = val;
    ptr += 3;
    val = my_atoi(ptr);
    timer.tm_hour = val;
    ptr += 3;
    val = my_atoi(ptr);
    timer.tm_min = val;
    ptr += 3;
    val = my_atoi(ptr);
    timer.tm_sec = val;

    timer.tm_isdst = 0;
	
	time1 = mktime(&timer);
    
    *timestamp = time1;
    return TRUE;
}

int gprs_sync_time(void) {
    char buf[64];
    char message[64];
    u16 timeout;
    char* ptr;

    if (gprs_stage >= STAGE_ready) {
		gprs_tcpclose();    
    }
	if (send_at_command("AT+CIPSHUT", "OK", 2000) != E_OK)
        return FALSE;
	if (send_at_command("AT+CSQ", "CSQ:", 500) != E_OK)
        return E_FAIL;
    if (send_at_command("AT+CSTT", "OK", 2000) != E_OK)
        return E_FAIL;
    if (send_at_command("AT+CIICR", "OK", 10000) != E_OK)
        return E_FAIL;
    if (send_at_command("AT+CIFSR", ".", 2000) != E_OK)
        return E_FAIL;

	memset_rx_buf();
    // Connect to NTP server to get the current time
    memset(buf, 0, sizeof(buf));
    sprintf(buf, "AT+CIPSTART=\"TCP\",\"%s\",%s\r\n", g_ntp_ip, g_ntp_port);
    send_string(buf);
    gprs_delay_msec(DEFAULT_DELAY_MS);
	d_printf("%s\r\n", buf);
    timeout = NTP_TIMEOUT / 100 + 1;
    while (timeout != 0) {
        if (strstr(GPRS_RxBuffer, "CONNECT OK") != NULL) {
            break;
        }
        gprs_delay_msec(110);
        timeout --;
    }
//	d_printf("rx[%s]\r\n", GPRS_RxBuffer);
    if (timeout == 0) {
		d_printf("NTP server %s, %s\r\n", g_ntp_ip, g_ntp_port);
        d_printf("Connect NTP server timeouted\r\n");
        return E_TIMEOUT;
    }
	memset_rx_buf();
    // NTP server will reply current time in seconds
    timeout = NTP_TIMEOUT / 100 + 1;
    while (timeout != 0) {
        // FIXME: the search pattern is not verified
        if (strstr(GPRS_RxBuffer, "UTC(NIST) *") != NULL) {
            gprs_delay_msec(DEFAULT_DELAY_MS);
            break;
        }
        gprs_delay_msec(100);
        timeout --;
    }
//	d_printf("rx[%s]\r\n", GPRS_RxBuffer);
    if (timeout == 0) {
        d_printf("Wait timing data timeouted\r\n");
        return E_TIMEOUT;
    }

    // Copy to temporary buffer to avoid be overrided
    memset(message, 0, sizeof(message));
    memcpy(message, GPRS_RxBuffer, sizeof(message));
	d_printf("Timing message: %s\r\n", message);

    // format ex:
    // 57748 16-12-26 11:25:10 00 1 0 550.1 UTC(NIST) * 
    ptr = strstr(message, "-");
    if (ptr == NULL) {
        d_printf("Reading data format error: [%s]\r\n", message);
        return E_FAIL;
    }

    memset(buf, 0, sizeof(buf));
    sprintf(buf, "AT+CCLK=\"%c%c/%c%c/%c%c,%c%c:%c%c:%c%c+08\"\r\n",
            *(ptr-2), *(ptr-1), *(ptr+1), *(ptr+2), *(ptr+4), *(ptr+5),
            *(ptr+7), *(ptr+8), *(ptr+10), *(ptr+11), *(ptr+13), *(ptr+14));

    if (send_at_command(buf, "OK", 500) != E_OK) {
        d_printf("Setting CCLK failed\r\n");
        return E_FAIL;
    }

    d_printf("Setting RTC time by [%s] done.\r\n", buf);
//    send_at_command("AT+CIPCLOSE", "CLOSE OK", 5000);
//    send_at_command("AT+CIPSHUT", "SHUT OK", 5000);
    check_and_clear_rxbuffer();

    return E_OK;
}
int Init_realtime(void){
    // update time
    if (!gprs_get_time(&g_timestamp)) {
        d_printf("Cannot uploading data for failing to get time stamp.\r\n");
        return E_NOTIME;
    }
    // over 24 hours passed since last syncing
    if (g_timestamp - timestamp_synced > 86400) {
        gprs_sync_time();
        if (!gprs_get_time(&g_timestamp)) {
            d_printf("Failed to get time stamp after syncing.\r\n");
            return E_NOTIME;
        }
        timestamp_synced = g_timestamp;
    }

	return E_OK;
}
bool Gprs_GetInfo(void) {
	char *buf = NULL;
    u16 t,i,len;
	char *tel_num;
	u16 timeout = 4000;

	memset_rx_buf();
	printf("**************************\r\n");
    send_string("AT+CNUM\r\n");
    
    t = timeout / 100 + 1;
	while (t != 0) {
        buf = strstr(GPRS_RxBuffer, "+CNUM");
		 if (buf != NULL)
            break;
        gprs_delay_msec(100);
        t--;
    }

    if (buf == NULL) {
        printf("Command AT+CNUM timeout.\r\n");
    }
	printf("num:%s\r\n",buf);
	tel_num = strstr(buf,"1");
	if(tel_num[0] != '1'){
		return FALSE;
	}
    for(i = 0;tel_num[i] != '\"';i++);
    tel_num[i] = '\0';
    len = i;
    
    strcpy(g_board_info[0].sim, tel_num);
	printf("sim card num:%s\r\n", g_board_info[0].sim);
	printf("**************************\r\n");
    
    return TRUE;

}
void init_client_xindi(void) {
    u8 len;

    // Handle the length to avoid pointer overlapping
    len = strlen(NTP_IP);
    if (len > sizeof(g_ntp_ip)) {
        d_printf("Too long NTP IP: %s, will be cut.\r\n", NTP_IP);
        len = sizeof(g_ntp_ip);
    }
    memcpy(g_ntp_ip, NTP_IP, len);

    len = strlen(NTP_PORT);
    if (len > sizeof(g_ntp_port)) {
        d_printf("Too long NTP PORT: %s, will be cut.\r\n", NTP_PORT);
        len = sizeof(g_ntp_port);
    }
    memcpy(g_ntp_port, NTP_PORT, len);

    // Initialize GPRS RTC
    if (gprs_sync_time() == E_OK) {
        // Record sync time in case we need sync again later
        if(gprs_get_time(&timestamp_synced)) {
            d_printf("GPRS RTC is initialized as %ul.\r\n", timestamp_synced);
        } else {
            d_printf("GPRS RTC does not work.\r\n");
        }
    } else {
        d_printf("Failed to sync from NTP (%s:%s).\r\n", g_ntp_ip, g_ntp_port);
    }
	
	shutdown_pdp();
}
extern char* itoa(uint32_t num);
extern char* ftoa(float num);
extern uint16_t BatteryValue;
extern AD_Config AD_cfg[];
void data_init(cJSON *data) {
	int itemInt;
	itemInt = g_board_info[0].id;
    cJSON_AddItemToObject(data, "devid", cJSON_CreateString(itoa(itemInt)));
    cJSON_AddItemToObject(data, "height1", cJSON_CreateString(itoa(g_board_info[0].level)));
    cJSON_AddItemToObject(data, "weight1", cJSON_CreateString(itoa(g_board_info[0].weight)));
    cJSON_AddItemToObject(data, "diff_pressure1", cJSON_CreateString(itoa(debugInfo[0].dp_val)));
	cJSON_AddItemToObject(data, "pressure1", cJSON_CreateString(itoa(debugInfo[0].p_val)));
	
    cJSON_AddItemToObject(data, "per1", cJSON_CreateString(ftoa((float)g_board_info[0].per / 10)));
	cJSON_AddItemToObject(data, "voltage1", cJSON_CreateString(itoa(BatteryValue)));
    cJSON_AddItemToObject(data, "valve1", cJSON_CreateString(itoa(0)));
    cJSON_AddItemToObject(data, "flow1", cJSON_CreateString(itoa(0)));
	
	cJSON_AddItemToObject(data, "warn", cJSON_CreateString(itoa(g_board_info[0].alarm_Bits)));
	cJSON_AddNumberToObject(data, "time", g_timestamp);
}
void realdata_init(char *out) {
	cJSON *root;
    char *p;
	char str[20];
	cJSON *JSONdata;
	
    root = cJSON_CreateObject();
	JSONdata = cJSON_CreateObject();
 
    cJSON_AddItemToObject(root, "type", cJSON_CreateString("realdata"));
    cJSON_AddItemToObject(root, "ver", cJSON_CreateString(httpHeader.ver));
    cJSON_AddItemToObject(root, "cpuid", cJSON_CreateString(httpHeader.cpuid));
    sprintf(str, "%d", g_timestamp);
    cJSON_AddItemToObject(root, "time", cJSON_CreateString(str));
	
	data_init(JSONdata);
	
	cJSON_AddItemToObject(root, "data", JSONdata);
    
	p =  cJSON_Print(root);
    
	strcpy(out, p);
    
	myfree(p);
    cJSON_Delete(root); 
    printf("***************\r\n");
    printf("%s\r\n",out); 
    printf("mem:%d\r\n", mem_perused());
    if (mem_perused() > 70){
        mem_init();
    }
    printf("***************\r\n");
}
void login_init(char* out) {
    cJSON *root;
    char *p;
	char str[20];
	
    root = cJSON_CreateObject();
 
    cJSON_AddItemToObject(root, "type", cJSON_CreateString("login"));
    cJSON_AddItemToObject(root, "ver", cJSON_CreateString(httpHeader.ver));
    cJSON_AddItemToObject(root, "cpuid", cJSON_CreateString(httpHeader.cpuid));
	sprintf(str, "%d", g_timestamp);
    cJSON_AddItemToObject(root, "time", cJSON_CreateString(str));
    p =  cJSON_Print(root);
    strcpy(out, p);
    myfree(p);
    cJSON_Delete(root); 
    printf("***************\r\n");
    printf("%s\r\n",out); 
    if (mem_perused() > 70){
        mem_init();
    }
    printf("***************\r\n");
}
void httpFillData(char * path, char * data, char *out) {
	char GprsData[600];
	
	sprintf(GprsData, 
	"POST /%s HTTP/1.1\r\nHost: monitor.sqd.zys.me\r\nContent-Type: application/json\r\nContent-Length: %d\r\n\r\n%s", path, strlen(data), data);
	printf("%s\r\n", GprsData);
	
	strcpy(out, GprsData);
}
void httpHeaderInit(void) {
	memset(&httpHeader, 0, sizeof(httpHeader));
	strcpy(httpHeader.ver, "2.0");
	sprintf(httpHeader.cpuid, "%08X%08X%08X", CpuID[0], CpuID[1], CpuID[2]);
}
char GPRS_send_data(void) {
    int ret;
    int counter = 0;
	char GprsSendData[650];
	//char tt[900];
	static uint32_t send_timestamp = 0;
	
	printf("sqd flag:%d\r\n", sqd_send_flag);
	memset(GprsSendData, 0, sizeof(GprsSendData));
	
	if ((g_timestamp - send_timestamp > 3600) || (httpHeader.status < HTTP_Login)) {
		send_timestamp = g_timestamp;
		login_init(GprsSendData);
		httpFillData("login", GprsSendData, GprsSendData);
			
		counter = 0;
		while(counter < 4) {
			 ret = write_data(GprsSendData, strlen(GprsSendData), true);
			 //ret = heart_com(GprsSendData, TRUE);
			 shutdown_pdp();
			if (ret == E_OK) {
				break;
			} else if (ret == E_BUSY) {
				return ret;
			}
			counter ++;
			d_printf("tcp send again\r\n");
		}
		
		if (counter >= 4) {
			// There is fatal error, so we have to reset GPRS module
			d_printf("Reseting GPRS module...\r\n");
			gprs_poweron();
			// No need do heatbeating before GPRS is ready
			return E_TIMEOUT;
		}
		// 登录成功
		switch (httpHeader.code) {
			case LOGIN_ERROR:
				printf("http login error msg:%s\r\n", httpHeader.msg);
			break;
			
			case LOGIN_OK:
				if (strcmp(httpHeader.type, "loginack") == 0) {
					httpHeader.status = HTTP_Login;
					printf("login success\r\n");
				} else {
					printf("login error\r\nmsg:%s\r\ntype:%s\r\n", httpHeader.msg, httpHeader.type);
				}
			break;
			
			case LOGIN_SESSION:
					httpHeader.status = HTTP_Login;
				printf("has been a active session\r\n");
			break;
			
			case LOGIN_TIMEOUT:
				printf("login timeout\r\n");
			break;
		}
	}
	realdata_init(GprsSendData);
	httpFillData("data", GprsSendData, GprsSendData);
	
	counter = 0;
	while(counter < 4) {
		 ret = write_data(GprsSendData, strlen(GprsSendData), true);
		 //ret = heart_com(GprsSendData, TRUE); 
		 shutdown_pdp();
		if (ret == E_OK) {
			break;
		} else if (ret == E_BUSY) {
			return ret;
		}
		counter ++;
		d_printf("tcp send again\r\n");
	}		
	
	if (httpHeader.code == 1) {
		printf("send data error msg:%s\r\n", httpHeader.msg);
		httpHeader.status = HTTP_LoginOut;
	}
	if (counter >= 4) {
		// There is fatal error, so we have to reset GPRS module
		d_printf("Reseting GPRS module...\r\n");
		gprs_poweron();
		// No need do heatbeating before GPRS is ready
		return E_TIMEOUT;
	}
	
	counter = 0 ;
	while(counter < 3){
		ret = heart_com(GprsSendData, FALSE);
		shutdown_pdp();
		
		// Only retry when there is writing failure
		if (ret == E_OK || ret == E_BUSY) {
			break;
		}
		counter++;
		d_printf("heart again\r\n");
	}
	
	if (counter >= 4) {
		// There is fatal error, so we have to reset GPRS module
		d_printf("Reseting GPRS module...\r\n");
		gprs_poweron();
		// No need do heatbeating before GPRS is ready
		return E_TIMEOUT;
	}
	
	
	memset(GprsSendData, 0, sizeof(GprsSendData));
	
	return E_OK;
}

extern uint32_t gprs_count;
extern unsigned int send_flag;
void GPRS_Task(void) {
	int res = 0;
	int getTimeCount = 0;
	
	get_cpuid_code();
	gprs_init();
	
	gprs_service();
	LOS_TaskDelay(4000);
	init_client_xindi();
	
	httpHeaderInit();
	Gprs_GetInfo();
	
	// 开始任务
    while (1) {
		res = Init_realtime();
		LOS_TaskDelay(100);
		if (!res) {
			send_flag &= ~SEND_FLAG_STATUS;
		
			gprs_service();		
			
			gprs_signal = gprs_get_gprs_signal();
			// Before the board is reset (after the new application is downloaded),
			// we don't need send the heartbeat message.
			if (is_gprs_update_ready()) {
				d_printf("Skip heatbeating for new app is ready for reboot.\r\n");
				IWDG_Init_3S();
				while(1);
			}
			
			if (gprs_stage >= STAGE_checkreg) {
				if (send_flag == 0) {
					printf("send data\r\n");
					res = GPRS_send_data();
					
					sqd_send_flag = 0;
					sqd_gprs_count = 0;
					gprs_count = 0;
				}
			}
			memset_rx_buf();
		} else {
			d_printf("read time init falie\r\n");
			send_flag |= SEND_FLAG_STATUS;
			gprs_sync_time();
			shutdown_pdp();
			getTimeCount++;
			if (getTimeCount >= 3) {
				getTimeCount = 0;
				gprs_reset();
			}
		}
	}
}
