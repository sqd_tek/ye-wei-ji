#ifndef __HAL_KEY_H__
#define __HAL_KEY_H__
#include "los_typedef.h"
#include "stm32f10x.h"
#include "los_event.h"

extern EVENT_CB_S key_event;
#define event_key  ((uint32_t)(1 << 0))
#define event_key1 ((uint32_t)(1 << 1))
#define event_key2 ((uint32_t)(1 << 2))
#define event_key3 ((uint32_t)(1 << 3))
#define event_key4 ((uint32_t)(1 << 4))
#define event_long1 ((uint32_t)(1 << 16))
#define event_long2 ((uint32_t)(1 << 17))
#define event_long3 ((uint32_t)(1 << 18))
#define event_long4 ((uint32_t)(1 << 19))

enum KEY {
    nokey = 0,
    key1short = 1<<0,
    key2short = 1<<1,
    key3short = 1<<2,
    key4short = 1<<3,
    key1long = 1<<8,
    key2long = 1<<9,
    key3long = 1<<10,
    key4long = 1<<11,
};

bool key_init(void);
bool key_exit(void);
enum KEY key_get_status(void);

#endif
