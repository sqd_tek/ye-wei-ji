#include "hal_key.h"
#include "gpio.h"
#include "stdio.h"
#include "los_hwi.h"
#include "Timer.h"
#include "los_task.h"

#define KEY1PORT GPIOB
#define KEY2PORT GPIOC
#define KEY3PORT GPIOC
#define KEY4PORT GPIOB
#define KEY1PIN  GPIO_Pin_0
#define KEY2PIN  GPIO_Pin_4
#define KEY3PIN  GPIO_Pin_5
#define KEY4PIN  GPIO_Pin_1


enum KEY keystatus;
int K_timer;
EVENT_CB_S  key_event;

void NVIC_Cfg(void);
void EXTI0_IRQHandler(void);
void EXTI1_IRQHandler(void);
void EXTI4_IRQHandler(void);
void EXTI9_5_IRQHandler(void);

bool key_event_init(void)
{
    UINT32 uwRet;
    
    uwRet = LOS_EventInit(&key_event);
    if(uwRet != LOS_OK) {
        printf("init key_event failed .\n");
        return 1;
    }
    
    return LOS_OK;
}
bool key_init(void)
{
    NVIC_InitTypeDef NVIC_InitStructure;
    UINTPTR uvIntSave;
    
    if (key_event_init() != LOS_OK) {
        printf("key_event_init failed .\n");
        return 1;
    }

    RCC_APB2PeriphClockCmd(RCC_APB2Periph_AFIO,ENABLE);
    
    GPIOX_Init(KEY1PORT, KEY1PIN, GPIO_Mode_IPU, true);
    GPIOX_Init(KEY2PORT, KEY2PIN, GPIO_Mode_IPU, true);
    GPIOX_Init(KEY3PORT, KEY3PIN, GPIO_Mode_IPU, true);
    GPIOX_Init(KEY4PORT, KEY4PIN, GPIO_Mode_IPU, true);
    NVIC_Cfg();
    uvIntSave = LOS_IntLock();
    LOS_HwiCreate(EXTI0_IRQn, 0, 0, EXTI0_IRQHandler, 0);
    LOS_HwiCreate(EXTI1_IRQn, 0, 0, EXTI1_IRQHandler, 0);
    LOS_HwiCreate(EXTI4_IRQn, 0, 0, EXTI4_IRQHandler, 0);
    LOS_HwiCreate(EXTI9_5_IRQn, 0, 0, EXTI9_5_IRQHandler, 0);
    LOS_IntRestore(uvIntSave);
    
    NVIC_PriorityGroupConfig(NVIC_PriorityGroup_1);
    NVIC_InitStructure.NVIC_IRQChannel = EXTI0_IRQn | EXTI4_IRQn | EXTI1_IRQn | EXTI9_5_IRQn;  // EXTI0_IRQn;
    NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 0x02;
    NVIC_InitStructure.NVIC_IRQChannelSubPriority = 0x02;
    NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
    NVIC_Init(&NVIC_InitStructure);
    
    return 0;
}
void NVIC_Cfg(void)
{
    EXTI_InitTypeDef EXTI_InitStructure;

    EXTI_ClearITPendingBit(EXTI_Line0);
    GPIO_EXTILineConfig(GPIO_PortSourceGPIOB, GPIO_PinSource0);
    EXTI_InitStructure.EXTI_Line = EXTI_Line0;
    EXTI_InitStructure.EXTI_Mode = EXTI_Mode_Interrupt;
    EXTI_InitStructure.EXTI_Trigger = EXTI_Trigger_Rising_Falling;
    EXTI_InitStructure.EXTI_LineCmd = ENABLE;
    EXTI_Init(&EXTI_InitStructure);
    
    EXTI_ClearITPendingBit(EXTI_Line1);
    GPIO_EXTILineConfig(GPIO_PortSourceGPIOB, GPIO_PinSource1);
    EXTI_InitStructure.EXTI_Line = EXTI_Line1;
    EXTI_InitStructure.EXTI_Mode = EXTI_Mode_Interrupt;
    EXTI_InitStructure.EXTI_Trigger = EXTI_Trigger_Rising_Falling;
    EXTI_InitStructure.EXTI_LineCmd = ENABLE;
    EXTI_Init(&EXTI_InitStructure);
    
    EXTI_ClearITPendingBit(EXTI_Line4);
    GPIO_EXTILineConfig(GPIO_PortSourceGPIOC, GPIO_PinSource4);
    EXTI_InitStructure.EXTI_Line = EXTI_Line4;
    EXTI_InitStructure.EXTI_Mode = EXTI_Mode_Interrupt;
    EXTI_InitStructure.EXTI_Trigger = EXTI_Trigger_Rising_Falling;
    EXTI_InitStructure.EXTI_LineCmd = ENABLE;
    EXTI_Init(&EXTI_InitStructure);
	
	EXTI_ClearITPendingBit(EXTI_Line5);
    GPIO_EXTILineConfig(GPIO_PortSourceGPIOC, GPIO_PinSource5);
    EXTI_InitStructure.EXTI_Line = EXTI_Line5;
    EXTI_InitStructure.EXTI_Mode = EXTI_Mode_Interrupt;
    EXTI_InitStructure.EXTI_Trigger = EXTI_Trigger_Rising_Falling;
    EXTI_InitStructure.EXTI_LineCmd = ENABLE;
    EXTI_Init(&EXTI_InitStructure);
}
void key_timer(void)
{
    K_timer++;
}
bool key_exit(void)
{
    return 0;
}

enum KEY key_get_status(void)
{
	enum KEY keystatus = nokey;
    UINT32 uwEvent, uwRet;
	
    if (GPIO_ReadInputDataBit(KEY1PORT ,KEY1PIN) && 
        GPIO_ReadInputDataBit(KEY2PORT ,KEY2PIN) && 
        GPIO_ReadInputDataBit(KEY3PORT ,KEY3PIN) &&
		GPIO_ReadInputDataBit(KEY4PORT ,KEY4PIN)) {
        keystatus = nokey;
        LOS_EventClear(&key_event, ~key_event.uwEventID);
    }
    
    uwEvent = LOS_EventRead(&key_event,
                            event_key1 | event_key2 | event_key3 | event_key4 |
							event_long1 | event_long2 | event_long3 | event_long4,
                            LOS_WAITMODE_OR,
                            80);
	LOS_EventClear(&key_event, ~(0x1f));
    switch (uwEvent) {
        case event_key1:
            keystatus = key1short;
		
            uwRet = LOS_EventWrite(&key_event, event_long1);
            if(uwRet != LOS_OK) {
                printf("event write failed .\n");
            }
            break;
        case event_key2:
            keystatus = key2short;
        
            uwRet = LOS_EventWrite(&key_event, event_long2);
            if(uwRet != LOS_OK) {
                printf("event write failed .\n");
            }    
		break;
        case event_key3:
            keystatus = key3short;
		
            uwRet = LOS_EventWrite(&key_event, event_long3);
            if(uwRet != LOS_OK) {
                printf("event write failed .\n");
            }
        break;
		case event_key4:
            keystatus = key4short;
            uwRet = LOS_EventWrite(&key_event, event_long4);
            if(uwRet != LOS_OK) {
                printf("event write failed .\n");
            }
        break;
			
		case event_long1:
			if (K_timer > 150) {
				K_timer = 0;
                keystatus = key1long;
                return keystatus;
            }
		break;
			
		case event_long2:
			if (K_timer > 25) {
				K_timer = 12;
                keystatus = key2long;
                return keystatus;
            }
		break;
		case event_long3:
			if (K_timer > 25) {
				K_timer = 12;
                keystatus = key3long;
                return keystatus;
            }
		break;
			
		case event_long4:
			if (K_timer > 150) {
				K_timer = 0;
                keystatus = key4long;
                return keystatus;
            }
		break;
        default:
            keystatus = nokey;
            return nokey;
    }

    return keystatus;
}

void EXTI0_IRQHandler(void)
{
    UINT32 uwRet, i;
    
    for(i = 0;i<200;i++)
            ;
    if(EXTI_GetITStatus(EXTI_Line0) != RESET) {  // Guaranteed interrupt in EXTI Line
        if (GPIO_ReadInputDataBit(KEY1PORT ,KEY1PIN)) {
            Timer4_Close();
            K_timer = 0;
            LOS_EventClear(&key_event, ~event_key1);
        } else {
            K_timer = 0;
            Timer4SetIRQCallBack(key_timer); 
            Timer4_Open(1000, 720);
            uwRet = LOS_EventWrite(&key_event, event_key1);
            if(uwRet != LOS_OK) {
                printf("event write failed .\n");
            } 
        }
        EXTI_ClearITPendingBit(EXTI_Line0);  // clear interrupt flag bit
    }  
}

void EXTI1_IRQHandler(void)
{
    UINT32 uwRet, i;
    for(i = 0;i<200;i++)
            ;
    if(EXTI_GetITStatus(EXTI_Line1) != RESET) {  // Guaranteed interrupt in EXTI Line
        if (GPIO_ReadInputDataBit(KEY4PORT ,KEY4PIN)) {
            Timer4_Close();
            K_timer = 0;
            LOS_EventClear(&key_event, ~event_key4);
        } else {
            K_timer = 0;
            Timer4SetIRQCallBack(key_timer); 
            Timer4_Open(1000, 720);
            uwRet = LOS_EventWrite(&key_event, event_key4);
            if(uwRet != LOS_OK) {
                printf("event write failed .\n");
            }
        }
        EXTI_ClearITPendingBit(EXTI_Line1);  // clear interrupt flag bit
    }  
}

void EXTI4_IRQHandler(void)
{
    UINT32 uwRet, i;
    for(i = 0;i<200;i++)
            ;
    if(EXTI_GetITStatus(EXTI_Line4) != RESET) {  // Guaranteed interrupt in EXTI Line
        if (GPIO_ReadInputDataBit(KEY2PORT ,KEY2PIN)) {
            Timer4_Close();
            K_timer = 0;
            LOS_EventClear(&key_event, ~event_key2);
        } else {
            K_timer = 0;
            Timer4SetIRQCallBack(key_timer); 
            Timer4_Open(1000, 720);
            uwRet = LOS_EventWrite(&key_event, event_key2);
            if(uwRet != LOS_OK) {
                printf("event write failed .\n");
            }
        }
        EXTI_ClearITPendingBit(EXTI_Line4);  // clear interrupt flag bit
    }  
}


void EXTI9_5_IRQHandler(void)
{
    UINT32 uwRet, i;
    for(i = 0;i<200;i++)
            ;
    if(EXTI_GetITStatus(EXTI_Line5) != RESET) {  // Guaranteed interrupt in EXTI Line
      if (GPIO_ReadInputDataBit(KEY3PORT ,KEY3PIN)) {
          Timer4_Close();
          K_timer = 0;
          LOS_EventClear(&key_event, ~event_key3);
        } else {
            K_timer = 0;
            Timer4SetIRQCallBack(key_timer); 
            Timer4_Open(1000, 720);
            uwRet = LOS_EventWrite(&key_event, event_key3);
            if(uwRet != LOS_OK) {
                printf("event write failed .\n");
            }
        }
      EXTI_ClearITPendingBit(EXTI_Line5);  // clear interrupt flag bit
    }  
}

