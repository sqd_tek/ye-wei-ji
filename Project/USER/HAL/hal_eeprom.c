#include "hal_eeprom.h"
#include "config.h"
#include "los_task.h"
#include "los_mux.h"
#include "hal_senser.h"
#include "string.h"
#include "delay.h"

#define d_printf printf
extern int g_print_flag;
// 信号量，读写 eeprom 时，需要申请信号量，如果申请不到，需要等到
static uint32_t  g_mux_eeprom;
extern AD_Config AD_cfg[];


/*立式罐参数*/
extern float Diameter_cm[2];
extern float Diameter_div4[2];
extern float PiDD[2];
extern float coff_ka[2]; //当h<=D/4 V(ml)=coff_ka*h
extern float coff_kb[2]; //当h>D/4 V(ml)=coff_kb*h - coff_b;
extern float coff_b[2];


// 当前可改变配置的罐子编号
int g_tank_num_id = 0;

void Eeprom_Delay(uint32_t ms) {
	LOS_TaskDelay(ms);
}

void hal_eeprom_Init(void) {
	AT24CXX_Init();
	while (AT24CXX_Check()) {
		printf("eeprom faild\r\n");
		Eeprom_Delay(1000);
	}
}

void hal_eeprom_ReadBytes(uint32_t Addr, uint8_t* DataBuff, uint32_t LenOfRead) {
	// 申请互斥锁
	if (LOS_MuxPend(g_mux_eeprom, LOS_WAIT_FOREVER) == LOS_OK) {
		while(LenOfRead)
		{
			*DataBuff++=AT24CXX_ReadOneByte(Addr++);	
			LenOfRead--;
		}
		/*释放互斥锁*/
		LOS_MuxPost(g_mux_eeprom);
	}
}

void hal_eeprom_WriteBytes(uint32_t Addr, uint8_t* DataBuff, uint32_t LenOfWrite) {
	// 申请互斥锁
	if (LOS_MuxPend(g_mux_eeprom, LOS_WAIT_FOREVER) == LOS_OK) {
		while(LenOfWrite--)
		{
			AT24CXX_WriteOneByte(Addr,*DataBuff);
			Addr++;
			DataBuff++;
		}
		/*释放互斥锁*/
		LOS_MuxPost(g_mux_eeprom);
	}
}

uint16_t eeprom_Read16Bits(uint32_t ReadAddr) {
	int res = -1;
	
	// 申请互斥锁
	if (LOS_MuxPend(g_mux_eeprom, LOS_WAIT_FOREVER) == LOS_OK) {
		res = AT24CXX_ReadLenByte(ReadAddr, 2);
		/*释放互斥锁*/
		LOS_MuxPost(g_mux_eeprom);
	}
	return res;
}
void eeprom_Write16Bits(uint32_t WriteAddr, uint16_t WriteData) {
	uint16_t data;
	
	data = WriteData & 0xFFFF;
	
	// 申请互斥锁
	if (LOS_MuxPend(g_mux_eeprom, LOS_WAIT_FOREVER) == LOS_OK) {
		AT24CXX_WriteLenByte(WriteAddr, data, 2);
		/*释放互斥锁*/
		LOS_MuxPost(g_mux_eeprom);
	}
}
uint32_t eeprom_Read32Bits(uint32_t ReadAddr) {
	int res = -1;
	// 申请互斥锁
	if (LOS_MuxPend(g_mux_eeprom, LOS_WAIT_FOREVER) == LOS_OK) {
		res = AT24CXX_ReadLenByte(ReadAddr, 4);
		/*释放互斥锁*/
		LOS_MuxPost(g_mux_eeprom);
	}
	
	return res;
}
void eeprom_Write32Bits(uint32_t WriteAddr, uint32_t data) {
	// 申请互斥锁
	if (LOS_MuxPend(g_mux_eeprom, LOS_WAIT_FOREVER) == LOS_OK) {
		printf("write 32bits\r\n");
		AT24CXX_WriteLenByte(WriteAddr, data, 4);
		/*释放互斥锁*/
		LOS_MuxPost(g_mux_eeprom);
	}
}


uint32_t getMask(int id, int mask_mode) {
	if (mask_mode) {
		return g_board_info[id].gprs_send_alarm_mask;
	} else {
		return g_board_info[id].gprs_send_data_mask;
	}
}

void setMask(int id, int mask_mode, uint32_t mask_value) {
	if (mask_mode) {
		g_board_info[id].gprs_send_alarm_mask = mask_value;
		eeprom_Write32Bits(EEPROM_MASK_ALARM_ADDR + EEPROM_PAR_MAX_ADDR * id, g_board_info[id].gprs_send_alarm_mask);
	} else {
		g_board_info[id].gprs_send_data_mask = mask_value;
		eeprom_Write32Bits(EEPROM_MASK_DATA_ADDR + EEPROM_PAR_MAX_ADDR * id, g_board_info[id].gprs_send_data_mask);
	}
}

void readID(void) {
	printf("tank[%d].id:%d\r\n", g_tank_num_id, g_board_info[g_tank_num_id].id);
}

void writeID(uint32_t id) {
	g_board_info[g_tank_num_id].id = id;
	
	printf("devid:%d\r\n", g_board_info[g_tank_num_id].id);
}

void eeprom_SetBoardDefault(void) {
	g_board_info[0].flag 					 			= BOARD_FLAG;
	printf("id:%d\r\n", g_tank_num_id);
	g_board_info[g_tank_num_id].id		 				= BOARD_ID;

	g_board_info[g_tank_num_id].diffpressure_ValueHigh  = BOARD_DP_VALUE_HIGH;
	g_board_info[g_tank_num_id].diffpressure_ValueLow   = BOARD_DP_VALUE_LOW;
	g_board_info[g_tank_num_id].diffpressure_VoltageHigh= BOARD_DP_VOL_HIGH;
	g_board_info[g_tank_num_id].diffpressure_VoltageLow = BOARD_DP_VOL_LOW;
	
	g_board_info[g_tank_num_id].pressure_ValueHigh      = BOARD_P_VALUE_HIGH;
	g_board_info[g_tank_num_id].pressure_ValueLow       = BOARD_P_VALUE_LOW;
	g_board_info[g_tank_num_id].pressure_VoltageHigh    = BOARD_P_VOL_HIGH;
	g_board_info[g_tank_num_id].pressure_VoltageLow     = BOARD_P_VOL_LOW;
	
	g_board_info[g_tank_num_id].temp_ValueHigh      = BOARD_T_VALUE_HIGH;
	g_board_info[g_tank_num_id].temp_ValueLow       = BOARD_T_VALUE_LOW;
	g_board_info[g_tank_num_id].temp_CurrentHigh    = BOARD_T_VOL_HIGH;
	g_board_info[g_tank_num_id].temp_CurrentLow     = BOARD_T_VOL_LOW;
	
	// 卧式罐计算系数
	g_board_info[g_tank_num_id].diffpressure_a			= BOARD_DP_A;
	g_board_info[g_tank_num_id].diffpressure_b			= BOARD_DP_B;
	g_board_info[g_tank_num_id].inputpressure_a			= BOARD_IP_A;
	g_board_info[g_tank_num_id].inputpressure_b			= BOARD_IP_B;
		
	// 卧式罐配置信息
	g_board_info[g_tank_num_id].maxVolume				= BOARD_MAX_VOLUME;
	g_board_info[g_tank_num_id].length					= BOARD_LENGTH;
	g_board_info[g_tank_num_id].diameter				= BOARD_DIAMETER;
	g_board_info[g_tank_num_id].gprs_send_alarm_mask	= BOARD_MASK_ALARM;
	g_board_info[g_tank_num_id].gprs_send_data_mask		= BOARD_MASK_DATA;
	g_board_info[g_tank_num_id].gas_index				= BOARD_GAS_INDEX;
	
	g_board_info[g_tank_num_id].alarm_LevelHighHigh	= BOARD_ALARM_DPHH;
	g_board_info[g_tank_num_id].alarm_LevelHigh		= BOARD_ALARM_DPH;
	g_board_info[g_tank_num_id].alarm_LevelLowLow	= BOARD_ALARM_DPLL;
	g_board_info[g_tank_num_id].alarm_LevelLow		= BOARD_ALARM_DPL;
	g_board_info[g_tank_num_id].alarm_InputPressureHigh		= BOARD_ALARM_IPH;
	
	
	// 立式罐计算参数
	Diameter_cm[g_tank_num_id] = (float)g_board_info[g_tank_num_id].diameter/10.0;//单位cm
	Diameter_div4[g_tank_num_id] = Diameter_cm[g_tank_num_id]/4.0;//单位cm
	PiDD[g_tank_num_id] = PI*Diameter_cm[g_tank_num_id]*Diameter_cm[g_tank_num_id];
	
	coff_ka[g_tank_num_id] = PiDD[g_tank_num_id]/6.0; //当h<=D/4 V(ml)=coff_ka*h
	coff_kb[g_tank_num_id] = PiDD[g_tank_num_id]/4.0; //当h>D/4 V(ml)=coff_kb*h - coff_b;
	coff_b[g_tank_num_id]  = Diameter_cm[g_tank_num_id]*PiDD[g_tank_num_id]/48.0;

	
	// AD_CFG  AD通道配置信息
	//for (i = 0;i < MAX_SENSOR;i++) {
//		temp 								= ((100 << 12) | (0 << 10) | (0 << 8) (1));					// 100欧姆电阻
	if (g_tank_num_id == 0) {
		AD_cfg[0].AD_Channel								= 1;		// 0-7 bits
		AD_cfg[0].AD_Mode									= 0; 		// 8-9 bits
		AD_cfg[0].pos										= 0;		// 10-11 bits
		AD_cfg[0].res										= 100;		// 13-31bits

		AD_cfg[1].AD_Channel								= 2;		// 0-7 bits
		AD_cfg[1].AD_Mode									= 0; 		// 8-9 bits
		AD_cfg[1].pos										= 0;		// 10-11 bits
		AD_cfg[1].res										= 100;		// 13-31bits		
	} else {	
		AD_cfg[2].AD_Channel								= 3;		// 0-7 bits
		AD_cfg[2].AD_Mode									= 0; 		// 8-9 bits
		AD_cfg[2].pos										= 0;		// 10-11 bits
		AD_cfg[2].res										= 100;		// 13-31bits

		AD_cfg[3].AD_Channel								= 4;		// 0-7 bits
		AD_cfg[3].AD_Mode									= 0; 		// 8-9 bits
		AD_cfg[3].pos										= 0;		// 10-11 bits
		AD_cfg[3].res										= 100;		// 13-31bits		
	}	
	//}
	printf("eeprom get board default information finish\r\n");
}

void eeprom_SaveBoardInfo(void) {
	int i;
	uint32_t temp;
	
	eeprom_Write32Bits(EEPROM_DP_VALUE_HIGH_ADDR + EEPROM_PAR_MAX_ADDR * g_tank_num_id, 	g_board_info[g_tank_num_id].diffpressure_ValueHigh);
	eeprom_Write32Bits(EEPROM_DP_VALUE_LOW_ADDR + EEPROM_PAR_MAX_ADDR * g_tank_num_id,  	g_board_info[g_tank_num_id].diffpressure_ValueLow);
	eeprom_Write32Bits(EEPROM_DP_VOL_HIGH_ADDR + EEPROM_PAR_MAX_ADDR * g_tank_num_id,   	g_board_info[g_tank_num_id].diffpressure_VoltageHigh);
	eeprom_Write32Bits(EEPROM_DP_VOL_LOW_ADDR + EEPROM_PAR_MAX_ADDR * g_tank_num_id,  		g_board_info[g_tank_num_id].diffpressure_VoltageLow);
	
	eeprom_Write32Bits(EEPROM_P_VALUE_HIGH_ADDR + EEPROM_PAR_MAX_ADDR * g_tank_num_id, 		g_board_info[g_tank_num_id].pressure_ValueHigh);
	eeprom_Write32Bits(EEPROM_P_VALUE_LOW_ADDR + EEPROM_PAR_MAX_ADDR * g_tank_num_id,  		g_board_info[g_tank_num_id].pressure_ValueLow);
	eeprom_Write32Bits(EEPROM_P_VOL_HIGH_ADDR + EEPROM_PAR_MAX_ADDR * g_tank_num_id,   		g_board_info[g_tank_num_id].pressure_VoltageHigh);
	eeprom_Write32Bits(EEPROM_P_VOL_LOW_ADDR + EEPROM_PAR_MAX_ADDR * g_tank_num_id,  		g_board_info[g_tank_num_id].pressure_VoltageLow);

	eeprom_Write32Bits(EEPROM_ID_ADDR + EEPROM_PAR_MAX_ADDR * g_tank_num_id, 				g_board_info[g_tank_num_id].id);
	
	// 卧式罐计算系数
	eeprom_Write32Bits(EEPROM_DP_A_ADDR + EEPROM_PAR_MAX_ADDR * g_tank_num_id, g_board_info[g_tank_num_id].diffpressure_a);
	eeprom_Write32Bits(EEPROM_DP_B_ADDR + EEPROM_PAR_MAX_ADDR * g_tank_num_id, g_board_info[g_tank_num_id].diffpressure_b);
	eeprom_Write32Bits(EEPROM_IP_A_ADDR + EEPROM_PAR_MAX_ADDR * g_tank_num_id, g_board_info[g_tank_num_id].inputpressure_a);
	eeprom_Write32Bits(EEPROM_IP_B_ADDR + EEPROM_PAR_MAX_ADDR * g_tank_num_id, g_board_info[g_tank_num_id].inputpressure_b);
	
	
	// 卧式罐配置信息
	eeprom_Write32Bits(EEPROM_MAX_VOLUME_ADDR + EEPROM_PAR_MAX_ADDR * g_tank_num_id, g_board_info[g_tank_num_id].maxVolume);
	eeprom_Write32Bits(EEPROM_LENGTH_ADDR + EEPROM_PAR_MAX_ADDR * g_tank_num_id, g_board_info[g_tank_num_id].length);
	eeprom_Write32Bits(EEPROM_DIAMETER_ADDR + EEPROM_PAR_MAX_ADDR * g_tank_num_id, g_board_info[g_tank_num_id].diameter);
	eeprom_Write32Bits(EEPROM_MASK_DATA_ADDR + EEPROM_PAR_MAX_ADDR * g_tank_num_id, g_board_info[g_tank_num_id].gprs_send_data_mask);
	eeprom_Write32Bits(EEPROM_MASK_ALARM_ADDR + EEPROM_PAR_MAX_ADDR * g_tank_num_id, g_board_info[g_tank_num_id].gprs_send_alarm_mask);
	eeprom_Write32Bits(EEPROM_TANK_TYPE_ADDR + EEPROM_PAR_MAX_ADDR * g_tank_num_id, g_board_info[g_tank_num_id].tank_type);
	eeprom_Write32Bits(EEPROM_SEND_TIME_ADDR + EEPROM_PAR_MAX_ADDR * g_tank_num_id, g_board_info[g_tank_num_id].sendTimeIndex);
	eeprom_Write32Bits(EEPROM_GAS_INDEX_ADDR + EEPROM_PAR_MAX_ADDR * g_tank_num_id, g_board_info[g_tank_num_id].gas_index);
	
	// 报警阈值信息
	eeprom_Write32Bits(EEPROM_ALARM_DPHH_ADDR + EEPROM_PAR_MAX_ADDR * g_tank_num_id, g_board_info[g_tank_num_id].alarm_LevelHighHigh);
	eeprom_Write32Bits(EEPROM_ALARM_DPH_ADDR + EEPROM_PAR_MAX_ADDR * g_tank_num_id, g_board_info[g_tank_num_id].alarm_LevelHigh);
	eeprom_Write32Bits(EEPROM_ALARM_DPLL_ADDR + EEPROM_PAR_MAX_ADDR * g_tank_num_id, g_board_info[g_tank_num_id].alarm_LevelLowLow);
	eeprom_Write32Bits(EEPROM_ALARM_DPL_ADDR + EEPROM_PAR_MAX_ADDR * g_tank_num_id, g_board_info[g_tank_num_id].alarm_LevelLow);
	eeprom_Write32Bits(EEPROM_ALARM_IPH_ADDR + EEPROM_PAR_MAX_ADDR * g_tank_num_id, g_board_info[g_tank_num_id].alarm_InputPressureHigh);
	
	
	// AD_CFG  AD通道配置信息
	for (i = 0;i < MAX_SENSOR;i++) {
		temp = 0;
		temp |= AD_cfg[i].AD_Channel;				// 0-7 bits
		temp |= AD_cfg[i].AD_Mode << 8; 			// 8-9 bits
		temp |= AD_cfg[i].pos << 10;				// 10-11 bits
		temp |= AD_cfg[i].res << 12;				// 13-31bits		
		eeprom_Write32Bits(EEPROM_AD_BASE_ADDR + 4 * i, temp);
	}
		
	
	eeprom_Write32Bits(EEPROM_FLAG_ADDR, 			BOARD_FLAG);
	printf("eeprom save board information finish\r\n");
}

void eeprom_GetBoardInfo(void) {
	int i;
	uint32_t temp;
	
	
	g_board_info[g_tank_num_id].id						 	= eeprom_Read32Bits(EEPROM_ID_ADDR);
	g_board_info[g_tank_num_id].diffpressure_ValueHigh 		= eeprom_Read32Bits(EEPROM_DP_VALUE_HIGH_ADDR + EEPROM_PAR_MAX_ADDR * g_tank_num_id);
	g_board_info[g_tank_num_id].diffpressure_ValueLow 		= eeprom_Read32Bits(EEPROM_DP_VALUE_LOW_ADDR + EEPROM_PAR_MAX_ADDR * g_tank_num_id);
	g_board_info[g_tank_num_id].diffpressure_VoltageHigh 	= eeprom_Read32Bits(EEPROM_DP_VOL_HIGH_ADDR + EEPROM_PAR_MAX_ADDR * g_tank_num_id);
	g_board_info[g_tank_num_id].diffpressure_VoltageLow 	= eeprom_Read32Bits(EEPROM_DP_VOL_LOW_ADDR + EEPROM_PAR_MAX_ADDR * g_tank_num_id);
	g_board_info[g_tank_num_id].pressure_ValueHigh 			= eeprom_Read32Bits(EEPROM_P_VALUE_HIGH_ADDR + EEPROM_PAR_MAX_ADDR * g_tank_num_id);
	g_board_info[g_tank_num_id].pressure_ValueLow 			= eeprom_Read32Bits(EEPROM_P_VALUE_LOW_ADDR + EEPROM_PAR_MAX_ADDR * g_tank_num_id);
	g_board_info[g_tank_num_id].pressure_VoltageHigh 		= eeprom_Read32Bits(EEPROM_P_VOL_HIGH_ADDR + EEPROM_PAR_MAX_ADDR * g_tank_num_id);
	g_board_info[g_tank_num_id].pressure_VoltageLow 		= eeprom_Read32Bits(EEPROM_P_VOL_LOW_ADDR + EEPROM_PAR_MAX_ADDR * g_tank_num_id);


	// 卧式罐计算系数
	g_board_info[g_tank_num_id].diffpressure_a				= eeprom_Read32Bits(EEPROM_DP_A_ADDR + EEPROM_PAR_MAX_ADDR * g_tank_num_id);
	g_board_info[g_tank_num_id].diffpressure_b				= eeprom_Read32Bits(EEPROM_DP_B_ADDR + EEPROM_PAR_MAX_ADDR * g_tank_num_id);
	g_board_info[g_tank_num_id].inputpressure_a				= eeprom_Read32Bits(EEPROM_IP_A_ADDR + EEPROM_PAR_MAX_ADDR * g_tank_num_id);
	g_board_info[g_tank_num_id].inputpressure_b				= eeprom_Read32Bits(EEPROM_IP_B_ADDR + EEPROM_PAR_MAX_ADDR * g_tank_num_id);
	
	
	// 卧式罐配置信息
	g_board_info[g_tank_num_id].maxVolume					= eeprom_Read32Bits(EEPROM_MAX_VOLUME_ADDR + EEPROM_PAR_MAX_ADDR * g_tank_num_id);
	g_board_info[g_tank_num_id].length						= eeprom_Read32Bits(EEPROM_LENGTH_ADDR + EEPROM_PAR_MAX_ADDR * g_tank_num_id);
	g_board_info[g_tank_num_id].diameter					= eeprom_Read32Bits(EEPROM_DIAMETER_ADDR + EEPROM_PAR_MAX_ADDR * g_tank_num_id);
	g_board_info[g_tank_num_id].tank_type					= eeprom_Read32Bits(EEPROM_TANK_TYPE_ADDR + EEPROM_PAR_MAX_ADDR * g_tank_num_id);
	g_board_info[g_tank_num_id].sendTimeIndex				= eeprom_Read32Bits(EEPROM_SEND_TIME_ADDR + EEPROM_PAR_MAX_ADDR * g_tank_num_id);
	g_board_info[g_tank_num_id].gas_index    				= eeprom_Read32Bits(EEPROM_GAS_INDEX_ADDR + EEPROM_PAR_MAX_ADDR * g_tank_num_id);
	
	
	// 立式罐计算参数
	Diameter_cm[g_tank_num_id] = (float)g_board_info[g_tank_num_id].diameter/10.0;//单位cm
	Diameter_div4[g_tank_num_id] = Diameter_cm[g_tank_num_id]/4.0;//单位cm
	PiDD[g_tank_num_id] = PI*Diameter_cm[g_tank_num_id]*Diameter_cm[g_tank_num_id];
	
	coff_ka[g_tank_num_id] = PiDD[g_tank_num_id]/6.0; //当h<=D/4 V(ml)=coff_ka*h
	coff_kb[g_tank_num_id] = PiDD[g_tank_num_id]/4.0; //当h>D/4 V(ml)=coff_kb*h - coff_b;
	coff_b[g_tank_num_id]  = Diameter_cm[g_tank_num_id]*PiDD[g_tank_num_id]/48.0;

	
	
	// 掩码
	g_board_info[g_tank_num_id].gprs_send_data_mask			= eeprom_Read32Bits(EEPROM_MASK_DATA_ADDR + EEPROM_PAR_MAX_ADDR * g_tank_num_id);
	g_board_info[g_tank_num_id].gprs_send_alarm_mask		= eeprom_Read32Bits(EEPROM_MASK_ALARM_ADDR + EEPROM_PAR_MAX_ADDR * g_tank_num_id);
	
	// 报警阈值信息
	g_board_info[g_tank_num_id].alarm_LevelHighHigh			= eeprom_Read32Bits(EEPROM_ALARM_DPHH_ADDR + EEPROM_PAR_MAX_ADDR * g_tank_num_id);
	g_board_info[g_tank_num_id].alarm_LevelHigh				= eeprom_Read32Bits(EEPROM_ALARM_DPH_ADDR + EEPROM_PAR_MAX_ADDR * g_tank_num_id);
	g_board_info[g_tank_num_id].alarm_LevelLowLow			= eeprom_Read32Bits(EEPROM_ALARM_DPLL_ADDR + EEPROM_PAR_MAX_ADDR * g_tank_num_id);
	g_board_info[g_tank_num_id].alarm_LevelLow				= eeprom_Read32Bits(EEPROM_ALARM_DPL_ADDR + EEPROM_PAR_MAX_ADDR * g_tank_num_id);
	g_board_info[g_tank_num_id].alarm_InputPressureHigh 	= eeprom_Read32Bits(EEPROM_ALARM_IPH_ADDR + EEPROM_PAR_MAX_ADDR * g_tank_num_id);
	
	
		
	// AD_CFG  AD通道配置信息
	for (i = 0;i < MAX_SENSOR;i++) {
		temp 								= eeprom_Read32Bits(EEPROM_AD_BASE_ADDR + 4 * i);
		AD_cfg[i].AD_Channel				= temp & 0xFF;				// 0-7 bits
		AD_cfg[i].AD_Mode					= (temp >> 8) & 0x3; 		// 8-9 bits
		AD_cfg[i].pos						= (temp >> 10) & 0x3;		// 10-11 bits
		AD_cfg[i].res						= (temp >> 12) & 0xFFFFF;	// 13-31bits		
	}
		
	printf("eeprom get board information finish\r\n");
}
void eeprom_PrintBoardInfo(void) {
	int i;
	
	printf("Board information:\r\n");
	printf("id:%d\r\n", g_board_info[g_tank_num_id].id);
	printf("gas:%d\r\n", g_board_info[g_tank_num_id].gas_index);
	// 卧式罐计算系数
	printf("diffpressure_a:%f\r\n", g_board_info[g_tank_num_id].diffpressure_a);
	printf("diffpressure_b:%f\r\n", g_board_info[g_tank_num_id].diffpressure_b);
	printf("iinputpressure_a:%f\r\n", g_board_info[g_tank_num_id].inputpressure_a);
	printf("inputpressure_b:%f\r\n", g_board_info[g_tank_num_id].inputpressure_b);
	
	
	// 卧式罐配置信息
	printf("max volume:%d\r\n", g_board_info[g_tank_num_id].maxVolume);
	printf("length:%d\r\n", g_board_info[g_tank_num_id].length);
	printf("diameter:%d\r\n", g_board_info[g_tank_num_id].diameter);
	
	printf("data mask:%d\r\n", g_board_info[g_tank_num_id].gprs_send_data_mask);
	printf("alarm mask:%d\r\n", g_board_info[g_tank_num_id].gprs_send_alarm_mask);
	
	printf("level high high:%d\r\n", g_board_info[g_tank_num_id].alarm_LevelHighHigh);
	printf("level high:%d\r\n", g_board_info[g_tank_num_id].alarm_LevelHigh);
	printf("level low low:%d\r\n", g_board_info[g_tank_num_id].alarm_LevelLowLow);
	printf("level low:%d\r\n", g_board_info[g_tank_num_id].alarm_LevelLow);
	printf("input pressure high:%d\r\n", g_board_info[g_tank_num_id].alarm_InputPressureHigh);
	
	
	
	printf("id\tchannel\tmode\tpos\tres\r\n");
	
	// AD_CFG  AD通道配置信息
	for (i = 0;i < MAX_SENSOR;i++) {
		printf("%d\t", i);
		printf("%d\t", AD_cfg[i].AD_Channel);				// 0-7 bits
		printf("%d\t", AD_cfg[i].AD_Mode); 		// 8-9 bits
		printf("%d\t", AD_cfg[i].pos);		// 10-11 bits
		printf("%d\t", AD_cfg[i].res);	// 13-31bits	
		printf("\r\n");
	}
	printf("\r\n");
}
void eeprom_SetBoardInfo(void) {
	uint32_t eeprom_flag = 0;
	int num = g_tank_num_id;
	
	eeprom_flag = eeprom_Read32Bits(EEPROM_FLAG_ADDR);
	printf("\r\nflag = %d\r\n", eeprom_flag);
	if (eeprom_flag != BOARD_FLAG) {
		// 获取两个罐的信息
		for (g_tank_num_id = 0;g_tank_num_id < TANK_MAX_LEN;g_tank_num_id++) {
			eeprom_SetBoardDefault();
			eeprom_SaveBoardInfo();
	
			eeprom_PrintBoardInfo();		
			printf("id:%d  %d\r\n", g_tank_num_id, g_board_info[g_tank_num_id].id);
		}		
	} else {
		// 获取两个罐的信息
		for (g_tank_num_id = 0;g_tank_num_id < TANK_MAX_LEN;g_tank_num_id++) {
			eeprom_GetBoardInfo();
			eeprom_PrintBoardInfo();	
		}
	}
	
	
	for (g_tank_num_id = 0;g_tank_num_id < TANK_MAX_LEN;g_tank_num_id++) {
		calc_diffpressureK(g_board_info[g_tank_num_id].diffpressure_ValueLow,   g_board_info[g_tank_num_id].diffpressure_ValueHigh, 
						   g_board_info[g_tank_num_id].diffpressure_VoltageLow, g_board_info[g_tank_num_id].diffpressure_VoltageHigh);
        calc_pressureK(g_board_info[g_tank_num_id].pressure_ValueLow,   g_board_info[g_tank_num_id].pressure_ValueHigh, 
                       g_board_info[g_tank_num_id].pressure_VoltageLow, g_board_info[g_tank_num_id].pressure_VoltageHigh);

	}
	
	g_tank_num_id = num;
}


void eeprom_task(void) {
	// 创建互斥锁
	if (LOS_MuxCreate(&g_mux_eeprom) != LOS_OK) {
		d_printf("eeprom mux create faild\r\n");
	}
	hal_eeprom_Init();
	
	eeprom_SetBoardInfo();
}


