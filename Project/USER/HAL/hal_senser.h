#ifndef __HAL_SENSER_H
#define __HAL_SENSER_H

#include "config.h"

#define MAXVOLUME_5M3     6000
#define MAXVOLUME_10M3    11000

#define MIN_DIVISION_20    15
#define MIN_DIVISION_50    45
#define MIN_DIVISION_100   95


typedef struct _AD_Config {
	uint32_t AD_Channel;
	uint32_t AD_Mode;
	uint32_t pos;
	uint32_t res;
} AD_Config;

extern AD_Config AD_cfg[];
extern float g_diffpressure_k[2];
extern float g_pressure_k[2];
extern float g_temp_k[2];

void get_sensor_data_task(void);
void calc_KValue(void);
void AD_SetLength(int len);
int AD_GetLength(void);
void AD_SetDiameter(int Diameter);
int AD_GetDiameter(void);
void AD_SetAB(int serson_num, int a, int b);
void AD_getAB(void);
void AD_SetRes(int num, int res);
int AD_GetRes(int num);
void AD_SetCfg(int num, int channel_num, int mode, int pos);
int AD_GetCfg(int num, int n);
//void AD_SetValue(enum sensor_num sensor, int pos, int value);
//void AD_SetVoltageValue(enum sensor_num sensor, int pos, int value);
int AD_ChannelVoltage(int channel_num, int mode, int pos);
void calc_diffpressureK(int dp1, int dp2, int cur1, int cur2);
void calc_pressureK(int p1, int p2, int cur1, int cur2);
void calc_tempK(int t1, int t2, int cur1, int cur2);
int getMaxVolume(void);
void setMaxVolume(int maxVolume);
void setThreshold(int levelHH, int levelH, int levelL, int levelLL, int pressureH);
void getThreshold(void);
void AD_SetTankType(int tanktype);
int AD_GetTankType(void);
int AD_GetGas(void);
void AD_SetGas(int gasIndex);
#endif
