#ifndef __HAL_LCD_H__
#define __HAL_LCD_H__
#include "los_typedef.h"
#include "stm32f10x.h"
#include "hal_key.h"
#include "oled.h"
#include "display.h"


#ifdef __cplusplus
extern "C" {
#endif

// 可以增加多个键一块摁功能
// 按键点击返回值    
#define KEY_MENU        key1short
#define KEY_UP          key2short
#define KEY_DOWN        key3short
#define KEY_ENTER       key4short

//按键长按返回值    
#define KEY_MENU_C      key1long
#define KEY_UP_C        key2long
#define KEY_DOWN_C      key3long
#define KEY_ENTER_C     key4long  

	
#define TIME_SEC    1
#define TIME_MIN    (TIME_SEC*60)
#define TIME_HOUR   (TIME_MIN*60)
#define TIME_DAY    (TIME_HOUR*24)
// MENU 空
#define Menu_NULL {0, 0, 0, 0, 0, 0, 0, 0, 0,}

#define DEBUGOUT(x)     printf("[%s] %d: %s\r\n", (uint8_t *)__FILE__, __LINE__, (x));

#define MSG_printf(x)   printf(x)
#define MSG_printfln(x) printf(x);printf("\r\n");

#define REF_SPEED		10 // 大约50*20=6000 ms 刷新一次屏幕


struct pwd
{
    char value[5];
    uint8_t  selected;
};

typedef enum{
    RANGE_LEVEL_INDEX = 0,
    RANGE_TIMER_INDEX,
    RANGE_GAS_INDEX,
    RANGE_ID_INDEX,
    RANGE_K_VALUE_INDEX,
	RANGE_VOLUME_INDEX,
	RANGE_LENGTH_INDEX,
	RANGE_DIAMETER_INDEX,
	RANGE_STATUS_INDEX,
	RANGE_VOLTAGE_INDEX,
	RANGE_CHANNEL_INDEX,
	RANGE_PRESSURE_INDEX,
	RANGE_MODE_INDEX,
    RANGE_INDEX_MAX,
}range_index;

#define RANGE_STATUS_MAX    1
#define RANGE_STATUS_MIN    0
#define RANGE_STATUS_SMALL  1
#define RANGE_STATUS_BIG    0


#define RANGE_LEVEL_MAX     999999
#define RANGE_LEVEL_MIN     0
#define RANGE_LEVEL_SMALL   10
#define RANGE_LEVEL_BIG     100

#define RANGE_TIMER_MAX     8
#define RANGE_TIMER_MIN     0
#define RANGE_TIMER_SMALL   1
#define RANGE_TIMER_BIG     0

#define RANGE_GAS_MAX       4
#define RANGE_GAS_MIN       0
#define RANGE_GAS_SMALL     1
#define RANGE_GAS_BIG       0

#define RANGE_ID_MAX        99999999
#define RANGE_ID_MIN        1
#define RANGE_ID_SMALL      1
#define RANGE_ID_BIG        10

#define RANGE_K_VALUE_MAX   1.00
#define RANGE_K_VALUE_MIN   0.01
#define RANGE_K_VALUE_SMALL 0.01
#define RANGE_K_VALUE_BIG   0.1


#define RANGE_LENGTH_MAX    99999
#define RANGE_LENGTH_MIN    1
#define RANGE_LENGTH_SMALL  1
#define RANGE_LENGTH_BIG    10

#define RANGE_DIAMETER_MAX        99999
#define RANGE_DIAMETER_MIN        1
#define RANGE_DIAMETER_SMALL      1
#define RANGE_DIAMETER_BIG        10

#define RANGE_VOLUME_MAX          999999
#define RANGE_VOLUME_MIN          1
#define RANGE_VOLUME_SMALL        10
#define RANGE_VOLUME_BIG          100


#define RANGE_VOLTAGE_MAX          5500
#define RANGE_VOLTAGE_MIN          1
#define RANGE_VOLTAGE_SMALL        1
#define RANGE_VOLTAGE_BIG          10


#define RANGE_CHANNEL_MAX          4
#define RANGE_CHANNEL_MIN          1
#define RANGE_CHANNEL_SMALL        1
#define RANGE_CHANNEL_BIG          0


#define RANGE_PRESSURE_MAX         8000
#define RANGE_PRESSURE_MIN         0
#define RANGE_PRESSURE_SMALL       1
#define RANGE_PRESSURE_BIG         10


#define RANGE_MODE_MAX             1
#define RANGE_MODE_MIN             0
#define RANGE_MODE_SMALL           1
#define RANGE_MODE_BIG             0

void lcd_task(void);

#ifdef __cplusplus
}
#endif

#endif
