#ifndef __HAL_GPRS_H__
#define __HAL_GPRS_H__
#include "los_typedef.h"
#include "stm32f10x.h"
#include "gprsdev.h"
#include "config.h"


#define SEND_FLAG_LOW_POWER		(1 << 2)
#define SEND_FLAG_TIME			(1 << 1)
#define SEND_FLAG_STATUS		(1 << 0)

//#define SERVER_IP   "115.28.130.242"
//#define SERVER_PORT "8997"
#define UPDATE_IP	"115.28.130.242"
#define UPDATE_PORT	"8997"
#define LOCAL_IP	"api.cellocation.com"
#define LOCAL_PORT	"80"
#define SERVER_IP   "monitor.sqd.zys.me"
#define SERVER_PORT "80"
#define SERVER_USER "ftpuser1"
#define SERVER_PASS "654321"

#define UPLOAD_IP   "222.222.120.38"
#define UPLOAD_PORT "9998"
// ����IP
//#define UPLOAD_IP   "222.222.120.52"
//#define UPLOAD_PORT "9998"

#define NTP_IP      "time.nist.gov"
#define NTP_PORT    "13"
#define NTP_TIMEOUT 8000


#define REG_TIMEOUT  30000  // Timeout for GPRS network registeration by milliseconds.
#define POLL_TIMEOUT 15  // Timeout for heatbeat polling by seconds.
#define DEFAULT_DELAY_MS 200  // Delay for each command.
#define DELAY_CIPSTART 10000  // Delay for CIPSTART.
#define DELAY_CIPRXGET 10000
#define DELAY_FTPGET   30000  // Delay for FTPGET.
#define DELAY_FTPDATA  6000

typedef enum {
    STAGE_unknown = 0, // Init stage
    STAGE_error,       // When critical error happens
    STAGE_poweroff,    // before reseting is done
    STAGE_poweron,     // when 'RDY' is received, this may not happen
    STAGE_checkreg,    // Register Network.
    STAGE_shutdown,
    STAGE_idle = 10,   // when network is registered
    STAGE_ready = 50,  // when TCP/IP connection is established
} GprsStage;

typedef enum {
    E_OK = 0,
    E_FAIL = -1,
    E_TIMEOUT = -2,
    E_RETRY = -3,
    E_NOTIME = -4,
    E_LOGIN = -5,
    E_LOGIN2 = -6,
    E_BUSY = -7,
} GPRS_RESULT;

typedef enum {
	HTTP_Error = -1,
	HTTP_LoginOut = 0,
	HTTP_Login = 1,
} HTTP_STATUS;


typedef enum {
	LOGIN_ERROR = -2,
	LOGIN_OK = 0,
	LOGIN_SESSION = 1,
	LOGIN_CPUID = 2,
	LOGIN_TIMEOUT = 3,
} HTTP_LOGIN;

typedef struct {
    u16 magic;  // magic code: 0x6867
    u8  sid[8];
    u16 cmd;
    u16 dlen;   // data length
    u16 seq;    // packet sequence
} PacketHeader;

typedef struct {
	char msg[50];
	char type[20];
	char ver[5];
	char cpuid[28];
	char time[15];
	char sid[36];
	int code;
	int status;
} HttpPackerHeader;

extern GprsStage gprs_stage;
bool gprs_init(void);
bool gprs_poweron(void);
void gprs_reset(void);
bool get_cpuid_code(void);
void gprs_service(void);
int Init_realtime(void);
int gprs_sync_time(void);
void init_client_xindi(void);
int gprs_connect(char* ip, char* port);
int send_at_command(char* cmd, char* expect, u16 timeout);
void GPRS_Task(void);
#endif
