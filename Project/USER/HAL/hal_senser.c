#include "hal_senser.h"
#include "uart.h"
#include "los_task.h"
#include "config.h"
#include "led.h"
#include "hal_eeprom.h"
#include "math.h"
#include "beep.h"
#include "stdlib.h"
#include "ADS1118.h"
#include "hal_lcd.h"

#define d_printf D_PRINTF
extern int g_print_flag;

// 当前可改变配置的罐子编号
extern int g_tank_num_id;

float g_diffpressure_k[2];
float g_pressure_k[2];
float g_temp_k[2];

/*立式罐参数*/
float Diameter_cm[2];
float Diameter_div4[2];
float PiDD[2];
float coff_ka[2]; //当h<=D/4 V(ml)=coff_ka*h
float coff_kb[2]; //当h>D/4 V(ml)=coff_kb*h - coff_b;
float coff_b[2];

// AD 结果存储区
float AD_Res[MAX_SENSOR];
// AD 配置数组
AD_Config AD_cfg[MAX_SENSOR];

int g_sensor_num_id;


// 计算重量，体积需要使用的值
float gas_k[5][4] ={
	                 {1.42906, 0.001141 , 23.13948, 111.818},  // LOX  液氧
					 {1.78402, 0.001393 , 28.25004, 136.514},  // LAR  液氩                            
                     {1.977  , 0.001101 , 22.32828, 107.898},  // CO2  二氧化碳
					 {1.25033, 0.000808 , 16.38624, 79.184 },  // LN2  液氮
                     {0.717  , 0.000426 , 8.63928 , 41.748 },  // LNG  液化天然气
	               };

void SetAlarm(void);
				   
void AD_Delay(uint32_t ms) {
	LOS_TaskDelay(ms);
}

/*
 * 设置气体介质
 * 0： 液氧
 * 1： 液氩
 * 2： 二氧化碳
 * 3： 液氮
 * 4： LNG
 */
void AD_SetGas(int gasIndex) {
	g_board_info[g_tank_num_id].gas_index = gasIndex;
		
	eeprom_Write32Bits(EEPROM_GAS_INDEX_ADDR + EEPROM_PAR_MAX_ADDR * g_tank_num_id, gasIndex);
	printf("len:%d mm\r\n", gasIndex);
}

/*
 * 获取气体介质
 */
int AD_GetGas(void) {
	uint32_t gas_index;
	
	gas_index = eeprom_Read32Bits(EEPROM_GAS_INDEX_ADDR + EEPROM_PAR_MAX_ADDR * g_tank_num_id);
	g_board_info[g_tank_num_id].gas_index = gas_index;
	
		
	return g_board_info[g_tank_num_id].gas_index;
}

/*
 * 设置罐体内容器直筒长度
 * 单位 mm
 */
void AD_SetLength(int len) {
	g_board_info[g_tank_num_id].length = len;
		
	eeprom_Write32Bits(EEPROM_LENGTH_ADDR + EEPROM_PAR_MAX_ADDR * g_tank_num_id, len);
	printf("len:%d mm\r\n", len);
}

/*
 * 获取罐体内容器直筒长度
 */
int AD_GetLength(void) {
	uint32_t len;
	
	len = eeprom_Read32Bits(EEPROM_LENGTH_ADDR + EEPROM_PAR_MAX_ADDR * g_tank_num_id);
	g_board_info[g_tank_num_id].length = len;
	
		
	return g_board_info[g_tank_num_id].length;
}

/*
 * 设置罐体类型
 * 1 卧式罐
 * 0 立式罐
 */
void AD_SetTankType(int tanktype) {
	
	g_board_info[g_tank_num_id].tank_type = tanktype;
	
	eeprom_Write32Bits(EEPROM_TANK_TYPE_ADDR + EEPROM_PAR_MAX_ADDR * g_tank_num_id, tanktype);
	
	if (tanktype) {
		printf("tank:卧式罐\r\n");
	} else {
		// 立式罐需要计算一些中间变量
		Diameter_cm[g_tank_num_id] = (float)g_board_info[g_tank_num_id].diameter/10.0;//单位cm
		Diameter_div4[g_tank_num_id] = Diameter_cm[g_tank_num_id]/4.0;//单位cm
		PiDD[g_tank_num_id] = PI*Diameter_cm[g_tank_num_id]*Diameter_cm[g_tank_num_id];
		
		coff_ka[g_tank_num_id] = PiDD[g_tank_num_id]/6.0; //当h<=D/4 V(ml)=coff_ka*h
		coff_kb[g_tank_num_id] = PiDD[g_tank_num_id]/4.0; //当h>D/4 V(ml)=coff_kb*h - coff_b;
		coff_b[g_tank_num_id]  = Diameter_cm[g_tank_num_id]*PiDD[g_tank_num_id]/48.0;

		printf("tank:立式罐\r\n");
	}	
}

/*
 * 获取罐体类型
 */
int AD_GetTankType(void) {	
	uint32_t type;
	
	type = eeprom_Read32Bits(EEPROM_TANK_TYPE_ADDR + EEPROM_PAR_MAX_ADDR * g_tank_num_id);
	g_board_info[g_tank_num_id].tank_type = type;
	
	return g_board_info[g_tank_num_id].tank_type;
}
/*
 * 设置罐体内容器直径
 * 单位 mm
 */
void AD_SetDiameter(int Diameter) {
	g_board_info[g_tank_num_id].diameter = Diameter;
	
	eeprom_Write32Bits(EEPROM_DIAMETER_ADDR + EEPROM_PAR_MAX_ADDR * g_tank_num_id, Diameter);
	
	// 计算中间变量
	Diameter_cm[g_tank_num_id] = (float)Diameter/10.0;//单位cm
	Diameter_div4[g_tank_num_id] = Diameter_cm[g_tank_num_id]/4.0;//单位cm
	PiDD[g_tank_num_id] = PI*Diameter_cm[g_tank_num_id]*Diameter_cm[g_tank_num_id];
	
	coff_ka[g_tank_num_id] = PiDD[g_tank_num_id]/6.0; //当h<=D/4 V(ml)=coff_ka*h
	coff_kb[g_tank_num_id] = PiDD[g_tank_num_id]/4.0; //当h>D/4 V(ml)=coff_kb*h - coff_b;
	coff_b[g_tank_num_id]  = Diameter_cm[g_tank_num_id]*PiDD[g_tank_num_id]/48.0;

	printf("Diameter:%d mm\r\n", Diameter);
}

/*
 * 获取内容器直径 mm
 */
int AD_GetDiameter(void) {	
	uint32_t Diameter;
	
	Diameter = eeprom_Read32Bits(EEPROM_DIAMETER_ADDR + EEPROM_PAR_MAX_ADDR * g_tank_num_id);
	g_board_info[g_tank_num_id].length = Diameter;
	
		
	return g_board_info[g_tank_num_id].diameter;
}

void AD_getAB(void) {
	printf("tank[%d]: A:%f,\t\tB:%f\r\n", g_tank_num_id, g_board_info[g_tank_num_id].diffpressure_a, g_board_info[g_tank_num_id].diffpressure_b);	
}
/*
 * 校准K值
 */
void calc_KValue(void) {
	calc_diffpressureK(g_board_info[g_tank_num_id].diffpressure_ValueLow, g_board_info[g_tank_num_id].diffpressure_ValueHigh, 
					   g_board_info[g_tank_num_id].diffpressure_VoltageLow, g_board_info[g_tank_num_id].diffpressure_VoltageHigh);
	calc_pressureK(g_board_info[g_tank_num_id].pressure_ValueLow, g_board_info[g_tank_num_id].pressure_ValueHigh, 
				   g_board_info[g_tank_num_id].pressure_VoltageLow, g_board_info[g_tank_num_id].pressure_VoltageHigh);
}
void AD_SetAB(int serson_num, int a, int b) {
	
	if (serson_num > MAX_SENSOR) {
		printf("serson num %d too big\r\n", serson_num);
		return ;
	}
	
	if (serson_num == DIFFPRESSURE_SENSOR) {
		g_board_info[g_tank_num_id].diffpressure_a = (float)a / 100;
		g_board_info[g_tank_num_id].diffpressure_b = (float)b / 100;
		eeprom_Write32Bits(EEPROM_DP_A_ADDR + EEPROM_PAR_MAX_ADDR * g_tank_num_id, a);
		eeprom_Write32Bits(EEPROM_DP_B_ADDR + EEPROM_PAR_MAX_ADDR * g_tank_num_id, b);
		printf("diffpressure:%f, %f\r\n", (float)a / 100, (float)b / 100);
	} else if (serson_num == INPUTPRESSURE_SENSOR) {
		g_board_info[g_tank_num_id].inputpressure_a = (float)a / 100;
		g_board_info[g_tank_num_id].inputpressure_b = (float)b / 100;
		eeprom_Write32Bits(EEPROM_IP_A_ADDR + EEPROM_PAR_MAX_ADDR * g_tank_num_id, a);
		eeprom_Write32Bits(EEPROM_IP_B_ADDR + EEPROM_PAR_MAX_ADDR * g_tank_num_id, b);
		printf("inputpressure:%f, %f\r\n", (float)a / 100, (float)b / 100);
	}
	calc_KValue();
}


/*
 * 设置电阻
 * num： 传感器值，在config.h 的 sensor_num 中
 * res： 电阻值
 *    ： 如果需要测量电流时，利用测得的电压 / 电阻，就可以得到当前电流
 */
void AD_SetRes(int num, int res) {
	uint32_t cfg_value;
	
	
	if (num > MAX_SENSOR) {
		printf("AD CONFIG num :%d too big\r\n", num);
		return ;
	}
	// 读取配置信息
	cfg_value = eeprom_Read32Bits(EEPROM_AD_BASE_ADDR + 4 * num);
	
	// 清除电阻值
	cfg_value &= 0x00000FFF;
	
	AD_cfg[num].res = res;
	// 设置电阻值
	cfg_value = cfg_value | (res << 12);
	
	eeprom_Write32Bits(EEPROM_AD_BASE_ADDR + 4 * num , cfg_value);
	printf("%d:res:%d\r\n", num, res);
}


/*
 * 获取电阻值
 * num： 传感器值，在config.h 的 sensor_num 中
 */
int AD_GetRes(int num) {
	if (num > MAX_SENSOR) {
		printf("sernor num %d too big\r\n", num);
		return -1;
	}
	
	return AD_cfg[num].res;
}


/*
 * 设置传感器的AD通道，模式，极性
 * num：  传感器值，在config.h 的 sensor_num 中
 * mode： 1 差分  0 单端
 * pos ： 0 正  1负
 */
void AD_SetCfg(int num, int channel_num, int mode, int pos) {
	uint32_t cfg_value;
	
	if (num > MAX_SENSOR) {
		printf("AD CONFIG num :%d too big\r\n", num);
		return ;
	}
	
	if (num > 8) {
		printf("AD CONFIG channel num :%d too big\r\n", channel_num);
		return ;
	}
	
	if (mode > 1) {
		printf("AD CONFIG mode:%d too big\r\n", mode);
		return ;
	}
	
	if (pos > 1) {
		printf("AD CONFIG pos:%d too big\r\n", pos);
		return ;
	}
	// 读取配置
	cfg_value = eeprom_Read32Bits(EEPROM_AD_BASE_ADDR + 4 * num);
	
	// 保留电阻值
	cfg_value &= 0XFFFFF000;
	
	AD_cfg[num].AD_Channel = channel_num;
	AD_cfg[num].AD_Mode = mode;
	AD_cfg[num].pos = pos;
	
	// 填充配置
	cfg_value = cfg_value | (pos << 10) | (mode << 8) | (channel_num);
	
	
	eeprom_Write32Bits(EEPROM_AD_BASE_ADDR + 4 * num, cfg_value);
	printf("%d:%d  %d  %d\r\n", num, channel_num, mode, pos);
}


// num:AD_cfg
// n: 1:channel_num
//    2:mode
//    3:pos
int AD_GetCfg(int num, int n) {
	int res = -1;
	
	switch (n) {
		case 1:
			res = AD_cfg[num].AD_Channel;
			printf("AD_cfg[%d]:channel num:%d\r\n", num, res);
			break;
		
		case 2:
			res = AD_cfg[num].AD_Mode;
			if (res == ADS8344_Mode_signel) {
				printf("AD_cfg[%d], Mode:signel\r\n", num);
			} else if (res == ADS8344_Mode_diff) {
				printf("AD_cfg[%d], Mode:diff\r\n", num);
			}
			break;
		
		case 3:
			res = AD_cfg[num].pos;
			printf("AD_cfg[%d], Pos:%d\r\n", num, res);
			break;
		
		default:
			printf("参数错误\r\n");
	}
	return res;
}



void setMaxVolume(int maxVolume) {
	g_board_info[g_tank_num_id].maxVolume = maxVolume;
	eeprom_Write32Bits(EEPROM_MAX_VOLUME_ADDR + EEPROM_PAR_MAX_ADDR * g_tank_num_id, maxVolume);
}

int getMaxVolume(void) {
	return g_board_info[g_tank_num_id].maxVolume;
}

// 获取阈值
void getThreshold(void) {
	printf("|-------------------------------------------------------|\r\n");
	printf("|  levelHH:%d mm\t\t alarm:%d		|\r\n", g_board_info[g_tank_num_id].alarm_LevelHighHigh, (g_board_info[g_sensor_num_id].alarm_Bits >> 0) | 0x1);
	printf("|  levelH:%d mm\t\t alarm:%d		|\r\n", g_board_info[g_tank_num_id].alarm_LevelHigh, (g_board_info[g_sensor_num_id].alarm_Bits >> 1) | 0x1);
	printf("|  levelL:%d mm\t\t alarm:%d		|\r\n", g_board_info[g_tank_num_id].alarm_LevelLow, (g_board_info[g_sensor_num_id].alarm_Bits >> 2) | 0x1);
	printf("|  levelLL:%d mm\t\t alarm:%d		|\r\n", g_board_info[g_tank_num_id].alarm_LevelLowLow, (g_board_info[g_sensor_num_id].alarm_Bits >> 3) | 0x1);
	printf("|  pressureH:%d KPa\t\t alarm:%d\t	|\r\n", g_board_info[g_tank_num_id].alarm_InputPressureHigh, (g_board_info[g_sensor_num_id].alarm_Bits >> 4) | 0x1);
	printf("|  Alarm:%d							|\r\n", (g_board_info[g_sensor_num_id].alarm_Bits >> 5) | 0x1);
	printf("|-------------------------------------------------------|\r\n");
}


void setThreshold(int levelHH, int levelH, int levelL, int levelLL, int pressureH) {
	g_board_info[g_tank_num_id].alarm_LevelHighHigh = levelHH; 
	g_board_info[g_tank_num_id].alarm_LevelHigh = levelH;
	g_board_info[g_tank_num_id].alarm_LevelLow = levelL;
	g_board_info[g_tank_num_id].alarm_LevelLowLow = levelLL;
	g_board_info[g_tank_num_id].alarm_InputPressureHigh = pressureH;
	
	
	// 报警阈值信息
	eeprom_Write32Bits(EEPROM_ALARM_DPHH_ADDR + EEPROM_PAR_MAX_ADDR * g_tank_num_id, g_board_info[g_tank_num_id].alarm_LevelHighHigh);
	eeprom_Write32Bits(EEPROM_ALARM_DPH_ADDR + EEPROM_PAR_MAX_ADDR * g_tank_num_id, g_board_info[g_tank_num_id].alarm_LevelHigh);
	eeprom_Write32Bits(EEPROM_ALARM_DPLL_ADDR + EEPROM_PAR_MAX_ADDR * g_tank_num_id, g_board_info[g_tank_num_id].alarm_LevelLowLow);
	eeprom_Write32Bits(EEPROM_ALARM_DPL_ADDR + EEPROM_PAR_MAX_ADDR * g_tank_num_id, g_board_info[g_tank_num_id].alarm_LevelLow);
	eeprom_Write32Bits(EEPROM_ALARM_IPH_ADDR + EEPROM_PAR_MAX_ADDR * g_tank_num_id, g_board_info[g_tank_num_id].alarm_InputPressureHigh);
	getThreshold();
}

// 返回AD通道电压值
int AD_ChannelVoltage(int channel_num, int mode, int pos) {
	// 配置通道
	AD1118_ChannelMode(channel_num, mode, pos);
	//AD_Delay(500);
	// 返回数据
	return (int)(ADS1118_ReadVal()*1000);
}

void AD_Init(void) {
	ADS_Init();
	printf("AD Init\r\n");
}


/*		  dp2  -  dp1
 * dpk = --------------
 *        cur2 -  cur1
 */
void calc_diffpressureK(int dp1, int dp2, int cur1, int cur2) {
	uint32_t current_t, value_t;

	
	cur1 = cur1 * g_board_info[g_tank_num_id].diffpressure_a + g_board_info[g_tank_num_id].diffpressure_b;  // 电流值校准
	cur2 = cur2 * g_board_info[g_tank_num_id].diffpressure_a + g_board_info[g_tank_num_id].diffpressure_b;  // 电流值校准
	printf("tank num:%d\r\n", g_tank_num_id);
	g_board_info[g_tank_num_id].diffpressure_VoltageHigh = cur2;
	g_board_info[g_tank_num_id].diffpressure_VoltageLow = cur1;
	g_board_info[g_tank_num_id].diffpressure_ValueHigh = dp2;
	g_board_info[g_tank_num_id].diffpressure_ValueLow = dp1;
	
	eeprom_Write32Bits(EEPROM_DP_VALUE_HIGH_ADDR + EEPROM_PAR_MAX_ADDR * g_tank_num_id, 	g_board_info[g_tank_num_id].diffpressure_ValueHigh);
	eeprom_Write32Bits(EEPROM_DP_VALUE_LOW_ADDR + EEPROM_PAR_MAX_ADDR * g_tank_num_id,  	g_board_info[g_tank_num_id].diffpressure_ValueLow);
	eeprom_Write32Bits(EEPROM_DP_VOL_HIGH_ADDR + EEPROM_PAR_MAX_ADDR * g_tank_num_id,   	g_board_info[g_tank_num_id].diffpressure_VoltageHigh);
	eeprom_Write32Bits(EEPROM_DP_VOL_LOW_ADDR + EEPROM_PAR_MAX_ADDR * g_tank_num_id,  		g_board_info[g_tank_num_id].diffpressure_VoltageLow);
	
	
	current_t = cur2 - cur1;//g_board_info[g_tank_num_id].diffpressure_VoltageHigh - g_board_info[g_tank_num_id].diffpressure_VoltageLow;
	value_t = dp2 - dp1;//g_board_info[g_tank_num_id].diffpressure_ValueHigh - g_board_info[g_tank_num_id].diffpressure_ValueLow;
	
	
	printf("current_t:%d uA  value_t:%d Pa\r\n", current_t, value_t);
	if (value_t != 0 && current_t != 0) {	
		g_diffpressure_k[g_tank_num_id] = (float)value_t / (float)current_t;
	} else {
		if (value_t == 0)
			printf("value1 == value2\r\n");
		if (current_t == 0) 
			printf("Current1 == Current2\r\n");
		g_diffpressure_k[g_tank_num_id] = 1;
	}
	printf("g_diffpressure_k:%f\r\n", g_diffpressure_k[g_tank_num_id]);
}
/*	 	 p2   -  p1
 * pk = --------------
 *       cur2 -  cur1
 */
void calc_pressureK(int p1, int p2, int cur1, int cur2) {
	uint32_t current_t, value_t;

	current_t = cur2 - cur1;  // uA   //g_board_info[g_tank_num_id].pressure_VoltageHigh - g_board_info[g_tank_num_id].pressure_VoltageLow;
	value_t = p2 - p1;  // Kpa  //g_board_info[g_tank_num_id].pressure_ValueHigh - g_board_info[g_tank_num_id].pressure_ValueLow;
	
	
	g_board_info[g_tank_num_id].pressure_VoltageHigh = cur2;
	g_board_info[g_tank_num_id].pressure_VoltageLow = cur1;
	g_board_info[g_tank_num_id].pressure_ValueHigh = p2;
	g_board_info[g_tank_num_id].pressure_ValueLow = p1;
	
	eeprom_Write32Bits(EEPROM_P_VALUE_HIGH_ADDR + EEPROM_PAR_MAX_ADDR * g_tank_num_id, 		g_board_info[g_tank_num_id].pressure_ValueHigh);
	eeprom_Write32Bits(EEPROM_P_VALUE_LOW_ADDR + EEPROM_PAR_MAX_ADDR * g_tank_num_id, 		g_board_info[g_tank_num_id].pressure_ValueLow);
	eeprom_Write32Bits(EEPROM_P_VOL_HIGH_ADDR + EEPROM_PAR_MAX_ADDR * g_tank_num_id, 		g_board_info[g_tank_num_id].pressure_VoltageHigh);
	eeprom_Write32Bits(EEPROM_P_VOL_LOW_ADDR + EEPROM_PAR_MAX_ADDR * g_tank_num_id, 		g_board_info[g_tank_num_id].pressure_VoltageLow);
	
	
	printf("pressure, cur_t:%d, value_t:%d\r\n", current_t, value_t);
	if (value_t != 0 && current_t != 0) {	
		g_pressure_k[g_tank_num_id] = (float)value_t / (float)current_t;
		
	} else {
		if (value_t == 0)
			printf("value1 == value2\r\n");
		if (current_t == 0) 
			printf("Current1 == Current2\r\n");
		
		
		g_pressure_k[g_tank_num_id] = 1;
	}
	printf("g_pressure_k:%f\r\n", g_pressure_k[g_tank_num_id]);
}


/*
 * 计算压力
 * 
 * pressure = (value - pressure_ValueLow) * pk + pressure_ValueLow
 */
void calc_pre(uint32_t Voltage_value) {
	float value = 0;
	float cur_pre;
	
	debugInfo[g_tank_num_id].p_ad = Voltage_value;
	if (!AD_cfg[INPUTPRESSURE_SENSOR].AD_Mode) {
		value = (float)Voltage_value; // mV;
	} else {
		value = (float)Voltage_value * 1000 / 100; // uV
	}
	printf("tank num:%d\t\tpressure vol value:%f mV\r\n", g_sensor_num_id, value);
	
	if(value > g_board_info[g_sensor_num_id].pressure_VoltageLow)
	{	// Kpa
		cur_pre = g_board_info[g_sensor_num_id].pressure_ValueLow + g_pressure_k[g_sensor_num_id] * ((float)(value) - g_board_info[g_sensor_num_id].pressure_VoltageLow);//??pa
	}
	else
	{
		cur_pre = 0; // Kpa
	}
	g_board_info[g_sensor_num_id].InputPressure = cur_pre; // Kpa
	debugInfo[g_tank_num_id].p_val = cur_pre;
	printf("input pressure:%d\n", g_board_info[g_sensor_num_id].InputPressure);
}

void calc_vo(uint32_t Voltage_value) {
	int32_t cur_pressure;
	float h, l, radius;
	float V1, V2, V3, V;
	uint32_t vo, m, max_weight;
	float value = 0;
	
	
	debugInfo[g_sensor_num_id].dp_ad = Voltage_value;
	if (!AD_cfg[DIFFPRESSURE_SENSOR].AD_Mode) {
		value = (float)Voltage_value; // mV
	} else {
		value = (float)Voltage_value * 1000 / 100; // uA
	}
	
	printf("tank num:%d\t\tdiffpressure vol value:%fmV\r\n", g_sensor_num_id, value);
	if(value > g_board_info[g_sensor_num_id].diffpressure_VoltageLow)
	{
		cur_pressure = g_board_info[g_sensor_num_id].diffpressure_ValueLow + g_diffpressure_k[g_sensor_num_id] * ((float)(value) - g_board_info[g_sensor_num_id].diffpressure_VoltageLow);//??pa
	}
	else
	{
		cur_pressure = 0;
	}
	debugInfo[0].dp_val = cur_pressure;
	printf("cur_pressure:%d Pa\r\n", cur_pressure);
	printf("num _id:%d\r\n", g_sensor_num_id);
	h =  (float)cur_pressure/gas_k[g_board_info[g_sensor_num_id].gas_index][3];//??cm ????
	debugInfo[g_sensor_num_id].level = h*10;
	printf("mm H2O:%f mm\r\n", h * gas_k[g_board_info[g_sensor_num_id].gas_index][1] * 10000);
	g_board_info[g_sensor_num_id].level = h * gas_k[g_board_info[g_sensor_num_id].gas_index][1] * 10000;
	debugInfo[g_sensor_num_id].h20_mm = g_board_info[g_sensor_num_id].level;
	printf("tank num:%d\t\tlevel:%fcm\r\n",g_sensor_num_id, h);
    /*************卧式罐*********************/
	if (g_board_info[g_sensor_num_id].tank_type) {
		if ((h*10) >= g_board_info[g_sensor_num_id].diameter) {
			h = g_board_info[g_sensor_num_id].diameter / 10.0;
		}
		h = h/100; // m
		l = g_board_info[g_sensor_num_id].length;
		radius =  (float)g_board_info[g_sensor_num_id].diameter/2000;				//??m,??????

		V1 = l*(PI*radius*radius/2);
		V2 = l*(radius*radius*asin(1-h/radius)+(radius-h)*sqrt(2*h*radius-h*h));
		
		if (V1 < V2) {
			V = 0;
		} else {
			V = V1 - V2;
		}
		vo = V*1000;
	} else {

//		/****************立式罐*******************/
		printf("tank num:%d\t\tmax diameter / 4:%f cm\r\n", g_sensor_num_id, Diameter_div4[g_sensor_num_id]);
		if(h > Diameter_div4[g_sensor_num_id])
		{
			vo = (coff_kb[g_sensor_num_id] * h - coff_b[g_sensor_num_id] + 0.5);//cm3
		}
		else
		{
			vo = (coff_ka[g_sensor_num_id] * h + 0.5); //cm3
		}
		printf("tank num:%d\t\tmax voume:%d L\r\n", g_sensor_num_id, g_board_info[g_sensor_num_id].maxVolume);
	}
	/***********************************************/
	if(g_board_info[g_sensor_num_id].maxVolume < MAXVOLUME_5M3)
	{
		m = (int)(((vo*gas_k[g_board_info[g_sensor_num_id].gas_index][1] + 0.5))/ MIN_DIVISION_20);//
		m = m * MIN_DIVISION_20;//15kg
	}
	else if(g_board_info[g_sensor_num_id].maxVolume > MAXVOLUME_5M3 && g_board_info[g_sensor_num_id].maxVolume < MAXVOLUME_10M3)
	{
		m = (int)(((vo*gas_k[g_board_info[g_sensor_num_id].gas_index][1]+0.5))/ MIN_DIVISION_50);//
		m = m * MIN_DIVISION_50;//??kg
	}
	else
	{
		m = (int)(((vo*gas_k[g_board_info[g_sensor_num_id].gas_index][1]+0.5))/ MIN_DIVISION_100);//
		m = m * MIN_DIVISION_100;//??kg
	}
	
	debugInfo[g_sensor_num_id].weight = m;
	g_board_info[g_sensor_num_id].weight = m;
	g_board_info[g_sensor_num_id].volume = (m/gas_k[g_board_info[g_sensor_num_id].gas_index][1]+500)/1000;//??L,????
    g_board_info[g_sensor_num_id].per = (float)vo / (float)g_board_info[g_sensor_num_id].maxVolume;//???*10,????
	if (g_board_info[g_sensor_num_id].per > 1000) {
		g_board_info[g_sensor_num_id].per = 1000;
	}
	g_board_info[g_sensor_num_id].per = (g_board_info[g_sensor_num_id].per>1000) ? 1000 : g_board_info[g_sensor_num_id].per;
	
	printf("percentage:%f %%\r\n", (float)g_board_info[g_sensor_num_id].per / 10);
	printf("weight:%d\n", g_board_info[g_sensor_num_id].weight);
	printf("tank num:%d\t\tweight:%d kg\r\n", g_sensor_num_id, g_board_info[g_sensor_num_id].weight);
}

void data_collection(void) {
	static int data_count = 0;
	static uint32_t dp_ad_Voltage_value[TANK_MAX_LEN] = {0}; // diffpressure
	static uint32_t ip_ad_Voltage_value[TANK_MAX_LEN] = {0}; // inputpressure
		
	dp_ad_Voltage_value[0] += AD_Res[DIFFPRESSURE_SENSOR]; // uV
	ip_ad_Voltage_value[0] += AD_Res[INPUTPRESSURE_SENSOR]; // uV
	
	data_count++;
	
	if (data_count >= AD_AVG_NUM) {
		for (g_sensor_num_id = 0;g_sensor_num_id < TANK_MAX_LEN;g_sensor_num_id++) {
			// 液位计算
			dp_ad_Voltage_value[g_sensor_num_id] /= AD_AVG_NUM;
			calc_vo(dp_ad_Voltage_value[g_sensor_num_id]);
			
			// 压力计算
			ip_ad_Voltage_value[g_sensor_num_id] /= AD_AVG_NUM;
			calc_pre(ip_ad_Voltage_value[g_sensor_num_id]);
			
			
			dp_ad_Voltage_value[g_sensor_num_id] = 0;
			ip_ad_Voltage_value[g_sensor_num_id] = 0;
			SetAlarm();
		}
		data_count = 0;
	}
}
int AlarmGetBit(int n) {
	return ((g_board_info[g_sensor_num_id].alarm_Bits >> n) & 0x1);
}

void AlarmSetBit(int n, bool s) {
	if (s) {
		g_board_info[g_sensor_num_id].alarm_Bits |= (0x1 << n); 
	} else {
		g_board_info[g_sensor_num_id].alarm_Bits &= ~(0x1 << n); 
	}
}

void SetAlarm(void) {
	if (g_board_info[g_sensor_num_id].level > g_board_info[g_sensor_num_id].alarm_LevelHighHigh) {
		AlarmSetBit(0, SET);
	} else {
		AlarmSetBit(0, RESET);
	}
	
	if ((g_board_info[g_sensor_num_id].level > g_board_info[g_sensor_num_id].alarm_LevelHigh)) {
		AlarmSetBit(1, SET);
	} else {
		AlarmSetBit(1, RESET);
	}
	
	if ((g_board_info[g_sensor_num_id].level < g_board_info[g_sensor_num_id].alarm_LevelLowLow)) {
		AlarmSetBit(2, SET);
	} else {
		AlarmSetBit(2, RESET);
	}
	
	if (g_board_info[g_sensor_num_id].level < g_board_info[g_sensor_num_id].alarm_LevelLow) {
		AlarmSetBit(3, SET);
	} else {
		AlarmSetBit(3, RESET);
	}
		
	if (g_board_info[g_sensor_num_id].alarm_Bits & 0x1f) {
		AlarmSetBit(5, SET);
	} else {
		AlarmSetBit(5, RESET);
	}
}
void get_sensor_data_task(void) {
	int i;
	float a[4];
	AD_Init();
	
	for (;;) {
		for (i = 0;i < MAX_SENSOR;i++) {
			AD_Res[i] = (float)AD_ChannelVoltage(AD_cfg[i].AD_Channel, AD_cfg[i].AD_Mode, AD_cfg[i].pos) * 2;  // uV
			AD_Delay(500);
		}
		
		data_collection();
	}
}

