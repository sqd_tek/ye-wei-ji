/* Includes ------------------------------------------------------------------*/
#include "delay.h"
#include <stdlib.h>
#include <stdint.h>
#include <string.h>
#include "MQTTPacket.h"
#include "gprsdev.h"
#include "gprs.h"
#include "transport.h"
#include "los_hwi.h"
#include "los_task.ph"
/* Private typedef -----------------------------------------------------------*/
/* Private define ------------------------------------------------------------*/
/* Private macro -------------------------------------------------------------*/
extern uint8_t domain_ip[4];
#define d_printf printf
/* Private variables ---------------------------------------------------------*/
/* Private function prototypes -----------------------------------------------*/
/* Private functions ---------------------------------------------------------*/
/**
  * @brief  通过TCP方式发送数据到TCP服务器
  * @param  buf 数据首地址
  * @param  buflen 数据长度
  * @retval 小于0表示发送失败
  */
int transport_sendPacketBuffer(unsigned char* buf, int buflen)
{
    int i = 0;
    int len = -1; 
	
	d_printf("buflen:%d\r\n", buflen);
    check_and_clear_rxbuffer();
    len = write_data((char*)buf, buflen);
    //d_printf("Packet:[");
//    for (i = 0; i < buflen; i++) {
//        d_printf("%02x ", buf[i]);
//    }
//    d_printf("]\r\n");
    gprs_delay_msec(DEFAULT_DELAY_MS);
    if (len == 0) {
        return buflen;
    } else {
        return len;
    }
}
/**
  * @brief  阻塞方式接收TCP服务器发送的数据
  * @param  buf 数据存储首地址
  * @param  count 数据缓冲区长度
  * @retval 小于0表示接收数据失败
  */

int g_location;

int transport_getdata(unsigned char* buf, int count)
{
    int len = 0;
    int i = 0;
    char *ptr = NULL;
    printf("transport_getdata num:%d,g_location:%d\r\n", count,g_location);
    ptr = strstr(&GPRS_RxBuffer[g_location], "SEND OK\r\n");
    if (ptr == NULL) {
        ptr = &GPRS_RxBuffer[g_location]; 
    } else {
        
        while (!((GPRS_RxBuffer[i] == ptr[9]) && (GPRS_RxBuffer[i + 1] == ptr[10]) && (GPRS_RxBuffer[i + 2] == ptr[11])) && (i < GPRS_RXBUFFER_LEN)) {
            i++;
           // printf("transport_getdata:%d\r\n", i);
        }
        if (i < GPRS_RXBUFFER_LEN) {
            g_location = i;
            ptr = &ptr[9]; 
        }
    }
    for (i = 0; i < count; i++) {
        buf[i] = ptr[i];
    }
    //strncpy((char*)buf, ptr, count);
    g_location += count;
    if ((count == 1) && (buf[0] == NULL)) {
        g_location--;
    }
    
    for (i = 0; i < count; i++) {
        d_printf("buf[%d]:[0x%x]\r\n", i, buf[i]);
    }
 
    return count;

}
unsigned char flag = 0;

/**
  * @brief  打开一个socket并连接到服务器
  * @param  无
  * @retval 小于0表示打开失败
  */
unsigned char NO_Send;
int transport_open(bool status)
{
    if (connect_tcp(SERVER_IP, SERVER_PORT) != E_OK) {
        NO_Send++;
        d_printf("Cannot connect to %s:%s\r\n", SERVER_IP, SERVER_PORT);
        
        return E_FAIL;
    } else {
        d_printf("connect ok\r\n");
    }
   
	return E_OK;
}
/**
  * @brief  关闭socket
  * @param  无
  * @retval 小于0表示关闭失败
  */
int transport_close(void)
{
    GPRS_TCP_Close();
    return 1;
}
unsigned char mqtt_buf[512];
int mqtt_publish(char *pTopic, char *pMessage)
{
    int32_t len, rc;
    MQTTPacket_connectData data = MQTTPacket_connectData_initializer;

	memset(mqtt_buf, 0, sizeof(mqtt_buf));
    MQTTString topicString = MQTTString_initializer;
    int msglen = strlen(pMessage);
    int buflen = sizeof(mqtt_buf);
    
    data.clientID.cstring = "me";
    data.keepAliveInterval = 15;
    data.cleansession = 1;
    len = MQTTSerialize_connect(mqtt_buf, buflen, &data); /* 1 */
    topicString.cstring = pTopic;
    len += MQTTSerialize_publish(mqtt_buf + len, buflen - len, 0, 0, 0, 0, topicString, (unsigned char*)pMessage, msglen); /* 2 */

    len += MQTTSerialize_disconnect(mqtt_buf + len, buflen - len); /* 3 */
    if  ( len > 512) {
        printf("length > 512 publish ERROR\r\n");
        d_printf("len3:%d\r\n", len);
        return 1;
    } 
    transport_open(true);	
    rc = transport_sendPacketBuffer(mqtt_buf, len);
    transport_close();
    if (rc == len)
		d_printf("Successfully published\n\r");
	else
		d_printf("Publish failed\n\r");
    return 0;
}
/**
  * @brief  向服务器订阅一个消息，该函数会因为TCP接收数据函数而阻塞
  * @param  pTopic 消息主题，传入
  * @param  pMessage 消息内容，传出
  * @retval 小于0表示订阅消息失败
  */
int mqtt_subscrib(char *pTopic,char *pMessage)
{
	MQTTPacket_connectData data = MQTTPacket_connectData_initializer;
	int rc = 0;
    unsigned char counter = 0;
    int i = 0;
	unsigned char buf[200];
	int buflen = sizeof(buf);
	int msgid = 1;
    MQTTString topicString = MQTTString_initializer;
	int req_qos = 0;
    flag = 1;
	int len = 0;
	rc = transport_open(false);
	if (rc < 0) {
        d_printf("transport_open error\n\r");
       // return rc;
    }
    gprs_delay_msec(DEFAULT_DELAY_MS);
    data.clientID.cstring = "me";
	data.keepAliveInterval = 12;//服务器保持连接时间，超过该时间后，服务器会主动断开连接，单位为秒
	data.cleansession = 1;
 
	len = MQTTSerialize_connect(buf, buflen, &data);
	rc = transport_sendPacketBuffer(buf, len);
    if (rc != len) {
        d_printf("connect transport_sendPacketBuffer error\n\r");
        goto exit;
    }
    d_printf("wait for connack\r\n");
    memset(buf, 1, sizeof(buf));
    
//	/* wait for connack */
	if (MQTTPacket_read(buf, sizeof(buf), transport_getdata) == CONNACK) {
		unsigned char sessionPresent, connack_rc;

		if (MQTTDeserialize_connack(&sessionPresent, &connack_rc, buf, buflen) != 1 || connack_rc != 0) {
			d_printf("Unable to connect, return code %d\n\r", connack_rc);
			goto exit;
		}
	} else {
        d_printf("MQTTPacket_read error\n\r");
        goto exit;
    }
    d_printf("connack over\r\n");
    
//	/* subscribe */
	topicString.cstring = pTopic;
	len = MQTTSerialize_subscribe(buf, buflen, 0, msgid, 1, &topicString, &req_qos);

	rc = transport_sendPacketBuffer(buf, len);
    if (rc != len) {
        d_printf("connect transport_sendPacketBuffer error\n\r");
        goto exit;
    }
    d_printf("SUBACK wait:\r\n");
	if (MQTTPacket_read(buf, buflen, transport_getdata) == SUBACK) 	{  /* wait for suback */
        d_printf("SUBACK ok\r\n");
		unsigned short submsgid;
		int subcount;
		int granted_qos;

		rc = MQTTDeserialize_suback(&submsgid, 1, &subcount, &granted_qos, buf, buflen);
		if (granted_qos != 0) {
			d_printf("granted qos != 0, %d\n\r", granted_qos);
			goto exit;
		}
	} else {
        d_printf("SUBACK Error\r\n");
		goto exit;
    }

	/* loop getting msgs on subscribed topic */
	topicString.cstring = pTopic;
    memset(buf, 0, buflen);
    rc = 0;
    check_and_clear_rxbuffer();
    while ((rc != 3) && (counter < 9)) {
        rc = MQTTPacket_read(buf, buflen, transport_getdata) ;
        LOS_TaskDelay(1000);
        counter++;
    }
    if (counter < 5) {
        unsigned char dup;
        int qos;
        unsigned char retained;
        unsigned short msgid;
        int payloadlen_in;
        unsigned char* payload_in;
        MQTTString receivedTopic;

        rc = MQTTDeserialize_publish(&dup, &qos, &retained, &msgid, &receivedTopic, &payload_in, &payloadlen_in, buf, buflen);
        d_printf("message arrived %d: %s\n\r", payloadlen_in, payload_in);
        strcpy(pMessage,(const char *)payload_in);
    }

    d_printf("disconnecting\n\r");
    len = MQTTSerialize_disconnect(buf, buflen);
    rc = transport_sendPacketBuffer(buf, len);
exit:
    transport_close();
    //disconnect_tcp();
    return rc;
}

