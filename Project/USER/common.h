#ifndef _COMMON_H_
#define _COMMON_H_

#include "stm32f10x_gpio.h"
#include "hal_gprs.h"
extern u16 g_version;
extern u32 g_boardid;

#define STM32F103VE
//#define STM32F103VC

#ifdef STM32F103VE
   // Map of on-chip Flash (512KB)
		#define BootloaderAddr            0x08000000    // Size = 0x09000, 36KB
		#define SysConfigAddr             0x08009000    // Size = 0x01000, 4KB
		#define PartitionA                0x0800A000    // Size = 0x19000, 100KB
		#define PartitionAInfo            0x08023800    // Size = 0x00800, 2KB
		#define PartitionB                0x08024000    // Size = 0x19000, 100KB
		#define PartitionBInfo            0x0803d800    // Size = 0x00800, 2KB
		#define PartitionC                0x0803e000    // Size = 0x19000, 100KB
		#define PartitionCInfo            0x08057800    // Size = 0x00800, 2KB
		// End of partition is            0x80058000 - 1

#endif
#ifdef STM32F103VC
		#define BootloaderAddr            0x08000000    // Size = 0x09000, 36KB
		#define SysConfigAddr             0x08009000    // Size = 0x01000, 4KB
		#define PartitionA                0x0800A000    // Size = 0x11000, 68KB
		#define PartitionAInfo            0x0801B000    // Size = 0x00800, 2KB
		#define PartitionB                0x0801B800    // Size = 0x11000, 68KB
		#define PartitionBInfo            0x0802C800    // Size = 0x00800, 2KB
		#define PartitionC                0x0802D000    // Size = 0x11000, 68KB
		#define PartitionCInfo            0x0803E000    // Size = 0x00800, 2KB
#endif

typedef enum {
    ParA = 0,
    ParB = 1,
    ParC = 2,
    ParInvalid,
}PartitionNum;

typedef struct {
    u32 base;
    u32 info;
    u32 len;
} PartitionConf;

// Timeout seconds before loading from flash
#define UART_UPDATE_TIMEOUTS 16

#define BOOT_MAGIC 0x0c0ffee0
#define USER_CONF_LEN 256
#define MAX_FAILURE 3  // larger than that, the partition will be deleted

typedef struct {
    u32 magic;
    u32 version;
    u32 timestamp;  // time for downloading
    u32 boardid;

    // local info for updating and bootloader
    u32 app_len;
    u32 writep;  // next position for paritition writing
    u16 copyfrom;  // which partition the partition A copied from
    u16 failure;   // failure count of boot

    // global info for partitions
    // in SysConfigAddr, all partitions info are valid, but len is not accurate
    // in each partition, only itself info is valid
    PartitionConf partitions[3];

    //  TODO: run-time info for user configuration
    u32 config[USER_CONF_LEN];

    // Verification
    u32 checksum;
} PartitionInfo;

typedef void (*FunVoidType)(void);


// Default config data, const with bootloader
extern const PartitionInfo *def_info;
// Run time config data, effective only in each run-time application
extern PartitionInfo g_info;

void init_factory_info(void);
PartitionNum gprs_update_init(void);
int gprs_get_data_callback(char* buf, int len);
void gprs_update_done(void);

bool runable_address(u32 addr);
void run_application(u32 addr, PartitionNum copyfrom);
void reset_by_iwdg(void);
bool valid_application(PartitionNum par);
bool boot_enter(PartitionNum copyfrom);
void boot_done(void);
void update_vtor(void);

void init_partition_info(PartitionInfo *info);
bool save_partition_info(PartitionNum par, PartitionInfo *info);
bool load_partition_info(PartitionNum par, PartitionInfo *info);
bool copy_partition(PartitionNum from, PartitionNum to);
bool delete_partition(PartitionNum par);
#endif

